CC = g++
CXXFLAGS += -std=c++11 -g -O3
LDFLAGS += 
LDLIBS += -lboost_program_options -lglog 

all:	glview mesh_color mesh_ext #mesh_simplify


glview:	glview.cpp
	g++ -o $@ $(CXXFLAGS) $^ $(LDFLAGS) $(LDLIBS) -lglfw -lGLEW -lGL

glrender:	glrender.cpp
	g++ -o $@ $(CXXFLAGS) $^ $(LDFLAGS) $(LDLIBS) -lglfw -lGLEW -lGL -lopencv_highgui -lopencv_imgproc -lopencv_core

glrender-list:	glrender-list.cpp
	g++ -o $@ $(CXXFLAGS) $^ $(LDFLAGS) $(LDLIBS) -lglfw -lGLEW -lGL -lopencv_highgui -lopencv_imgproc -lopencv_core

glrender2:	glrender2.cpp cnpy.cpp
	g++ -o $@ $(CXXFLAGS) $^ $(LDFLAGS) $(LDLIBS) -lglfw -lGLEW -lGL -lopencv_highgui -lopencv_imgproc -lopencv_core
