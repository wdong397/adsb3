import sys
import os
import simplejson as json
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from django.db import IntegrityError, transaction
from django.contrib.auth.models import User
from annotate.utils import fix_transpose
from annotate.models import *

class Command(BaseCommand):

    def handle(self, *args, **options):
        seen = {}
        for anno in Annotation.objects.all():
            a = fix_transpose(json.loads(anno.anno)["shapes"][0]["geometry"])
            path = anno.image.meta
            name = os.path.splitext(os.path.basename(path))[0]
            if name in seen:
                continue
            seen[name] = True
            print "%s\t%g\t%g\t%g\t%g" %(name, a["x"], a["y"], a["width"], a["height"])
        #hours = options['hours'] + 24 * options['days']
        #check_and_import(hours, not options['run'], options['check'])

        pass

