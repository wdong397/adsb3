#!/usr/bin/env python
import sys
import random
import json
import glob
import subprocess
from tqdm import tqdm
from adsb3 import *
import color
import cv2
import picpac

VAL = 20

class Anno:
    def __init__ (self):
        import cPickle as pickle
        PKL_PATH = os.path.join(DATA_DIR, 'case_anno.p')
        if os.path.exists(PKL_PATH):
            with open(PKL_PATH, 'rb') as f:
                print "Loading cases from %s" % PKL_PATH
                self.cases = pickle.load(f)
                return
        case_lookup = {}
        for path in glob.glob(os.path.join(DATA_DIR, 'stage1/*/*.dcm')):
            case = os.path.basename(os.path.dirname(path))
            frame = os.path.splitext(os.path.basename(path))[0]
            #print frame, "=>", case
            case_lookup[frame] = case
            pass
        cases = {}
        with open(os.path.join(DATA_DIR, 'axial_annotations'), 'r') as f:
            for l in f:
                name, x, y, w, h = l.strip().split()
                case = case_lookup[name]
                cases.setdefault(case, []).append((name, float(x), float(y), float(w), float(h)))
                pass
            pass
        self.cases = []
        for uid, _ in STAGE1.train:
            if uid in cases:
                self.cases.append((uid, cases[uid]))
                pass
            pass
        with open(PKL_PATH, 'wb') as f:
            pickle.dump(self.cases, f)
        pass
    pass

ANNO = Anno()

subprocess.check_call('rm -rf db/axial_pos.pic db/sagittal_pos.pic db/coronal_pos.pic', shell=True)
subprocess.check_call('rm -rf db/axial_pos_val.pic db/sagittal_pos_val.pic db/coronal_pos_val.pic', shell=True)

axial_db = picpac.Writer('db/axial_pos.pic')
sagittal_db = picpac.Writer('db/sagittal_pos.pic')
coronal_db = picpac.Writer('db/coronal_pos.pic')

axial_val_db = picpac.Writer('db/axial_pos_val.pic')
sagittal_val_db = picpac.Writer('db/sagittal_pos_val.pic')
coronal_val_db = picpac.Writer('db/coronal_pos_val.pic')

def conv_bounds (box, zs, Y, X):
    lz, uz, ly, uy, lx, ux = box
    lz /= zs
    uz /= zs
    ly *= Y
    uy *= Y
    lx *= X
    ux *= X
    return [(lz, uz), (ly, uy), (lx, ux)]

def dump (case, bounds, db):
    idx = set()
    for lb, ub in bounds:
        for i in range(int(round(lb)), int(round(ub))+1):
            idx.add(i)
            pass
        pass
    images = case.images
    print "dump view", case.view, idx
    for i in idx:
        if i >= images.shape[0]:
            continue
        image = case.images[i]
        buf = cv2.imencode('.jpg', image)[1].tostring()
        db.append(1, buf)
        pass
    pass

CLIP = 0.3

uids = set([uid for uid, label in STAGE1.train if label > 0])
for uid, anno in ANNO.cases:
    uids.remove(uid)
    pass
uids = list(uids)
with open('data/pos_val_uids', 'w') as f:
    f.write('\n'.join([str(uid) for uid in uids]))
    pass

random.shuffle(ANNO.cases)
C = 0
for uid, anno in ANNO.cases:
    print uid, anno
    case = load_8bit_lungs(uid)

    boxes = []
    zs = case.spacing[2]
    for name, x, y, w, h in anno:
        z = case.dcm_z_position[name]
        #x = x * X
        #w = w * X
        #y = y * Y
        #h = h * Y
        cz = zs * CLIP
        ch = h * CLIP
        cw = w * CLIP
        boxes.append((z-zs+cz, z+zs-cz, y+ch, y+h-ch, x+cw, x+w-cw))
        pass

    case = case.rescale3D(SPACING)
    Z, Y, X = case.images.shape
    zs = case.spacing[2]

    boxes = [conv_bounds(box, zs, Y, X) for box in boxes]

    if C >= VAL:
        dump(case, [box[0] for box in boxes], axial_db)
        dump(case.transpose(CORONAL), [box[1] for box in boxes], coronal_db)
        dump(case.transpose(SAGITTAL), [box[2] for box in boxes], sagittal_db)
    else:
        dump(case, [box[0] for box in boxes], axial_val_db)
        dump(case.transpose(CORONAL), [box[1] for box in boxes], coronal_val_db)
        dump(case.transpose(SAGITTAL), [box[2] for box in boxes], sagittal_val_db)
    C += 1
    pass

