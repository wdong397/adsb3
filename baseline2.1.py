#!/usr/bin/env python
import sys
import numpy as np
import tensorflow as tf
import time
from skimage import measure
from adsb3 import *
import cPickle as pickle
import nets

def find_center (prob, bbox):
    z0, y0, x0, z1, y1, x1 = bbox
    return [(z0+z1)/2, (y0+y1)/2, (x0+x1)/2], [(z1-z0)/2, (y1-y0)/2, (x1-x0)/2]

def find_blobs (prefix, gal, case, prob):
    binary = prob > 0.5
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    pp = measure.regionprops(labels)
    cnt = 0
    for roi in pp:
        if roi.area < 10:
            continue
        C, R = find_center(prob, roi.bbox)
        gal.text('%s.%d' % (prefix, cnt))
        cnt += 1
        for view in VIEWS:
            images = case.transpose(view).images
            c = index_view(C, view)
            r = index_view(R, view)
            I = cv2.cvtColor(images[c[0]], cv2.COLOR_GRAY2BGR)
            #P = cv2.cvtColor(prob[C[0]], cv2.COLOR_GRAY2BGR)
            #P *= 255
            cv2.circle(I, (c[2], c[1]), max(r[1], r[2]), (0, 255, 0))
            cv2.imwrite(gal.next(), I)
            #vis.append(I)
            pass
        #vis = np.hstack(tuple(vis))
        pass
    pass

BATCH = 32
def round_stride (images, stride=16):
    T, H, W = images.shape[:3]
    nT = T / stride * stride
    nH = H / stride * stride
    nW = W / stride * stride
    oT = (T - nT)/2
    oH = (H - nH)/2
    oW = (W - nW)/2
    return images[oT:(oT+nT),oH:(oH+nH),oW:(oW+nW)]

class ViewModel:
    def __init__ (self, view, name, path):
        self.name = name
        self.view = view
        self.X = tf.placeholder(tf.float32, shape=(None, None, None), name="images")
        X4 = tf.expand_dims(self.X, axis=3)
        self.prob, self.loader = nets.import_meta_graph(path, X4, name, softmax=True)
        pass

class Model:
    def __init__ (self):
        models = []
        for i in range(len(VIEWS)):
            name = VIEW_NAMES[i]
            models.append(ViewModel(VIEWS[i], name, 'models/luna/' + name))
            pass
        self.models = models
        pass

    def load (self, sess):
        for m in self.models:
            m.loader(sess)
            pass
        pass

    def apply (self, sess, case):
        r = []
        #comb = np.ones_like(case.images, dtype=np.float32)
        for m in self.models:
            cc = case.transpose(m.view)
            images = cc.images
            prob = np.zeros_like(images, dtype=np.float32)
            N = images.shape[0]
            for b in range(0, N, BATCH):
                e = min(N, b + BATCH)
                x = images[b:e]
                y, = sess.run([m.prob], feed_dict={m.X:x})
                prob[b:e] = y[:,:,:]
            prefix = '%d.%s.%s' % (label, case.uid, m.name)
            axial_prob = cc.transpose_array(AXIAL, prob)
            #find_blobs(prefix, gal, case, axial_prob)
            r.append(axial_prob)
            #np.minimum(comb, axial_prob, comb)
            pass
        #prefix = '%d.%s.comb' % (label, case.uid)
        #find_blobs(prefix, gal, case, comb)
        #r.append(comb)
        return r
    pass

model = Model()

config = tf.ConfigProto()
with tf.Session(config=config) as sess:
    tf.global_variables_initializer().run()
    model.load(sess)
    for uid, label in STAGE1.train:
        cache = os.path.join('cache/baseline2', uid)
        if not os.path.exists(cache):
            with open(cache, 'wb') as f:
                pass
            start_time = time.time()
            case = load_8bit_lungs(uid)
            case = case.rescale3D(SPACING)
            case.round_stride()
            load_time = time.time()
            rr = model.apply(sess, case)
            predict_time = time.time()

            lb1 = rr[0]
            np.minimum(lb1, rr[1], lb1)
            np.minimum(lb1, rr[2], lb1)

            np.savez_compressed(cache, lb1)
            save_time = time.time()
            print uid, label, (load_time - start_time), (predict_time - load_time), (save_time - predict_time)
        else:
            print uid, 'done'
    pass
