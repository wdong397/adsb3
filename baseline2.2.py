#!/usr/bin/env python
import sys
import numpy as np
import time
from skimage import measure
from adsb3 import *
from gallery import Gallery

def find_center (prob, bbox):
    z0, y0, x0, z1, y1, x1 = bbox
    return [(z0+z1)/2, (y0+y1)/2, (x0+x1)/2], [(z1-z0)/2, (y1-y0)/2, (x1-x0)/2]

def find_blobs (prefix, gal, case, prob):
    binary = prob > 0.5
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    pp = measure.regionprops(labels)
    cnt = 0
    for roi in pp:
        if roi.area < 10:
            continue
        C, R = find_center(prob, roi.bbox)
        gal.text('%s.%d' % (prefix, cnt))
        cnt += 1
        for view in VIEWS:
            images = case.transpose(view).images
            c = index_view(C, view)
            r = index_view(R, view)
            I = cv2.cvtColor(images[c[0]], cv2.COLOR_GRAY2BGR)
            #P = cv2.cvtColor(prob[C[0]], cv2.COLOR_GRAY2BGR)
            #P *= 255
            cv2.circle(I, (c[2], c[1]), max(r[1], r[2]), (0, 255, 0))
            cv2.imwrite(gal.next(), I)
            #vis.append(I)
            pass
        #vis = np.hstack(tuple(vis))
        pass
    pass

case = CaseBase()
case.view = AXIAL
ft = open('baseline2.ft', 'w')
for uid, label in STAGE1.train:
    cache = os.path.join('baseline2.cache', uid + '.npz')
    if not os.path.exists(cache):
        continue
    start_time = time.time()
    try:
        data = np.load(cache)
        views = [
            case.transpose_array(VIEWS[0], data['arr_0']),
            case.transpose_array(VIEWS[1], data['arr_1']),
            case.transpose_array(VIEWS[2], data['arr_2'])
            ]
    except:
        print 'failed to load', uid
        continue
    load_time = time.time()
    lb = np.ones_like(views[0], dtype=np.float32)
    ub = np.zeros_like(views[0], dtype=np.float32)
    f = []
    for view in views:
        f.append(np.sum(view))
        np.minimum(lb, view, lb)
        np.maximum(ub, view, ub)
    f.append(np.sum(lb))
    ft.write('%s %d %s\n' % (uid, label, ' '.join(['%f' % x for x in f])))
    ft.flush()
    print uid, label, ','.join(['%.4f' %x for x in f])
    #rr = model.apply(sess, case, gal, label)
    #finish_time = time.time()
    #f = []
    #for x in rr:
    #    f.append(np.sum(x))
    #    pass
    pass
ft.close()
