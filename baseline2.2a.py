#!/usr/bin/env python
import sys
import numpy as np
import time
from skimage import measure
from adsb3 import *
from pyramid import *
#from papaya import Papaya

case = CaseBase()
case.view = AXIAL
#ft = open('baseline2.ft', 'w')

#pap = Papaya('/home/wdong/public_html/baseline2')
for uid, label in STAGE1.train:
    cache = os.path.join('baseline2.cache', uid + '.npz')
    if not os.path.exists(cache):
        continue
    start_time = time.time()
    try:
        data = np.load(cache)
        views = [
            case.transpose_array(VIEWS[0], data['arr_0']),
            case.transpose_array(VIEWS[1], data['arr_1']),
            case.transpose_array(VIEWS[2], data['arr_2'])
            ]
    except:
        print 'failed to load', uid
        continue
    load_time = time.time()

    fts = extract_ft(views, pap)

    for ft in fts:
        print '\t', ft
        pass
    break
    pass
