#!/usr/bin/env python
import sys
import numpy as np
import time
from skimage import measure
from adsb3 import *
from baseline2 import *
from papaya import Papaya, Annotations

#case = CaseBase()
#case.view = AXIAL
#ft = open('baseline2.ft', 'w')

case = CaseBase()
case.view = AXIAL
pap = Papaya('/home/wdong/public_html/baseline2')
for uid, label in STAGE1.train:
    if label == 0:
        continue
    cache = os.path.join('baseline2.cache', uid + '.npz')
    if not os.path.exists(cache):
        continue
    start_time = time.time()
    try:
        data = np.load(cache)
        views = [
            case.transpose_array(VIEWS[0], data['arr_0']),
            case.transpose_array(VIEWS[1], data['arr_1']),
            case.transpose_array(VIEWS[2], data['arr_2'])
            ]
    except:
        print 'failed to load', uid
        raise
        continue
    case = load_8bit_lungs_noseg(uid)
    case = case.rescale3D(SPACING)
    case.round_stride()
    assert case.images.shape == views[0].shape
    load_time = time.time()

    fts = extract_ft(views)
    annos = Annotations()
    gal = Gallery('/home/wdong/public_html/baseline2/%s' % uid, cols=3, header=['axial', 'sagittal', 'coronal'])    
    for ft in fts:
        annos.add(ft.box, '    '.join(['%.4f' % x for x in ft.ft]))
        pass
    pap.next(case, annos)
    gal.flush()
    pass
pap.flush()

