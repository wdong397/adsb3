#!/usr/bin/env python
import math
import sys
import numpy as np
import time
from scipy.ndimage.morphology import grey_dilation, binary_dilation
from skimage import measure
from adsb3 import *
from gallery import Gallery

def extend (z0, z1, Z, ext):
    zx = int(math.ceil((z1-z0) * ext))
    if zx <= 0:
        return z0, z1
    z0 = max(0, z0 - zx)
    z1 = min(Z, z1 + zx)
    return z0, z1

class Features:
    def __init__ (self, box, prob,th):
        z0, y0, x0, z1, y1, x1 = box.bbox
        if False:
            Z, Y, X = views[0].shape
            r = 0
            rr = (z1-z0) * SPACING
            r += rr * rr
            rr = (y1-y0) * SPACING
            r += rr * rr
            rr = (x1-x0) * SPACING
            r += rr * rr
            print math.sqrt(r)
            if ext: # extend the bounding box
                z0, z1 = extend(z0, z1, Z, ext)
                y0, y1 = extend(y0, y1, Y, ext)
                x0, x1 = extend(x0, x1, X, ext)
        self.box = (z0, y0, x0, z1, y1, x1)
        #area 11
        #bbox (89L, 306L, 343L, 94L, 310L, 344L)
        #coords [[ 89 307 343],
        # ......
        # [ 93 308 343]]
        #equivalent_diameter 2.75929428187
        #extent 0.55
        #filled_area 11
        ft = []
        #ft.append((z1-z0)*(y1-y0)*(x1-x0))
        roi = prob[z0:z1,y0:y1,x0:x1]
        #roi = np.clip(roi, th, 1)
        #roi[roi < th] = 0
        unit = SPACING * SPACING * SPACING
        ft.append(np.sum(roi) * unit)
        self.ft = ft
        pass

# used to be 0.1 and 5
def extract_ft (views, th=0.05, ext=2):
    lb = np.ones_like(views[0], dtype=np.float32)

    for view in views:
        np.minimum(lb, view, lb)

    binary = lb > th
    k = int(round(ext / SPACING))
    binary = binary_dilation(binary, iterations=k)
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    boxes = measure.regionprops(labels)

    fts = []
    for box in boxes:
        #if box.area < 10:
        #    continue
        fts.append(Features(box,lb,th))#extract_box(box, views))
        pass
    return fts

