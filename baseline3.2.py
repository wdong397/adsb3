#!/usr/bin/env python
import sys
import numpy as np
import time
from skimage import measure
from adsb3 import *
from baseline2 import *
from papaya import Papaya, Annotations

#case = CaseBase()
#case.view = AXIAL
#ft = open('baseline2.ft', 'w')

case = CaseBase()
case.view = AXIAL
for uid, label in STAGE1.train:
    cache = os.path.join('cache/baseline3', uid + '.npz')
    if not os.path.exists(cache):
        continue
    output = os.path.join('cache/baseline3', uid + '.ft')
    if os.path.exists(output):
        continue
    start_time = time.time()
    try:
        data = np.load(cache)
        lb = data['arr_0']
    except:
        print 'failed to load', uid
        continue
    load_time = time.time()
    views = [lb]
    fts = extract_ft(views)
    line = '%s %s\n' % (uid, ' '.join(['%.4f' % x.ft[0] for x in fts]))
    sys.stdout.write(line)
    with open(output, 'w') as f:
        f.write(line)
    pass

