#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys
import os
import numpy as np
from adsb3 import *
from sklearn.model_selection import KFold
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss
from sklearn.preprocessing import normalize
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier

label_lookup = {}
for uid, label in STAGE1.train:
    label_lookup[uid] = label
    pass

def extract_local (x):
    if len(x) == 0:
        return [0]
    return [len(x)]

LOG = True
def extract (x):
    #x = [a for a in x if a > 5 ]
    if len(x) == 0:
        ft = [0.0, 0.0]
    else:
        ft = [sum(x), len(x)]
    if False:
        if LOG:
            x = [min(math.log(a), 7.9) for a in x if a > 1]
            ranges = [[0, 8],[0,4],[4,8],[0,2],[2,4],[4,6],[6,8]]
        else:
            x = [min(a, 2999) for a in x]
            ranges = [[10, 3000],[0,10],
                      [100, 3000],[0,100],
                      [500, 3000],
                      [1000,3000]]

        for lb, ub in ranges:
            ll = [a for a in x if a >= lb and a < ub]
            ft.extend(extract_local(ll))
            pass
    #print(ft)
    return ft

X = []
Y = []
for path in glob('cache/baseline3/*.ft'):
    with open(path, 'r') as f:
        l = f.read().strip().split(' ')
        uid = l[0]
        nodules = [float(x) for x in l[1:]]
        Y.append(label_lookup[uid])
        X.append(extract(nodules))
        pass

X = np.array(X, dtype=np.float32)

Y = np.array(Y, dtype=np.float32)
print(X.shape)
print(Y.shape)

Y1 = np.sum(Y)
Y0 = len(Y) - Y1
print(Y0, Y1)

kf = KFold(n_splits=50, shuffle=True, random_state=88)
y_pred = Y * 0
y_pred_prob = Y * 0
#model = LogisticRegression() #class_weight={0: Y1, 1:Y0})
model = GradientBoostingClassifier(n_estimators=50, learning_rate=0.1,  max_depth=1, random_state=0)

print("baseline2")
for i in range(2):
    for train, test in kf.split(X):
        X_train, X_test, y_train = X[train,:], X[test,:], Y[train]
        #model.fit(X_train, y_train)
        model.fit(X_train, y_train)
        
        y_pred[test] = model.predict(X_test)
        try:
            y_pred_prob[test] = model.predict_proba(X_test)[:,1]
        except:
            y_pred_prob[test] = y_pred[test]

    try:
        print(classification_report(Y, y_pred, target_names=["0", "1"]))
    except:
        pass
    #print(zip(Y, y_pred_prob))
    print("logloss",log_loss(Y, y_pred_prob))
    X = X[:, 0:1]
    print("baseline:")

