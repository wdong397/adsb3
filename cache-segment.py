#!/usr/bin/env python
import sys
import time
import traceback
from skimage import measure
from adsb3 import *
from multiprocessing import Pool

def cache_mask (uid):
    try:
        cache = 'cache/mask-123.85-0.01/%s.npz' % uid
        if os.path.exists(cache):
            return True
        open(cache, 'wb').close()
        case1 = load_8bit_lungs(uid)
        case2 = load_8bit_lungs(uid)
        assert np.array_equal(case1.images, case2.images)
    except:
        os.remove(cache)
        traceback.print_exc()
        traceback.print_stack()
    return True


uids = [x[0] for x in STAGE1.train + STAGE1.test]
uids.extend(list(LUNA_DIR_LOOKUP.iterkeys()))
#print uids
print len(uids), 'uids found'
pool = Pool(None)
pool.map(cache_mask, uids)

