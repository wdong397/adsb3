#!/usr/bin/env python
import sys
import time
import traceback
import subprocess
import argparse
import numpy as np
from scipy.ndimage.morphology import grey_dilation, binary_dilation
from skimage import measure
import theano
from theano import tensor as T
import lasagne
from adsb3 import *

def extract (prob, fts, th=0.05, ext=2):
    if not fts is None:
        prob4 = np.reshape(prob, prob.shape + (1,))
        assert prob4.base is prob
        fts = np.clip(fts, 0, 6)
        fts *= prob4
    binary = prob > th
    k = int(round(ext / SPACING))
    binary = binary_dilation(binary, iterations=k)
    labels = measure.label(binary, background=0)
    boxes = measure.regionprops(labels)

    nodules = []
    dim = 1
    if not fts is None:
        dim = fts.shape[3]
    for box in boxes:
        #print prob.shape, fts.shape
        z0, y0, x0, z1, y1, x1 = box.bbox
        #ft.append((z1-z0)*(y1-y0)*(x1-x0))
        prob_roi = prob[z0:z1,y0:y1,x0:x1]
        weight_sum = np.sum(prob_roi)
        UNIT = SPACING * SPACING * SPACING
        prob_sum = weight_sum * UNIT

        if fts is None:
            one = [prob_sum]
        else:
            fts_roi = fts[z0:z1,y0:y1,x0:x1,:]
            fts_sum = np.sum(fts_roi, axis=(0,1,2))
            one = list(fts_sum/weight_sum)
        nodules.append((prob_sum, one))
        pass
    nodules = sorted(nodules, key=lambda x: -x[0])
    return dim, nodules

parser = argparse.ArgumentParser(description='')
parser.add_argument('--model', default='alex_version3')
parser.add_argument('--params', default='fold0_params_50')
parser.add_argument('--bits', type=int, default=16)
parser.add_argument('--channels', type=int, default=1)
parser.add_argument('--mask', default=None)
parser.add_argument('--fast')
args = parser.parse_args()

logging.basicConfig(level=logging.INFO)

import pkgutil
loader = pkgutil.get_importer('theano_models')
# load network from file in 'models' dir
model = loader.find_module(args.model).load_module(args.model)

input_var = T.tensor4('input')
label_var = T.tensor4('label')
shape=(1,1,256,256)

net, _, _,_ = model.network(input_var, label_var, shape)
# load saved parameters from "params"
with open(os.path.join('theano_models', args.model, args.params), 'rb') as f:
    import pickle
    params = pickle.load(f)
    lasagne.layers.set_all_param_values(net, params)
    pass
output_var = lasagne.layers.get_output(net, deterministic=True)
pred = theano.function([input_var], output_var)

_, _, rows, cols =shape


name = args.model + '_' + args.params
if not args.mask is None:
    name += '_' + args.mask
if args.channels != 3:
    name += '_c' + str(args.channels)
if args.bits != 16:
    name += '_b' + str(args.bits)
if SPACING != 0.8:
    name += '_s%.1f' % SPACING
if GAP != 5:
    name += '_g%d' % GAP

ROOT = os.path.join('cache2', name)
try_mkdir(ROOT)

force = False
if len(sys.argv) > 1:
    uids = sys.argv[1:]
    force = True
elif args.fast:
    uids = [uid for uid, _ in STAGE1.fast]
else:
    uids = [uid for uid, _ in STAGE1.train + STAGE1.test]
for uid in uids:
    cache = os.path.join(ROOT, uid + '.pkl')
    if (not force) and os.path.exists(cache):
        continue
    with open(cache, 'wb') as f:
        pass
    start_time = time.time()
    if args.bits == 8:
        case = load_8bit_lungs_noseg(uid)
    elif args.bits == 16:
        case = load_16bit_lungs_noseg(uid)
    else:
        assert False
    load_time = time.time()
    mask = None
    if not args.mask is None:
        try:
            mask_path = 'maskcache/%s/%s.npz' % (args.mask, uid)
            mask = load_mask(mask_path)
            mask = case.copy_replace_images(mask.astype(dtype=np.float32))
            mask = mask.rescale3D(SPACING)
            if args.dilate > 0:
                #print 'dilate', AGS.dilate
                ksize = args.dilate * 2 + 1
                mask.images = grey_dilation(mask.images, size=(ksize, ksize, ksize), mode = 'constant')
        except:
            traceback.print_exc()
            logging.error('failed to load mask %s' % mask_path)
            mask = None

    case = case.rescale3D(SPACING)

    #fts = []
    prob = np.zeros((case.images.shape[0], rows, cols), dtype=np.float32)
    for i in range(case.images.shape[0]):
        image = cv2.resize(case.images[i],(rows, cols))
        image = image.reshape(1, 1, rows, cols)
        prob[i] = pred(image)
        ## reshape back to non-tensor
        #output *= 255   # for visualize
        #output_sum=output.sum()
        #fts.append(output_sum)
    #fts = sorted(fts, key=lambda x: -x)
    dim, nodules = extract(prob, None)

    predict_time = time.time()
    with open(cache, 'wb') as f:
        pickle.dump((1, nodules), f)
        pass
    print uid, (load_time - start_time), (predict_time - load_time)

