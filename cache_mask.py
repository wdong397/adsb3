#!/usr/bin/env python
import sys
import time
import subprocess
import numpy as np
import mesh
from adsb3 import *

if len(sys.argv) > 1:
    uids = sys.argv[1:]
    force = True
else:
    uids = [uid for uid, _ in STAGE1.train + STAGE1.test]
    force = False

for uid in uids:
    cache = os.path.join('maskcache/mask', uid + '.npz')
    if (not force) and os.path.exists(cache):
        continue
    with open(cache, 'wb') as f:
        pass
    start_time = time.time()
    case = load_case(uid)
    case.normalizeHU()
    spacing = case.spacing
    UNIT = spacing[0] * spacing[1] * spacing[2]
    binary, body_counts = mesh.segment_lung(case.images) #, smooth=20)
    ft = (1, [(x * UNIT, [x]) for x in body_counts])
    with open('cache2/holes/%s.pkl' % uid, 'wb') as f:
        pickle.dump(ft, f)
    save_mask(cache, binary)
    cache = os.path.join('maskcache/hull', uid + '.npz')
    binary = mesh.convex_hull(binary)
    save_mask(cache, binary)
    load_time = time.time()
    print uid, (load_time - start_time)
pass

