#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '4'
import random
import numpy as np
import tensorflow as tf
import cPickle as pickle
from adsb3 import *

class ViewModel:
    def __init__ (self, path, name='logits:0'):
        """applying tensorflow image model.

        path -- path to model
        name -- output tensor name
        prob -- convert output (softmax) to probability
        """
        graph = tf.Graph()
        with graph.as_default():
            saver = tf.train.import_meta_graph(path + '.meta')
        if False:
            for op in graph.get_operations():
                for v in op.values():
                    print(v.name)
        inputs = graph.get_tensor_by_name("images:0")
        outputs = graph.get_tensor_by_name(name)
        outputs = tf.reshape(outputs, (-1, 2))
        outputs = tf.nn.softmax(outputs)
        outputs = tf.slice(outputs, [0, 1], [-1, -1])
            # remove trailing dimension of 1
        outputs = tf.squeeze(outputs, axis=[1])

        self.path = path
        self.graph = graph
        self.inputs = inputs
        self.outputs = outputs
        self.saver = saver
        self.sess = None
        pass


    def __enter__ (self):
        assert self.sess is None
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        self.sess = tf.Session(config=config, graph=self.graph)
        #self.sess.run(init)
        self.saver.restore(self.sess, self.path)
        pass

    def __exit__ (self, eType, eValue, eTrace):
        self.sess.close()
        self.sess = None
        pass

    def apply (self, images, batch=32):
        if self.sess is None:
            raise Exception('Model.apply must be run within context manager')
        if len(images.shape) == 3:  # grayscale
            images = images.reshape(images.shape + (1,))
            pass

        out = np.zeros(images.shape[0])

        for b in range(0, images.shape[0], batch):
            e = b + batch
            if e > images.shape[0]:
                e = images.shape[0]
            bb = self.sess.run(self.outputs, feed_dict={self.inputs: images[b:e]})
            out[b:e] = bb
        return out
    pass

class Model:
    def __init__ (self, axial, sagittal, coronal, SPACING=1.0):
        self.axial = ViewModel(axial)
        self.sagittal = ViewModel(sagittal)
        self.coronal = ViewModel(coronal)
        self.SPACING = SPACING

    def apply (self, uid):
        case = load_8bit_lungs(uid)
        #TODO: better normalization
        case = case.rescale3D(self.SPACING)

        with self.axial:
            axial = self.axial.apply(case.images)
        with self.sagittal:
            sagittal = self.sagittal.apply(case.transpose(SAGITTAL).images)
        with self.coronal:
            coronal = self.coronal.apply(case.transpose(SAGITTAL).images)
        return axial, sagittal, coronal
    pass

def load_list (path, label, max=None):
    ll = []
    with open(os.path.join(DATA_DIR, path), 'r') as f:
        for l in f:
            ll.append((l.strip(), label))
            pass
        pass
    if max > 0 and len(ll) > max:
        random.shuffle(ll)
        ll = ll[:max]
    return ll


USE=300
try:
    os.mkdir('inter.small')
except:
    pass
if __name__ == '__main__':
    cases = []
    cases.extend(load_list('pos_val_uids', 1, USE))
    cases.extend(load_list('neg_val_uids', 0, USE))
    random.shuffle(cases)

    model = Model('db/axial.resnet_v1_50.small/1000', 'db/sagittal.resnet_v1_50.small/3000', 'db/coronal.resnet_v1_50.small/2000')
    results = []
    for uid, label in cases:
        axi, sag, cor = model.apply(uid)
        axi_hist, _ = np.histogram(axi, density=True, range=(0.0, 1.0))
        sag_hist, _ = np.histogram(sag, density=True, range=(0.0, 1.0))
        cor_hist, _ = np.histogram(cor, density=True, range=(0.0, 1.0))
        axi = list(axi)
        sag = list(sag)
        cor = list(cor)
        axi_hist = list(axi_hist)
        axi_hist.reverse()
        sag_hist = list(sag_hist)
        sag_hist.reverse()
        cor_hist = list(cor_hist)
        cor_hist.reverse()
        print("%d %s" % (label, uid))
        print("%d axial %s" % (label, ' '.join(['%.2f'%x for x in axi_hist])))
        print("%d sagittal %s" % (label, ' '.join(['%.2f'%x for x in sag_hist])))
        print("%d coronal  %s" % (label, ' '.join(['%.2f'%x for x in cor_hist])))
        print('==========')
        result = (label, axi, sag, cor, uid)
        results.append(result)
        with open('inter.small/%s.pkl' % uid, 'wb') as f:
            pickle.dump(result, f)
    pass
    with open('inter.small.pkl', 'wb') as f:
        pickle.dump(results, f)
