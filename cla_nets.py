#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import tensorflow as tf
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim.nets import resnet_v1
from tensorflow.contrib.slim.nets import resnet_utils
from tensorflow.contrib.slim.nets import alexnet
#from tensorflow.contrib.slim.nets.inception_v3 import inception_v3

def alex (X, num_classes=2):
    net, _ = alexnet.alexnet_v2(X, num_classes=2)
    net = tf.identity(net, 'logits')
    return net

#def inception (X, num_classes=2):
#    net, _ = inception_v3(X, num_classes=2)
#    net = tf.identity(net, 'logits')
#    return net

def resnet_v1_50 (X, num_classes=2):
    with tf.name_scope('resnet_v1_50'):
        net, _ = resnet_v1.resnet_v1_50(X,
                                num_classes=num_classes,
                                global_pool = True)
    net = tf.identity(net, 'logits')
    return net


def  resnet_v1_101 (X, num_classes=2):
    with tf.name_scope('resnet_v1_101'):
        net, _ = resnet_v1.resnet_v1_50(X,
                                num_classes=num_classes,
                                global_pool = True)
    net = tf.identity(net, 'logits')
    return net

