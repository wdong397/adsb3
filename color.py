import numpy as np
import logging
from sklearn.mixture import GaussianMixture
from skimage import measure

# detect out-of-scanner values
# i.e. pixels of value -1000/-2000
# return lb, the true lower-bound of pixel values
# so all pixels < lb are out-of-scanner pixels

# histogram:
#   ^_____^___^^^^^^^^^^^^
#    if there are train of > th zeros after V
#   then V is an artificial value

def detect_lb (v, th=10, bins=100):
    # detect isolated peaks
    assert bins > 10
    hist, edges = np.histogram(v, bins=bins)
    L = len(hist)
    L1 = L * 4 / 5   # only detect within this portion of hist
    i = 0
    all = []
    while i < L1:
        if not hist[i]: # skip 0s
            i += 1
            continue
        j = i + 1
        while hist[j] == 0 and j < L:
            j += 1
        train0 = j - i - 1
        if train0 > th:
            all.append((edges[i+1], edges[j], train0))
            pass
        i = j
        pass
    logging.debug(all)
    assert len(all) <= 1
    if not all:
        return np.min(v)
    lb, ub, _ = all[0]
    #print 'th', (lb + ub)/2
    masked = np.ma.masked_less_equal(v, (lb+ub)/2)
    m = masked.min()
    assert m < 100
    return m

def largest_label_volume(im, bg=-1):
    vals, counts = np.unique(im, return_counts=True)
    counts = counts[vals != bg]
    vals = vals[vals != bg]
    if len(counts) > 0:
        return vals[np.argmax(counts)]
    else:
        return None

def segment_lung_mask(image, fill_lung_structures=True, th=-320):

    # not actually binary, but 1 and 2. 
    # 0 is treated as background, which we do not want
    binary_image = np.array(image > th, dtype=np.int8)+1
    labels = measure.label(binary_image)

    # Pick the pixel in the very corner to determine which label is air.
    #   Improvement: Pick multiple background labels from around the patient
    #   More resistant to "trays" on which the patient lays cutting the air 
    #   around the person in half
    background_label = labels[0,0,0]

    #Fill the air around the person
    binary_image[background_label == labels] = 2

    # binary_image
    #   1: lungs
    #   2: other

    # Method of filling the lung structures (that is superior to something like 
    # morphological closing)
    if fill_lung_structures:
        # For every slice we determine the largest solid structure
        for i, axial_slice in enumerate(binary_image):
            axial_slice = axial_slice - 1
            #  0: lungs
            #  1: other
            labeling = measure.label(axial_slice)
            # label with largest volume
            # assuming non-lung tissue is larger
            # so l_max is non-lung
            l_max = largest_label_volume(labeling, bg=0)

            if l_max is not None: #This slice contains some lung
                binary_image[i][labeling != l_max] = 1

    binary_image -= 1 #Make the image actual binary
    binary_image = 1-binary_image # Invert it, lungs are now 1

    # 0: not lung
    # 1: lung

    # Remove other air pockets insided body
    labels = measure.label(binary_image, background=0)
    l_max = largest_label_volume(labels, bg=0)
    if l_max is not None: # There are air pockets
        binary_image[labels != l_max] = 0

    return binary_image

#def detect_lungs (v):
#    hist, edges = np.histogram(v, bins= 50)
#    print hist


