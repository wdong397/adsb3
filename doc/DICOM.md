# DICOM Related

ImagePositionPatient    (0020,0032)
    The x, y, and z coordinates of the upper left hand corner (center of
    the first voxel transmitted) of the image, in mm.
    (More)[http://dicom.nema.org/medical/dicom/2014c/output/chtml/part03/sect_C.7.6.2.html#sect_C.7.6.2.1.1]

    example: [-145.500000\-158.199997\-173.699997]

    When (0010,2210) is absent or is BIPED, as in our case
    (QUADRUPED has different meanings):

    x-axis is increasing to the left hand side of the patient
              (right -> left)
    y-axis is increasing to the posterior
              (front -> back)
    z-axis is increasing toward the head of the patient
              (foot -> head)

    So we shall order the slices by z-axis.

ImageOrientationPatient (0020,0037)
    The direction cosines of the first row and the first column with
    respect to the patient.

    Typical case:
       /
    z /
     /     x
    o -----------------> row            o is ImagePositionPatient
    |
    |       -------
    |      /       \
  y |    0|    x    |0
    |      \_^___^_/
   \|/       feets
   col

    So cosines of row should be  (1, 0, 0)
                  col should be  (0, 1, 0)


Sanity check:
    1.  (0010,2210) Anatomical Orientation Type is not present of has
        the value BIPED; (and QUADRUPED doesn't appear at all)
    2.  ImageOrientationPatient should roughly be (1, 0, 0, 0, 1, 0)


