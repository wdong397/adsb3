#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys
import os
import math
import datetime
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from adsb3 import *
from sklearn.model_selection import KFold
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss
from sklearn.preprocessing import normalize
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier, RandomForestClassifier
from xgboost import XGBClassifier

SPLIT=3

MIN_NODULE=10

do_valid = False
do_fast = False
do_faster = False
do_fastest = False
voff = 0
for voff in range(1, len(sys.argv)):
    if not '--' in sys.argv[voff]:
        break
    if sys.argv[voff] == '--valid':
        do_valid = True
    elif sys.argv[voff] == '--fast':
        do_fast = True
        do_valid = True
    elif sys.argv[voff] == '--faster':
        do_faster = True
        do_valid = True
    elif sys.argv[voff] == '--fastest':
        do_fastest = True
        do_valid = True
    pass
models = sys.argv[voff:]

failed = []
missing = []

POS_FT = 'luna.ns.3c_b8pos'

#PARTITIONS = [(1,1,1),(1, 1, 2),(3,1,1),(1,1,3)]
PARTITIONS = [(1,1,1),(1,1,2),(3,1,1),(1,1,3)]
#PARTITIONS = [(1,1,1),(1, 1, 2),(3,1,1),(1,1,3)]
TOTAL_PARTS = 0
for x, y, z in PARTITIONS:
    TOTAL_PARTS += x * y * z
    pass
print("TOTAL_PARTS: ", TOTAL_PARTS)

def extract (dim, nodules):
    if dim == 2:
        dim = 1
    if len(nodules) == 0 or nodules[0][0] < MIN_NODULE:
        return [0] * dim
    else:
        return nodules[0][2][:dim]

def pyramid (dim, nodules):
    parts = []
    for _ in range(TOTAL_PARTS):
        parts.append([])
    for w, pos, ft in nodules:
        z, y, x = pos
        off = 0
        for LZ, LY, LX in PARTITIONS:
            zi = min(int(math.floor(z * LZ)), LZ-1)
            yi = min(int(math.floor(y * LY)), LY-1)
            xi = min(int(math.floor(x * LX)), LX-1)
            pi = off + (zi * LY + yi) * LX + xi
            off += LZ * LY * LX
            assert pi < off
            parts[pi].append((w, pos, ft))
            pass
        assert off == TOTAL_PARTS
        pass
    ft = []
    for nodules in parts:
        ft.extend(extract(dim, nodules))
        pass
    return ft

def load_all_ft (uid):
    ft = []

    for model in models:
        cached = os.path.join('cache', model, uid + '.pkl')
        if not os.path.exists(cached):
            missing.append(cached)
            ft.append(None)
            continue
        try:
            dim, nodules = load_fts(cached)
            ft.append(pyramid(dim, nodules))
        except:
            failed.append(cached)
            ft.append(None)
            pass
        pass
    return ft

U = []
X = []
Y = []
Xt = []

all_fts = []
if do_fastest:
    for uid, label in STAGE1.fastest:
        fts = load_all_ft(uid)
        all_fts.append((uid, label, fts))
elif do_faster:
    for uid, label in STAGE1.faster:
        fts = load_all_ft(uid)
        all_fts.append((uid, label, fts))
elif do_fast:
    for uid, label in STAGE1.fast:
        fts = load_all_ft(uid)
        all_fts.append((uid, label, fts))
else:
    for uid, label in STAGE1.train:
        fts = load_all_ft(uid)
        all_fts.append((uid, label, fts))

DIMS = [None] * len(models)
# fix None features
for i in range(len(models)):
    dim = 0
    for _, _, fts in all_fts:
        if not fts[i] is None:
            dim = len(fts[i])
            break
        pass
    DIMS[i] = dim
    pass

def merge_fts (fts):
    v = []
    fixed = False
    for i in range(len(fts)):
        if fts[i] is None:
            fixed = True
            v.extend([0] * DIMS[i])
        else:
            assert len(fts[i]) == DIMS[i]
            v.extend(fts[i])
            pass
        pass
    return v, fixed

for uid, label, fts in all_fts:
    v, fixed = merge_fts(fts)
    if do_valid and fixed:
        continue
    U.append(uid)
    Y.append(label)
    X.append(v)

if do_fast or do_faster or do_fastest:
    for uid, _ in STAGE1.test[:1]:
        v, fixed = merge_fts(load_all_ft(uid))
        Xt.append(v)
else:
    for uid, _ in STAGE1.test:
        v, fixed = merge_fts(load_all_ft(uid))
        Xt.append(v)

X = np.array(X, dtype=np.float32)
Y = np.array(Y, dtype=np.float32)
Xt = np.array(Xt, dtype=np.float32)
print('X', X.shape)
print('Y', Y.shape)
print('Xt', Xt.shape)

Y1 = np.sum(Y)
Y0 = len(Y) - Y1
print('neg:', Y0, 'pos:', Y1)

kf = KFold(n_splits=SPLIT, shuffle=True, random_state=88)
y_pred = Y * 0
y_pred_prob = Y * 0
Yt = np.zeros((Xt.shape[0],), dtype=np.float32)

#model = LogisticRegression() #class_weight={0: Y1, 1:Y0})
#model = GradientBoostingClassifier(n_estimators=50, learning_rate=0.1,  max_depth=2, random_state=0)
#model = XGBClassifier(n_estimators=100, learning_rate=0.1,max_depth=2,seed=2016)

#model = GradientBoostingClassifier(n_estimators=1200, learning_rate=0.01,  max_depth=2, random_state=2016)    # 313-0931


model = XGBClassifier(n_estimators=1200, learning_rate=0.01,max_depth=2,seed=2016, subsample=0.9, colsample_bytree=0.4) #, reg_lambda=0.5)
#model = XGBClassifier(n_estimators=300, learning_rate=0.05,max_depth=2,seed=2016, subsample = 0.90, colsample_bytree = 0.40, reg_lambda=1 ) #, reg_lambda=2)

#model = RandomForestClassifier(n_estimators=1000,max_depth=5)
#model = GradientBoostingClassifier(n_estimators=500, learning_rate=0.1,  max_depth=2, random_state=0)    # 313-0931

def pred_wrap (Xin):
    Yout = model.predict_proba(Xin)[:,1]
    #Yout[Yout < 0.01] = 0.01
    #Yout[Yout > 0.99] = 0.99
    return Yout

N = 0
for train, test in kf.split(X):
    X_train, X_test, y_train = X[train,:], X[test,:], Y[train]
    #model.fit(X_train, y_train)
    model.fit(X_train, y_train)

    y_pred[test] = model.predict(X_test)
    #try:
    y_pred_prob[test] = pred_wrap(X_test) #model.predict_proba(X_test)[:,1]
    #Yt += pred_wrap(Xt) #model.predict_proba(Xt)[:,1]
    N += 1
    #except:
    #    y_pred_prob[test] = y_pred[test]
#Yt /= N

model.fit(X, Y)
Yt = pred_wrap(Xt)

try:
    print(classification_report(Y, y_pred, target_names=["0", "1"]))
except:
    pass
#print(zip(Y, y_pred_prob))
print("logloss",log_loss(Y, y_pred_prob))
    #X = X[:, 0:1]
    #print("baseline:")
print("%d corrupt" % len(failed))
for uid in failed:
    print(uid)
    pass
pass
assert N == SPLIT

print("%d missing" % len(missing))
if do_valid:
    sys.exit(0)

for uid in missing:
    print(uid)
    pass
pass

test_uids = [uid for uid, _ in STAGE1.test]
test_ys = list(Yt)

test_meta = zip(test_uids, test_ys)

prefix = os.path.join('stage', datetime.datetime.now().strftime('%m%d-%H%M%S')[1:])
if os.path.exists(prefix + '.submit'):
    print("submission %s already exists, not overwriting." % prefix)
    sys.exit(0)
dump_meta(prefix + '.submit', test_meta)
with open(prefix + '.cmd', 'w') as f:
    f.write(' '.join(sys.argv))
    f.write('\n')
fig = plt.figure()
ax = fig.add_subplot(111)
ax.hist(Yt, bins=20)
fig.savefig(prefix + '.jpg')

COLS = [i+1 for i in range(X.shape[1])]

def libsvm_row (y, x):
    return '%g %s\n' % (y,
            ' '.join(['%d:%.6f' % (c, v) for c, v in zip(COLS, list(x))]))

with open(prefix + '.train', 'w') as f:
    for i in range(len(Y)):
        f.write(libsvm_row(Y[i], list(X[i])))
with open(prefix + '.test', 'w') as f:
    for i in range(Xt.shape[0]):
        f.write(libsvm_row(0, list(Xt[i])))

from xgboost import DMatrix
xgb_matrix = DMatrix(X, label=Y)
xgb_matrix.save_binary(prefix + '.xtrain')
xgb_matrix = DMatrix(Xt)
xgb_matrix.save_binary(prefix + '.xtest')

# generate bad cases
with open(prefix + '.html', 'w') as f:
    f.write('<html><body><table><tr><th>class</th><th>prob</th><th>uid</th></tr>\n')
    v = zip(Y, list(Y * y_pred_prob + (1-Y)*(1- y_pred_prob)), U, X)
    v = sorted(v, key=lambda x: x[1])
    for y, p, uid, x in v:
        ft_txt = ' '.join(['%.4f' % ft for ft in x])
        f.write('<tr><td>%d</td><td>%.4f</td><td><a target="_blank" href="http://localhost/~wdong/lungs/%s.html">%s</a></td><td>%s</td></tr>\n' % (y, p, uid, uid, ft_txt))
        pass
    f.write('</table></body></html>\n')

print('Submission generated:', prefix)

