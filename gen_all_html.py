#!/usr/bin/env python
import os
from adsb3 import *
from papaya import Papaya

class DummyCase:
    def __init__ (self, uid):
        self.uid = uid
        self.path = os.path.join(DATA_DIR, 'stage1', uid)
        pass

pap = Papaya('/home/wdong/public_html/lungs')
for uid, _ in STAGE1.train:
    print uid
    pap.next(DummyCase(uid))
pap.flush()

