#!/usr/bin/env python
import sys
import numpy as np
from adsb3 import *
#from adsb3_lab import *
from gallery import Gallery
import logging


SPACING=2
STEP=10
#gal = Gallery('nnc_stage1/images')

IMAGE_DIR = 'nnc_stage1/images'

C = 0
for uid, label in STAGE1.train:
    if label == 0:
        continue
    C += 1
    if len(glob.glob(os.path.join(IMAGE_DIR, uid + '*'))) > 0:
        print uid, 'done'
        continue
    print uid, C
    try:
        case = Case(uid)
    except:
        try:
            case = LunaCase(uid)
        except:
            logging.error("NOT FOUND: %s" % uid)
            continue
        pass
    case.standardize_color()
    case = case.rescale3D(SPACING)

    assert case.view == AXIAL
    #print case.origin
    z0 = case.origin[0]
    sp = case.spacing[0]
    for i in range(0, len(case.images), STEP):
        off = z0 + sp * i
        name = os.path.join(IMAGE_DIR, '%s.%04d.%.4f.png' % (uid, int(round(off)), off))
        #print name
        cv2.imwrite(name, case.images[i])
        pass
    #lung = lung.rescale3D(SCALE)
    #print case.images.shape[0]

    pass

