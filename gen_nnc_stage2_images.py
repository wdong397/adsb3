#!/usr/bin/env python
import sys
import numpy as np
from adsb3 import *
#from adsb3_lab import *
from gallery import Gallery
import logging

CC = {}

with open('data/nnc_stage1', 'r') as f:
    for l in f:
        uid, off = l.strip().split()
        off = float(off)
        CC.setdefault(uid, []).append(off)
        pass
    pass

IMAGE_DIR = 'nnc_stage2/images'

RANGE = 10
STEP = 4

SPACING=1

for uid, offs in CC.iteritems():
    print uid, offs
    case = load_8bit_lungs_noseg(uid)
    if case is None:
        continue
    #case.standardize_color()
    case = case.rescale3D(SPACING)
    z0 = case.origin[0]
    sp = case.spacing[0]
    slices = set()
    assert case.view == AXIAL
    dz = RANGE/case.spacing[0]
    for off in offs:
        cz = (off - z0)/sp
        bb = max(0, int(cz-dz))
        ee = min(case.images.shape[0], int(cz + dz+1))
        for i in range(bb, ee, STEP):
            slices.add(i)
            pass
        pass

    for i in slices:
        off = z0 + sp * i
        name = os.path.join(IMAGE_DIR, '%s.%04d.%.4f.png' % (uid, int(round(off)), off))
        #print name
        cv2.imwrite(name, case.images[i])
        pass
    #lung = lung.rescale3D(SCALE)
    #print case.images.shape[0]

    pass

