#!/usr/bin/env python
import sys
import numpy as np
from adsb3 import *
#from adsb3_lab import *
from gallery import Gallery
import logging

CC = {}

with open('data/nnc_stage2_kaggle_axial', 'r') as f:
    for l in f:
        l = l.strip().split()
        uid = l[0]
        box = [float(x) for x in l[1:]]
        CC.setdefault(uid, []).append(box)
        pass
    pass

IMAGE_DIR = 'nnc_stage3/images'

STEP = 4

SPACING=1

def ranges2slices (ranges):
    # merge overlapping ranges
    while True:
        out = []
        for b, e in ranges:
            merged = False
            for r in out:
                if e < r[0] or b > r[1]:
                    continue
                r[0], r[1] = min(r[0], b), max(r[1], e)
                merged=True
                break
            if not merged:
                out.append([b,e])
                pass
            pass
        if len(out) == len(ranges):
            break
        ranges = out
        pass
    slices = []
    for b, e in ranges:
        for i in range(b, e, STEP):
            slices.append(i)
            pass
        pass
    return slices

for uid, boxes in CC.iteritems():
    print uid
    case = load_8bit_lungs_noseg(uid)
    if case is None:
        continue
    case = case.rescale3D(SPACING)
    D, H, W = case.images.shape
    assert case.view == AXIAL
    #sagittal_slices = set()
    #coronal_slices = set()
    sagittal_ranges = []
    coronal_ranges = []

    for off, x, y, w, h in boxes:
        bb = int(math.floor(x * W))
        ee = int(math.ceil((x + w)*W))
        sagittal_ranges.append([bb, ee])
        bb = int(math.floor(y * H))
        ee = int(math.ceil((y + h)*H))
        coronal_ranges.append([bb, ee])
        #pass

    sagittal = case.transpose(SAGITTAL)
    #coronal = case.transpose(CORONAL)
    z0 = case.origin[2]
    sp = case.spacing[2]
    for i in ranges2slices(sagittal_ranges):
        off = z0 + sp * i
        name = os.path.join(IMAGE_DIR, '%s.sagittal.%04d.%.4f.png' % (uid, int(round(off)), off))
        #print name
        cv2.imwrite(name, sagittal.images[i])
        pass

    coronal = case.transpose(CORONAL)
    z0 = case.origin[1]
    sp = case.spacing[1]
    for i in ranges2slices(coronal_ranges):
        off = z0 + sp * i
        name = os.path.join(IMAGE_DIR, '%s.coronal.%04d.%.4f.png' % (uid, int(round(off)), off))
        #print name
        cv2.imwrite(name, coronal.images[i])
        pass

    pass

