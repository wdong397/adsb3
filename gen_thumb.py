#!/usr/bin/env python
import subprocess
from jinja2 import Environment, FileSystemLoader
import dicom
from PIL import Image
import cv2
import numpy as np
from adsb3 import *

THUMB_SIZE=180


def make_gif (case):
    frames = []
    for dcm in case.dcms:
        path = os.path.join(case.thumb_path, dcm.sid + '-thumb.jpg')
        frames.append(Image.open(path))
        pass
    frames[0].save(os.path.join(case.thumb_path, 'all.gif'), save_all=True, append_images=frames[1:], duration=0.1, loop=True)
    pass


env = Environment(loader=FileSystemLoader(searchpath="./templates" ))
case_tmpl = env.get_template('case.html')
index_tmpl = env.get_template('index.html')

todo = []
cases = []
#for uid in UIDS:
for uid, _ in STAGE1.samples:
    case = Case(uid)
    try:
        os.makedirs(case.thumb_path)
    except:
        pass
    cases.append({
            'uid': case.uid,
            'slices': len(case.dcms),
            'length': case.length
        })
    for dcm in case.dcms:
        basename = os.path.basename(dcm.dcm.filename)
        dcm_path = os.path.join(case.path, basename)
        jpg_path = os.path.join(case.thumb_path, dcm.sid + '.jpg')
        thumb_path = os.path.join(case.thumb_path, dcm.sid + '-thumb.jpg')
        if True: #not os.path.exists(thumb_path):
            todo.append((dcm_path, jpg_path, thumb_path))
            pass
        pass
    with open(os.path.join(case.thumb_path, 'index.html'), 'w') as f:
        f.write(case_tmpl.render(dcms = case.dcms))
    pass

with open(os.path.join('data/thumb/index.html'), 'w') as f:
    f.write(index_tmpl.render(cases = cases))
    pass

for fro, to1, to2 in todo:
    #print fro, '=>', to
    #subprocess.check_call("convert %s -auto-level -depth 8 %s" % (fro, to), shell=True)
    dcm = dicom.read_file(fro)
    image = dcm.pixel_array.astype(dtype=np.float32)
    #print '+', np.min(image), np.max(image)
    image[image <= 0] = 0
    image = cv2.normalize(image, None, 0, 255, cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    cv2.imwrite(to1, image)
    longside = max(image.shape[:2])
    if longside > THUMB_SIZE:
        rate = 1.0*THUMB_SIZE/longside
        image = cv2.resize(image, None, fx = rate, fy = rate)
    cv2.imwrite(to2, image)
    pass
#for uid in UIDS:
for uid, _ in STAGE1.samples:
    case = Case(uid)
    make_gif(case)
    pass


