// Include standard headers
// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include <vcg/complex/complex.h>
#include <wrap/io_trimesh/import_ply.h>

#include <string>
#include <glog/logging.h>
#include <boost/program_options.hpp>
#include "Camera.h"
#include "TrackBallInteractor.h"

class Vertex; class Edge; class Face;

struct UsedTypes: public vcg::UsedTypes<vcg::Use<::Vertex>::AsVertexType,
                                         vcg::Use<::Edge>::AsEdgeType,
                                         vcg::Use<::Face>::AsFaceType>{};
class Vertex: public vcg::Vertex<::UsedTypes,
                                 vcg::vertex::Coord3f,
                                 vcg::vertex::Normal3f,
                                 vcg::vertex::Color4b> {};
class Face: public vcg::Face<::UsedTypes,
                             vcg::face::VertexRef> {};
class Edge: public vcg::Edge<::UsedTypes> {};
class Mesh: public vcg::tri::TriMesh<std::vector<::Vertex>, std::vector<::Face>, std::vector<::Edge>> {};

using namespace glm;
using namespace std;
using namespace rsmz;

string const VertexShaderCode = R"gl(
#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexColor;

// Output data ; will be interpolated for each fragment.
out vec3 fragmentColor;
// Values that stay constant for the whole mesh.
uniform mat4 MVP;

void main(){

        // Output position of the vertex, in clip space : MVP * position
        gl_Position =  MVP * vec4(vertexPosition_modelspace,1);

        // The color of each vertex will be interpolated
        // to produce the color of each fragment
        fragmentColor = vertexColor;
}
)gl";

string const FragmentShaderCode = R"gl(
#version 330 core

// Interpolated values from the vertex shaders
in vec3 fragmentColor;

// Ouput data
out vec3 color;

void main(){

        // Output color = color specified in the vertex shader,�
        // interpolated between all 3 surrounding vertices
        color = fragmentColor;
}
)gl";

GLuint LoadShaders () {

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file

	// Read the Fragment Shader code from the file
	GLint Result = GL_FALSE;
	int InfoLogLength;


    LOG(INFO) << "Compiling vertex shader...";
    char const *ptr = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &ptr , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    CHECK(Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0){
		string msg;
        msg.resize(InfoLogLength+1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &msg[0]);
        LOG(WARNING) << msg;
	}

    LOG(INFO) << "Compiling fragment shader...";

    ptr = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &ptr , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    CHECK(Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		string msg;
        msg.resize(InfoLogLength+1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &msg[0]);
        LOG(WARNING) << msg;
	}

	// Link the program
	LOG(INFO) << "Linking program";
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);
	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    CHECK(Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		string msg;
        msg.resize(InfoLogLength+1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &msg[0]);
        LOG(WARNING) << msg;
	}
	
	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);
	
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}

glm::mat4 ID      = glm::mat4(1.0f);
glm::mat4 ModelMatrix = glm::mat4(1.0f);


static const int WINDOW_WIDTH = 1024;
static const int WINDOW_HEIGHT = 768;

Camera camera;
TrackBallInteractor trackball;



vector<TrackBallInteractor::CameraMotionType> RIGHT_MODES{
    TrackBallInteractor::NONE, TrackBallInteractor::ARC, TrackBallInteractor::FIRSTPERSON, TrackBallInteractor::PAN, TrackBallInteractor::ROLL, TrackBallInteractor::ZOOM};
int right_mode = 0;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    float length;

    switch(action) {
        case GLFW_PRESS:
            switch(key)
            {
                case GLFW_KEY_ESCAPE:
                    // Exit app on ESC key.
                    glfwSetWindowShouldClose(window, GL_TRUE);
                    break;
                case GLFW_KEY_LEFT_CONTROL:
                case GLFW_KEY_RIGHT_CONTROL:
                    trackball.setSpeed(10.0f);
                    break;
                case GLFW_KEY_LEFT_SHIFT:
                case GLFW_KEY_RIGHT_SHIFT:
                    trackball.setSpeed(1.0f);
                    break;
                case GLFW_KEY_F1:
                    //instance().mAnimator.setAnimation(Animator::ORBIT);
                    break;
                case GLFW_KEY_C:
                    std::cout
                        << "(" << camera.getEye().x
                        << "," << camera.getEye().y
                        << "," << camera.getEye().z << ") "
                        << "(" << camera.getCenter().x
                        << "," << camera.getCenter().y
                        << "," << camera.getCenter().z << ") "
                        << "(" << camera.getUp().x
                        << "," << camera.getUp().y
                        << "," << camera.getUp().z  << ")\n";
                    break;
                case GLFW_KEY_R:
                    // Reset the view.
                    camera.reset();
                    trackball.setCamera(&camera);
                    break;
                case GLFW_KEY_T:
                    // Toogle motion type.
                    trackball.setMotionRightClick(RIGHT_MODES[right_mode]);
                    right_mode += 1;
                    right_mode %= RIGHT_MODES.size();
                    /*
                    if (trackball.getMotionRightClick() ==
                            TrackBallInteractor::FIRSTPERSON) {
                        trackball.setMotionRightClick(
                                TrackBallInteractor::PAN);
                    } else {
                        trackball.setMotionRightClick(
                                TrackBallInteractor::FIRSTPERSON);
                    }
                    */
                    break;
                case GLFW_KEY_X:
                    // Snap view to axis.
                    length = glm::length(camera.getEye() -
                                         camera.getCenter());
                    camera.setEye(length,0,0);
                    camera.setUp(0,1,0);
                    camera.update();
                    trackball.setCamera(& camera);
                    break;
                case GLFW_KEY_Y:
                    length = glm::length(camera.getEye() -
                                         camera.getCenter());
                    camera.setEye(0,length,0);
                    camera.setUp(1,0,0);
                    camera.update();
                    trackball.setCamera(& camera);
                    break;
                case GLFW_KEY_Z:
                    length = glm::length(camera.getEye() -
                                         camera.getCenter());
                    camera.setEye(0,0,length);
                    camera.setUp(1,0,0);
                    camera.update();
                    trackball.setCamera(& camera);
                    break;
                default: break;
            }
            break;
        case GLFW_RELEASE:
            switch(key)
            {
                case GLFW_KEY_LEFT_CONTROL:
                case GLFW_KEY_RIGHT_CONTROL:
                case GLFW_KEY_LEFT_SHIFT:
                case GLFW_KEY_RIGHT_SHIFT:
                    trackball.setSpeed(1.f);
                    break;
            }
            break;
        default: break;
    }
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    switch(action)
    {
        case GLFW_PRESS:
        {
            switch(button)
            {
                case GLFW_MOUSE_BUTTON_LEFT:
                    trackball.setLeftClicked(true);
                    break;
                case GLFW_MOUSE_BUTTON_MIDDLE:
                    trackball.setMiddleClicked(true);
                    break;
                case GLFW_MOUSE_BUTTON_RIGHT:
                    trackball.setRightClicked(true);
                    break;
            }

            double xpos, ypos;
            glfwGetCursorPos(window, & xpos, & ypos);
            trackball.setClickPoint(xpos, ypos);
            break;
        }
        case GLFW_RELEASE:
        {
            switch(button)
            {
                case GLFW_MOUSE_BUTTON_LEFT:
                    trackball.setLeftClicked(false);
                    break;
                case GLFW_MOUSE_BUTTON_MIDDLE:
                    trackball.setMiddleClicked(false);
                    break;
                case GLFW_MOUSE_BUTTON_RIGHT:
                    trackball.setRightClicked(false);
                    break;
            }
            break;
        }
        default: break;
    }
}

void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	trackball.setClickPoint(xpos, ypos);
}

void scroll_callback(GLFWwindow* window, double xpos, double ypos)
{
	trackball.setScrollDirection(xpos + ypos > 0 ? true : false);
}


void setup_input (GLFWwindow *window) {
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetScrollCallback(window, scroll_callback);
	camera.reset();
	trackball.setCamera(&camera);
    trackball.setSpeed(3.0f);
}

glm::mat4 computeMatricesFromInputs() {
	glm::mat4 Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);
	trackball.update();
	return Projection * camera.getMatrix() * ModelMatrix;
}

bool equal (glm::mat4 const &p1, glm::mat4 const &p2) {
    for (unsigned i = 0; i < 4; ++i) {
        for (unsigned j = 0; j < 4; ++j) {
            if (p1[i][j] != p2[i][j]) return false;
        }
    }
    return true;
}

int main( int argc, char **argv )
{
    string input;
    {
        namespace po = boost::program_options;
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help,h", "produce help message.")
            ("input", po::value(&input), "")
            ;

        po::positional_options_description p;
        p.add("input", 1);

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).
                         options(desc).positional(p).run(), vm);
        po::notify(vm);

        if (vm.count("help") || input.empty()) {
            cout << "Usage:" << endl;
            cout << "\t" << argv[0] << " [options] <input> <output>" << endl;
            cout << desc;
            cout << endl;
            return 0;
        }
    }

    vector<vcg::Point3f> vertex_buffer_data;
    vector<vcg::Point3f> color_buffer_data;

    {
        vertex_buffer_data.clear();
        color_buffer_data.clear();
        CHECK(sizeof(vertex_buffer_data[0]) == 12);
        CHECK(sizeof(color_buffer_data[0]) == 12);
        Mesh m;
        int r = vcg::tri::io::ImporterPLY<Mesh>::Open(m, input.c_str());
        CHECK(r==vcg::ply::E_NOERROR) << "Failed to open " << input;

        vcg::tri::UpdateBounding<Mesh>::Box(m);
        auto bbox = m.bbox;
        auto ct = (bbox.min + bbox.max)/2;
        auto sz = bbox.max - bbox.min;
        float L = 1.0/std::max(std::max(sz[0], sz[1]), sz[2]);

        LOG(INFO) << bbox.min[0] << ',' << bbox.min[1] << ',' << bbox.min[2];
        ModelMatrix = glm::scale(ID, glm::vec3(L, L, L)) * glm::translate(ID, glm::vec3(-ct[0], -ct[1], -ct[2]));

        for (auto const &f: m.face) {
            for (int i = 0; i < 3; ++i) {
                auto const *v = f.cV1(i);
                auto p = v->cP();
                vertex_buffer_data.emplace_back(p);
                auto c = v->cC();
                color_buffer_data.emplace_back(c[0], c[1], c[2]);
                color_buffer_data.back() /= 255.0;
            }
        }
        LOG(INFO) << "Loaded " << vertex_buffer_data.size() << " verts";
        for (int i = 0; i < 10; ++i) {
            auto a = vertex_buffer_data[i];
            auto b = color_buffer_data[i];
            LOG(INFO) << a[0] << ',' << a[1] << ',' << a[2] << '/' << b[0] << ',' << b[1] << ',' << b[2]; 
        }
    }

	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Tutorial 04 - Colored Cube", NULL, NULL);
	if( window == NULL ){
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		getchar();
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
    setup_input(window);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS); 

	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders();

	// Get a handle for our "MVP" uniform
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	// glm::mat4 Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);
	// Camera matrix
	/* glm::mat4 View       = glm::lookAt(
								glm::vec3(4,3,-3), // Camera is at (4,3,-3), in World Space
								glm::vec3(0,0,0), // and looks at the origin
								glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)
						   );
                           */
	// Our ModelViewProjection : multiplication of our 3 matrices
	// glm::mat4 MVP        = Projection * View * Model; // Remember, matrix multiplication is the other way around


	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data[0]) * vertex_buffer_data.size(), 
                                  &vertex_buffer_data[0], GL_STATIC_DRAW);

	GLuint colorbuffer;
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(color_buffer_data[0]) * color_buffer_data.size(),
                                  &color_buffer_data[0], GL_STATIC_DRAW);

	double lastTime = glfwGetTime();
    vec3 gOrientation1{0,0,0};
    glm::mat4 old(1.0);
	do{
		double currentTime = glfwGetTime();
		float deltaTime = (float)(currentTime - lastTime); 
		lastTime = currentTime;

		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(programID);

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
        glm::mat4 MVP = computeMatricesFromInputs();
        if (!equal(old, MVP)) {
            cout << "M: {";
            for (unsigned i = 0; i < 4; ++i) {
                for (unsigned j = 0; j < 4; ++j) {
                    cout << MVP[i][j];
                    if ((i != 3) || (j != 3)) cout << ", ";
                }
            }
            cout << "}" << endl;
            old = MVP;
        }
	    glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
		glVertexAttribPointer(
			0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalized?
			0,                  // stride
			(void*)0            // array buffer offset
		);

		// 2nd attribute buffer : colors
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
		glVertexAttribPointer(
			1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
		);


		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, vertex_buffer_data.size() * 3); // 12*3 indices starting at 0 -> 12 triangles

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
		   glfwWindowShouldClose(window) == 0 );

	// Cleanup VBO and shader
	glDeleteBuffers(1, &vertexbuffer);
	glDeleteBuffers(1, &colorbuffer);
	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &VertexArrayID);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}

#include <wrap/ply/plylib.cpp>
#include "TrackBallInteractor.cpp"
#include "Camera.cpp"
