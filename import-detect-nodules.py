#!/usr/bin/env python
import sys
import time
import traceback
import subprocess
import numpy as np
import simplejson as json
import tensorflow as tf
from tensorflow.python.framework import meta_graph
from scipy.ndimage.morphology import grey_dilation, binary_dilation
from skimage import measure
import picpac
from adsb3 import *
import pyadsb3

MAX_NODULES = 20
IMAGE_SIZE = 256

def setGpuConfig (config):
    mem_total = subprocess.check_output('nvidia-smi --query-gpu=memory.total --format=csv,noheader,nounits', shell=True)
    mem_total = float(mem_total)
    frac = 5000.0/mem_total
    print("setting GPU memory usage to %f" % frac)
    if frac < 0.5:
        config.gpu_options.per_process_gpu_memory_fraction = frac
    pass

def logits2prob (v, scope='logits2prob'):
    with tf.name_scope(scope):
        shape = tf.shape(v)    # (?, ?, ?, 2)
        # softmax
        v = tf.reshape(v, (-1, 2))
        v = tf.nn.softmax(v)
        v = tf.reshape(v, shape)
        # keep prob of 1 only
        v = tf.slice(v, [0, 0, 0, 1], [-1, -1, -1, -1])
        # remove trailing dimension of 1
        v = tf.squeeze(v, axis=3)
    return v

class ViewModel:
    def __init__ (self, X, view, name, dir_path, node='logits:0', softmax=True):
        self.name = name
        self.view = view
        paths = glob(os.path.join(dir_path, '*.meta'))
        assert len(paths) == 1
        path = os.path.splitext(paths[0])[0]
        mg = meta_graph.read_meta_graph_file(path + '.meta')
        fts, = tf.import_graph_def(mg.graph_def, name=name,
                            input_map={'images:0':X},
                            return_elements=[node])
        if softmax:
            fts = logits2prob(fts)
        self.fts = fts
        self.saver = tf.train.Saver(saver_def=mg.saver_def, name=name)
        self.loader = lambda sess: self.saver.restore(sess, path)
        pass

MODE_AXIAL = 1
MODE_MIN   = 3

GAP = 5

class Model:
    def __init__ (self, prob_model, prob_mode, channels = 3):
        if channels == 1:
            self.X = tf.placeholder(tf.float32, shape=(None, None, None))
            X4 = tf.expand_dims(self.X, axis=3)
        elif channels == 3:
            self.X = tf.placeholder(tf.float32, shape=(None, None, None, channels))
            X4 = self.X 
        else:
            assert False

        models = []
        models.append(None)
        models.append(ViewModel(X4, AXIAL, 'axial', 'models/%s/axial' % prob_model))
        if prob_mode > MODE_AXIAL:
            models.append(ViewModel(X4, SAGITTAL, 'sagittal', 'models/%s/sagittal' % prob_model))
            models.append(ViewModel(X4, CORONAL, 'coronal', 'models/%s/coronal' % prob_model))
        self.channels = channels
        self.models = models
        self.mode = prob_mode
        pass

    def load (self, sess):
        for m in self.models:
            if m:
                m.loader(sess)
        pass

    def apply (self, sess, views, mask):
        r = []
        #comb = np.ones_like(case.images, dtype=np.float32)
        for m in self.models:
            if m is None:
                r.append(None)
                continue
            cc = views[m.view]
            images = cc.images
            N, H, W = images.shape

            fts = None #np.zeros_like(images, dtype=np.float32)
            margin = 0
            if self.channels == 3:
                margin = GAP

            x = np.zeros((1,H,W,FLAGS.channels), dtype=np.float32)
            for i in range(margin, N-margin):
                if self.channels == 1:
                    x = images[i:(i+1)]
                elif self.channels == 3:
                    x[0,:,:,0] = images[i-GAP]
                    x[0,:,:,1] = images[i]
                    x[0,:,:,2] = images[i+GAP]
                else:
                    assert False
                y, = sess.run([m.fts], feed_dict={self.X:x})
                if fts is None:
                    fts = np.zeros((N,) + y.shape[1:], dtype=np.float32)
                fts[i] = y[0]
            if m.view != AXIAL:
                fts = cc.transpose_array(AXIAL, fts)
            r.append(fts)
            pass
        if self.mode == MODE_AXIAL:
            prob = r[1]
        elif self.mode == MODE_MIN:
            prob = r[1]
            np.minimum(prob, r[2], prob)
            np.minimum(prob, r[3], prob)
        else:
            assert False

        if mask:
            pre_sum = np.sum(prob)
            prob *= mask.images
            post_sum = np.sum(prob)
            logging.info('mask reduction %f' % ((pre_sum-post_sum)/pre_sum))
        return prob
    pass

def dump_nodule (db, ID, nid, vid, images, pos):
    z, y, x = pos
    image = get3c(images, z)
    if image is None:
        return
    H, W, _ = image.shape
    out = np.zeros((IMAGE_SIZE, IMAGE_SIZE, 3), dtype=np.uint16)
    y = y - IMAGE_SIZE/2
    x = x - IMAGE_SIZE/2
    oy = 0
    ox = 0
    h = IMAGE_SIZE
    w = IMAGE_SIZE
    if y < 0:
        oy -= y
        h += y
        y = 0
    if x < 0:
        ox -= x
        w += x
        x = 0
    if y + h > H:
        h = H - y
    if x + w > W:
        w = W - x
    out[oy:(oy+h),ox:(ox+w)] = image[y:(y+h),x:(x+w)]
    buf = cv2.imencode('.tiff', out.astype(np.uint16))[1].tostring()
    #label = (ID * MAX_NODULES + nid) * 3 + vid
    meta = {'index': ID,
            'nodule': nid,
            'view': vid}
    db.append(buf, json.dumps(meta))
    pass

def dump_nodules (db, ID, views, prob, th=0.1):
    binary = prob > th
    labels = measure.label(binary, background=0)
    boxes = measure.regionprops(labels)

    cand = []
    for box in boxes:
        #print prob.shape, fts.shape
        if box.filled_area < 10:
            continue
        z0, y0, x0, z1, y1, x1 = box.bbox
        z = (z0 + z1)/2
        y = (y0 + y1)/2
        x = (x0 + x1)/2

        prob_roi = prob[z0:z1,y0:y1,x0:x1]
        weight_sum = np.sum(prob_roi)
        cand.append((weight_sum, (z, y, x)))
        pass
    cand = sorted(cand, key=lambda x: -x[0])[:MAX_NODULES]
    for i in range(len(cand)):
        z, y, x = cand[i][1]
        dump_nodule(db, ID, i, 0, views[0].images, (z, y, x))
        dump_nodule(db, ID, i, 1, views[1].images, (x, y, z))
        dump_nodule(db, ID, i, 2, views[2].images, (y, z, x))
    pass


flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('prob', 'luna.3c16', 'prob model')      # prob model
flags.DEFINE_string('mask', None, 'mask')
flags.DEFINE_integer('mode', MODE_AXIAL, '')              # use axial instead of min of 3 views
flags.DEFINE_integer('channels', 3, '')
flags.DEFINE_integer('bits', 16, '')
flags.DEFINE_integer('stride', 16, '')
flags.DEFINE_integer('dilate', 10, '')


def main (argv):
    model = Model(FLAGS.prob, FLAGS.mode, FLAGS.channels)
    name = FLAGS.prob
    if FLAGS.mode != MODE_AXIAL:
        name += '_m' + str(FLAGS.mode)
    if FLAGS.channels != 3:
        name += '_c' + str(FLAGS.channels)
    if FLAGS.bits != 16:
        name += '_b' + str(FLAGS.bits)
    if not FLAGS.mask is None:
        name += '_' + FLAGS.mask
    if FLAGS.dilate != 10:
        name += '_d' + str(FLAGS.dilate)

    #name = '%s_%s_%d_%d' % (FLAGS.prob, FLAGS.fts, FLAGS.mode, FLAGS.channels)
    ROOT = os.path.join('/data/ssd/wdong/3c16/', name)
    try_mkdir(ROOT)
    db_path = os.path.join(ROOT, 'all')
    try_remove(db_path)
    db = picpac.Writer(db_path)

    config = tf.ConfigProto()
    setGpuConfig(config)
    with tf.Session(config=config) as sess:
        tf.global_variables_initializer().run()
        model.load(sess)
        ALL = STAGE1.train + STAGE1.test
        for C in range(len(ALL)):
            uid, label = ALL[C]
            start_time = time.time()
            if FLAGS.bits == 8:
                case = load_8bit_lungs_noseg(uid)
            elif FLAGS.bits == 16:
                case = load_16bit_lungs_noseg(uid)
            else:
                assert False
            load_time = time.time()
            mask = None
            if not FLAGS.mask is None:
                try:
                    mask_path = 'maskcache/%s/%s.npz' % (FLAGS.mask, uid)
                    mask = load_mask(mask_path)
                    mask = case.copy_replace_images(mask.astype(dtype=np.float32))
                    mask = mask.rescale3D(SPACING)
                    mask.round_stride(FLAGS.stride)
                    if FLAGS.dilate > 0:
                        #print 'dilate', FLAGS.dilate
                        ksize = FLAGS.dilate * 2 + 1
                        mask.images = grey_dilation(mask.images, size=(ksize, ksize, ksize), mode = 'constant')
                except:
                    traceback.print_exc()
                    logging.error('failed to load mask %s' % mask_path)
                    mask = None

            case = case.rescale3D(SPACING)
            case.round_stride(FLAGS.stride)
            views = [case.transpose(AXIAL),
                     case.transpose(SAGITTAL),
                     case.transpose(CORONAL)]

            prob = model.apply(sess, views, mask)
            #pyadsb3.save_mesh(verts, faces, cache)
            predict_time = time.time()
            dump_nodules(db, C, views, prob)
            print uid, (load_time - start_time), (predict_time - load_time)
        pass

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    tf.app.run()

