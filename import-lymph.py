#!/usr/bin/env python
import sys
import traceback
from glob import glob
import xml.etree.ElementTree as ET
import numpy as np
import logging
import cv2
import simplejson as json
import nibabel as nib
import picpac
from adsb3 import *


def dump_masks (db, gal, case, mask):
    N, H, W = case.images.shape
    C = 0
    for i in range(GAP, N-GAP):
        ma = mask.images[i]
        mas = np.sum(ma)
        image = get3c(case.images, i)
        buf1 = cv2.imencode('.png', image)[1].tostring()
        if mas < 10:
            db.append(0, buf1)
            continue
        #image = case.images[i]
        #cv2.normalize(image, image, 0, 255, cv2.NORM_MINMAX)
        #image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        contours = measure.find_contours(ma, 0.5)
        shapes = []
        for contour in contours:
			#tmp = np.copy(contour[:,0])
			#contour[:, 0] = contour[:, 1]
			#contour[:, 1] = tmp 
			#contour = contour.reshape((1, -1, 2)).astype(np.int32)
			#cv2.polylines(image, contour, True, (0,255,0))
            points = []
            for y, x in contour:
                points.append({'x': 1.0 * x / W,
                              'y': 1.0 * y / H})
            shapes.append({'type':'polygon', 'geometry':{'points': points}})
        anno = {'shapes':shapes}
        buf2 = json.dumps(anno)
        # positive
        db.append(1, buf1, buf2)
        C += 1
        #cv2.imwrite(gal.next(), image)
        pass
    return C

logging.basicConfig(level=logging.INFO)
#for path in glob('data/tcia/tcia-lidc-xml/185/069.xml'):
root = 'db/lymph_%.1f' % SPACING
try_mkdir(root)
path = os.path.join(root, 'axial')
try_remove(path) 
db = picpac.Writer(path) 
C = 0
#gal = Gallery('/home/wdong/public_html/lym')
for path in glob('data/lymph/masks/MED_*'):
    uid = os.path.basename(path)
    try:
        case = Case(uid, regroup=False)
    except:
        traceback.print_exc()
        print 'failed to load', uid
        continue
    case.standardize_color()
    _, H, W = case.images.shape

    mask_path = os.path.join(path, uid+'_mask.nii.gz')
    mask_obj = nib.load(mask_path)
    mask = mask_obj.get_data()
    mask = np.swapaxes(mask, 0, 2)
    print case.images.shape, mask.shape
    mask[mask > 0] = 1

    mask = case.copy_replace_images(mask.astype(np.float32))
    case = case.rescale3D(SPACING)
    mask = mask.rescale3D(SPACING)

    c = dump_masks (db, None, case, mask)
    print path, c

    C += 1
    pass

