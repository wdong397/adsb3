#!/usr/bin/env python
from glob import glob
import xml.etree.ElementTree as ET
import numpy as np
import logging
import cv2
import simplejson as json
import tcia
import picpac
from adsb3 import *

RADIUS = int(round(3 / SPACING))

logging.basicConfig(level=logging.INFO)
#for path in glob('data/tcia/tcia-lidc-xml/185/069.xml'):
try_mkdir('db/small_%.1f'%SPACING)
try_remove('db/small_%.1f/axial' % SPACING)
db = picpac.Writer('db/small_%.1f/axial' % SPACING)
C = 0
for path in glob('data/tcia/tcia-lidc-xml/*/*.xml'):
    anno = tcia.Annotation(path)
    c = 0
    for z, sl in anno.slices.iteritems():
        c += len(sl.non_nodules)
    if c == 0:
        continue
    print anno.uid, c, C, len(anno.slices), path
    try:
        case = LunaCase(anno.uid)
    except:
        print 'failed to load', anno.uid
        continue
    case.standardize_color()
    _, H, W = case.images.shape
    case.rescale3D(SPACING)

    for z, sl in anno.slices.iteritems():
        nonnod = sl.small_nodules
        if len(nonnod) == 0:
            continue
        i = int(round((z - case.origin[0]) / case.spacing[0]))
        image = get3c(case.images, i)
        if  image is None:
            continue
        shapes = []
        for x, y in nonnod:
            shapes.append({'type':'ellipse', 'geometry':{
                                'x': 1.0 * (x-RADIUS) / W,
                                'y': 1.0 * (y-RADIUS) / H,
                                'width': RADIUS * 2.0 / W,
                                'height': RADIUS * 2.0 / H
                            }})
        anno = {'shapes': shapes}
        buf1 = cv2.imencode('.png', image)[1].tostring()
        buf2 = json.dumps(anno)
        db.append(buf1, buf2)
        pass
    C += 1
    pass

