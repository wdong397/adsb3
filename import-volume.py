#!/usr/bin/env python
import sys
import random
import glob
import subprocess
from tqdm import tqdm
from adsb3 import *
import cv2
import picpac
import cubic_wrap

ALL = {}

def import_half (half, n):
    pos = [(uid, label) for (uid, label) in half if label > 0]
    neg = [(uid, label) for (uid, label) in half if label == 0]
    cubic_wrap.import_db('/data/ssd/wdong/vol_%.1f/half%d.pos' % (SPACING, n), pos)
    cubic_wrap.import_db('/data/ssd/wdong/vol_%.1f/half%d.neg' % (SPACING, n), neg)
    for uid, label in pos + neg:
        ALL[uid] = label
        pass

import_half(STAGE1.half0, 0)
import_half(STAGE1.half1, 1)

for uid, label in STAGE1.train:
    assert uid in ALL
    assert ALL[uid] == label


