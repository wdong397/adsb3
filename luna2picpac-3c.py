#!/usr/bin/env python
import sys
import json
import subprocess
from multiprocessing import Pool
#from tqdm import tqdm
from adsb3 import *
#import color
import cv2
import picpac

#SPACING=0.7421879768371582

#SPACING *= 2
MAX = -1 #100 #None #50
#MAX = None

GAP = 5

def get3c (images, i):
    if i < GAP:
        return None
    if i + GAP >= images.shape[0]:
        return None
    a = images[i-GAP]
    b = images[i]
    c = images[i+GAP]
    c3 = np.zeros(a.shape + (3,), dtype=np.float32)
    c3[:,:,0] = a
    c3[:,:,1] = b
    c3[:,:,2] = c
    return c3

def dump (db, dbneg, case):
    images = case.images
    N, H, W = images.shape
    by_frame = {}
    neg = set(range(N))
    for anno in case.picpac_anno():
        for j, x, y, rx, ry in anno:
            r = max(W * rx, H * ry) #* SPACING
            print '\t', j, x, y, rx, ry, r
            if r < 1:   # less than 
                continue
            try:
                neg.remove(j)
            except:
                pass
            one = by_frame.setdefault(j, [])
            one.append({
                    'type': 'ellipse',
                    'geometry': {
                        'x': x - rx,
                        'y': y - ry,
                        'width': rx * 2,
                        'height': ry * 2
                    }
                })
            pass
        pass
    for j, shapes in by_frame.iteritems():
        image = get3c(images, j)
        if image is None:
            continue
        anno = {"shapes": shapes}
        buf1 = cv2.imencode('.png', image)[1].tostring()
        buf2 = json.dumps(anno)
        db.append(buf1, buf2)
    #for j in neg:
    #    image = images[j]
    #    buf1 = cv2.imencode('.png', image)[1].tostring()
    #    dbneg.append(0, buf1)
    pass

#subprocess.check_call('rm -rf db/luna3c/*', shell=True)
db1 = picpac.Writer('/data/ssd/wdong/3c/luna/axial_pos')
#db1neg = picpac.Writer('db/luna3c/axial_neg')
db2 = picpac.Writer('/data/ssd/wdong/3c/luna/sagittal_pos')
#db2neg = picpac.Writer('db/luna3c/sagittal_neg')
db3 = picpac.Writer('/data/ssd/wdong/3c/luna/coronal_pos')
#db3neg = picpac.Writer('db/luna3c/coronal_neg')
uids = LUNA_ANNO.keys()
#if MAX and (MAX < len(uids)):
#    uids = uids[:MAX]

uids = uids[:MAX]

print 'CASES:', len(uids)
pool = Pool(None)
for chunk in chunks(range(len(uids)), 32):
    chunk = [uids[i] for i in chunk]
    cases = pool.map(load_8bit_lungs_noseg, chunk)
    for case in cases:
        print case.uid
        case = case.rescale3D(SPACING)
        print '  AXIAL'
        dump(db1, None, case)
        print '  SAGITTAL'
        dump(db2, None, case.transpose(SAGITTAL))
        print '  CORONAL'
        dump(db3, None, case.transpose(CORONAL))
    pass

