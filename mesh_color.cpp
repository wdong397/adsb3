#include <boost/program_options.hpp>
#include "mesh.h"

int main( int argc, char **argv )
{
    MeshModelParams params;
    string input;
    string output;
    {
        namespace po = boost::program_options;
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help,h", "produce help message.")
            ("input", po::value(&input), "")
            ("output", po::value(&output), "")
            ("smooth", po::value(&params.smooth)->default_value(10), "")
            ("sample", po::value(&params.sample)->default_value(10), "")
            ;

        po::positional_options_description p;
        p.add("input", 1);
        p.add("output", 1);

        po::variables_map vm;
        po::store(po::command_line_parser(argc, argv).
                         options(desc).positional(p).run(), vm);
        po::notify(vm);

        if (vm.count("help") || input.empty() || output.empty()) {
            cout << "Usage:" << endl;
            cout << "\t" << argv[0] << " [options] <input> <output>" << endl;
            cout << desc;
            cout << endl;
            return 0;
        }
    }
    Mesh m;
    int r = vcg::tri::io::ImporterPLY<Mesh>::Open(m, input.c_str());
    CHECK(r==vcg::ply::E_NOERROR) << "Failed to open " << input;
    MeshModel model(params);
    model.apply(m);
    vcg::tri::io::ExporterPLY<Mesh>::Save(m, output.c_str(), vcg::tri::io::SAVE_MASK);
    return 0;
}

