#!/usr/bin/env python
import sys
import random
import glob
import subprocess
from tqdm import tqdm
from adsb3 import *
import cv2
import picpac

MAX=None

#subprocess.check_call('rm -rf db/neg3c/axial.pic db/neg3c/sagittal.pic db/neg3c/coronal.pic', shell=True)

axial_db = picpac.Writer('/data/ssd/wdong/3c/neg/axial.pic')
sagittal_db = picpac.Writer('/data/ssd/wdong/3c/neg/sagittal.pic')
coronal_db = picpac.Writer('/data/ssd/wdong/3c/neg/coronal.pic')

GAP=5

def get3c (images, i):
    if i < GAP:
        return None
    if i + GAP >= images.shape[0]:
        return None
    a = images[i-GAP]
    b = images[i]
    c = images[i+GAP]
    c3 = np.zeros(a.shape + (3,), dtype=np.float32)
    c3[:,:,0] = a
    c3[:,:,1] = b
    c3[:,:,2] = c
    return c3

def dump (case, db):
    N = case.images.shape[0]
    for o in range(0, N, 2):
        image = get3c(case.images, o)
        if image is None:
            continue
        buf = cv2.imencode('.png', image)[1].tostring()
        db.append(0, buf)
        pass
	pass

uids = [uid for uid, label in STAGE1.train if label == 0]
if not MAX is None:
    random.shuffle(uids)
    uids = uids[:MAX]

for uid in uids:
    print uid
    case = load_8bit_lungs_noseg(uid)
    case = case.rescale3D(SPACING)
    dump(case, axial_db)
    dump(case.transpose(SAGITTAL), sagittal_db)
    dump(case.transpose(CORONAL), coronal_db)
    pass

