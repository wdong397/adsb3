#!/usr/bin/env python
import sys
import random
import glob
import subprocess
from tqdm import tqdm
from adsb3 import *
import cv2
import picpac

MAX=None

subprocess.check_call('rm -rf db/neg.ns/axial.pic db/neg.ns/sagittal.pic db/neg.ns/coronal.pic', shell=True)

axial_db = picpac.Writer('db/neg.ns/axial.pic')
sagittal_db = picpac.Writer('db/neg.ns/sagittal.pic')
coronal_db = picpac.Writer('db/neg.ns/coronal.pic')

def dump (case, db):
    N = case.images.shape[0]
    for o in range(0, N, 2):
        image = case.images[o]
        buf = cv2.imencode('.png', image)[1].tostring()
        db.append(0, buf)
        pass
	pass

uids = [uid for uid, label in STAGE1.train if label == 0]
if not MAX is None:
    random.shuffle(uids)
    uids = uids[:MAX]

for uid in uids:
    print uid
    case = load_8bit_lungs_noseg(uid)
    case = case.rescale3D(SPACING)
    dump(case, axial_db)
    dump(case.transpose(SAGITTAL), sagittal_db)
    dump(case.transpose(CORONAL), coronal_db)
    pass

