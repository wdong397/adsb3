from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import subprocess
from glob import glob
import tensorflow as tf
from tensorflow.python.framework import meta_graph
from adsb3 import *

def setGpuConfig (config):
    mem_total = subprocess.check_output('nvidia-smi --query-gpu=memory.total --format=csv,noheader,nounits', shell=True)
    mem_total = float(mem_total)
    frac = 6000.0/mem_total
    print("setting GPU memory usage to %f" % frac)
    if frac < 0.5:
        config.gpu_options.per_process_gpu_memory_fraction = frac
    pass


def logits2prob (v, scope='logits2prob', scale=None):
    with tf.name_scope(scope):
        shape = tf.shape(v)    # (?, ?, ?, 2)
        # softmax
        v = tf.reshape(v, (-1, 2))
        v = tf.nn.softmax(v)
        v = tf.reshape(v, shape)
        # keep prob of 1 only
        v = tf.slice(v, [0, 0, 0, 1], [-1, -1, -1, -1])
        # remove trailing dimension of 1
        v = tf.squeeze(v, axis=3)
        if scale:
            v *= scale
    return v

def import_meta_graph (dir_path, inputs, name=None, softmax=False, features=False):
    paths = glob(os.path.join(dir_path, '*.meta'))
    assert len(paths) == 1
    path = os.path.splitext(paths[0])[0]
    mg = meta_graph.read_meta_graph_file(path + '.meta')
    logits = None
    fts = None
    if features:
        fts, = tf.import_graph_def(mg.graph_def, name=name,
                            input_map={'images:0':inputs},
                            return_elements=['fts:0'])
    else:
        logits, = tf.import_graph_def(mg.graph_def, name=name,
                            input_map={'images:0':inputs},
                            return_elements=['logits:0'])
        if softmax:
            logits = logits2prob(logits)
    saver = tf.train.Saver(saver_def=mg.saver_def, name=name)
    return logits, fts, lambda sess: saver.restore(sess, path)

class ViewModel:
    def __init__ (self, view, name, path, channels=1, features=False):
        self.name = name
        self.view = view
        if channels == 1:
            self.X = tf.placeholder(tf.float32, shape=(None, None, None), name="%s_images"%name)
            X4 = tf.expand_dims(self.X, axis=3)
        elif channels == 3:
            self.X = tf.placeholder(tf.float32, shape=(None, None, None, channels), name="%s_images"%name)
            X4 = self.X 
        else:
            assert False
        self.prob, self.fts, self.loader = import_meta_graph(path, X4, name, softmax=True, features=features)
        pass

class Models:
    def __init__ (self, model, batch):
        models = []
        #for i in range(len(VIEWS)):
        #    name = VIEW_NAMES[i]
        #    models.append(ViewModel(VIEWS[i], name, 'models/luna/' + name))
        #    pass
        # intentionally using unmatching models
        models.append(ViewModel(AXIAL, 'axial', 'models/%s/axial' % model))
        models.append(ViewModel(SAGITTAL, 'sagittal', 'models/%s/sagittal' % model))
        models.append(ViewModel(CORONAL, 'coronal', 'models/%s/coronal' % model))
        self.models = models
        self.batch = batch
        pass

    def load (self, sess):
        for m in self.models:
            m.loader(sess)
            pass
        pass

    def apply (self, sess, case):
        r = []
        #comb = np.ones_like(case.images, dtype=np.float32)
        views = [case.transpose(AXIAL),
                 case.transpose(SAGITTAL),
                 case.transpose(CORONAL)]
        for m in self.models:
            cc = views[m.view]
            images = cc.images
            prob = np.zeros_like(images, dtype=np.float32)
            N = images.shape[0]
            for b in range(0, N, self.batch):
                e = min(N, b + self.batch)
                x = images[b:e]
                y, = sess.run([m.prob], feed_dict={m.X:x})
                prob[b:e] = y[:,:,:]
            axial_prob = cc.transpose_array(AXIAL, prob)
            r.append(axial_prob)
            pass
        return r
    pass


