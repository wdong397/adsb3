#!/usr/bin/env python
import sys
import random
import simplejson as json
import glob
import subprocess
from tqdm import tqdm
from adsb3 import *
import color
import cv2
import picpac

VAL = 20


def load_annotation (view):
    path = 'data/nnc_kaggle_%s' % view
    lookup = {}
    with open(path, 'r') as f:
        for l in f:
            l = l.strip().split()
            uid = l[0]
            anno = [float(x) for x in l[1:]]
            lookup.setdefault(uid, []).append(anno)
            pass
        pass
    return lookup

def load_all_annotations ():
    axial = load_annotation('axial')
    sagittal = load_annotation('sagittal')
    coronal = load_annotation('coronal')
    lookup = {}
    for uid, annos in axial.iteritems():
        lookup.setdefault(uid, [[],[],[]])[0] = annos
    for uid, annos in sagittal.iteritems():
        lookup.setdefault(uid, [[],[],[]])[1] = annos
    for uid, annos in coronal.iteritems():
        lookup.setdefault(uid, [[],[],[]])[2] = annos
    return lookup

ANNO = load_all_annotations ()

subprocess.check_call('rm -rf db/nnc/axial.pic db/nnc/sagittal.pic db/nnc/coronal.pic', shell=True)

axial_db = picpac.Writer('db/nnc/axial.pic')
sagittal_db = picpac.Writer('db/nnc/sagittal.pic')
coronal_db = picpac.Writer('db/nnc/coronal.pic')

def dump (case, origin, annos, db):
    sp = case.spacing[0]
	# TODO: interpolate
    for off, x, y, w, h in annos:
        o = int(round((off - origin) /  sp))
        image = case.images[o]
        buf = cv2.imencode('.png', image)[1].tostring()
        buf2 = json.dumps({'shapes':[{
                   'type': 'ellipse',
                    'geometry': {
                        'x': x, 
                        'y': y, 
                        'width': w,
                        'height': h
                    }}]})
        db.append(buf, buf2)
        pass
	pass

for uid, annos in ANNO.iteritems():
    print uid
    axial, sagittal, coronal = annos
    case = load_8bit_lungs(uid)
    case = case.rescale3D(SPACING)
    dump(case, case.origin[0], axial, axial_db)
    dump(case.transpose(SAGITTAL), case.origin[2], sagittal, sagittal_db)
    dump(case.transpose(CORONAL), case.origin[1], coronal, coronal_db)
    pass

