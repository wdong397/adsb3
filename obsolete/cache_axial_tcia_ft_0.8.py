#!/usr/bin/env python
import sys
import numpy as np
import tensorflow as tf
import time
import subprocess
from scipy.ndimage.morphology import grey_dilation, binary_dilation
from skimage import measure
from adsb3 import *
import cPickle as pickle
import nets

SPACING = 0.8

GAP = 5

def one_nodule (box, prob, fts):
    #print prob.shape, fts.shape
    z0, y0, x0, z1, y1, x1 = box.bbox
    #ft.append((z1-z0)*(y1-y0)*(x1-x0))
    prob_roi = prob[z0:z1,y0:y1,x0:x1]
    fts_roi = fts[z0:z1,y0:y1,x0:x1,:]
    prob_sum = np.sum(prob_roi)
    fts_sum = np.sum(fts_roi, axis=(0,1,2))
    unit = SPACING * SPACING * SPACING
    return [prob_sum * unit] + list(fts_sum/prob_sum)

def extract_ft (prob, fts, th=0.05, ext=2):
    prob4 = np.reshape(prob, prob.shape + (1,))
    assert prob4.base is prob
    fts = np.clip(fts, 0, 6)
    fts *= prob4
    binary = prob > th
    k = int(round(ext / SPACING))
    binary = binary_dilation(binary, iterations=k)
    labels = measure.label(binary, background=0)
    boxes = measure.regionprops(labels)

    ret = []
    for box in boxes:
        ret.append(one_nodule(box, prob, fts))
        pass
    return sorted(ret, key=lambda x: -x[0])


class Models3c:
    def __init__ (self, fts_model, prob_model = 'luna.ns.3c'):
        self.fts = nets.ViewModel(AXIAL, 'axial_fts', 'models/%s/axial' % fts_model, 3, features=True)
        self.prob = nets.ViewModel(AXIAL, 'axial_prob', 'models/%s/axial' % prob_model, 3)
        pass

    def load (self, sess):
        self.prob.loader(sess)
        self.fts.loader(sess)
        pass

    def apply (self, sess, case):
        images = case.images
        N, H, W = images.shape
        prob = np.zeros((N, H, W), dtype=np.float32)
        fts = np.zeros((N, H, W, 7), dtype=np.float32)
        print 'shape', N, H, W

        x = np.zeros((1,H,W,3), dtype=np.float32)
        for i in range(0+GAP, N-GAP):
            x[0,:,:,0] = images[i-GAP]
            x[0,:,:,1] = images[i]
            x[0,:,:,2] = images[i+GAP]
            #y, z = sess.run([self.prob.prob, self.fts.fts], feed_dict={self.prob.X:x, self.fts.X:x})
            y, z = sess.run([self.prob.prob, self.fts.fts], feed_dict={self.prob.X:x, self.fts.X:x})
            #y, = sess.run([self.prob.prob], feed_dict={self.prob.X:x})
            #z, = sess.run([self.fts.fts], feed_dict={self.fts.X:x})
            prob[i] = y[0,:,:]
            fts[i] = z[0,:,:,:]
        return prob, fts
    pass

flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('model', 'tcia.ft', '')

def list2str (x):
    assert len(x) == 8
    return ' '.join(['%.4f' % v for v in x])

def main (argv):
    argv = argv[1:]
    if len(argv) < 1:
        argv = None
    assert FLAGS.model
    model = Models3c(FLAGS.model)
    ROOT = os.path.join('cache', FLAGS.model+'-0.8')
    try:
        os.mkdir(ROOT)
    except:
        pass

    config = tf.ConfigProto()
    nets.setGpuConfig(config)
    with tf.Session(config=config) as sess:
        tf.global_variables_initializer().run()
        model.load(sess)
        if argv:
            uids = argv
        else:
            uids = [uid for uid, _ in STAGE1.train + STAGE1.test]
        for uid in uids:
            cache = os.path.join(ROOT, uid + '.npz.ft')
            if (not argv is None) or not os.path.exists(cache):
                subprocess.check_call('rm -rf %s.*' % cache, shell=True)
                with open(cache, 'wb') as f:
                    pass
                start_time = time.time()
                case = load_8bit_lungs_noseg(uid)
                case = case.rescale3D(SPACING)
                print case.images.shape
                case.round_stride()
                load_time = time.time()
                prob, fts = model.apply(sess, case)
                predict_time = time.time()

                #np.minimum(lb1, rr[1], lb1)
                #np.minimum(lb1, rr[2], lb1)
                fts = extract_ft(prob, fts)
                line = '+%s %d %s\n' % (uid, len(fts), ' '.join([list2str(x) for x in fts]))
                with open(cache, 'w') as f:
                    f.write(line)
                pass
                save_time = time.time()
                print uid, (load_time - start_time), (predict_time - load_time), (save_time - predict_time)
            else:
                print uid, 'done'
        pass

if __name__ == '__main__':
    tf.app.run()

