#!/usr/bin/env python
import sys
import numpy as np
import tensorflow as tf
import time
from skimage import measure
from adsb3 import *
import cPickle as pickle
import nets

flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('model', None, '')
flags.DEFINE_integer('batch', 32, '')

class ViewModel:
    def __init__ (self, view, name, path):
        self.name = name
        self.view = view
        self.X = tf.placeholder(tf.float32, shape=(None, None, None), name="images")
        X4 = tf.expand_dims(self.X, axis=3)
        self.prob, self.loader = nets.import_meta_graph(path, X4, name, softmax=True)
        pass

class Model:
    def __init__ (self, model):
        models = []
        #for i in range(len(VIEWS)):
        #    name = VIEW_NAMES[i]
        #    models.append(ViewModel(VIEWS[i], name, 'models/luna/' + name))
        #    pass
        # intentionally using unmatching models
        models.append(ViewModel(AXIAL, 'axial', 'models/%s/axial' % model))
        models.append(ViewModel(SAGITTAL, 'sagittal', 'models/%s/sagittal' % model))
        models.append(ViewModel(CORONAL, 'coronal', 'models/%s/coronal' % model))
        self.models = models
        pass

    def load (self, sess):
        for m in self.models:
            m.loader(sess)
            pass
        pass

    def apply (self, sess, case):
        r = []
        #comb = np.ones_like(case.images, dtype=np.float32)
        views = [case.transpose(AXIAL),
                 case.transpose(SAGITTAL),
                 case.transpose(CORONAL)]
        for m in self.models:
            cc = views[m.view]
            images = cc.images
            prob = np.zeros_like(images, dtype=np.float32)
            N = images.shape[0]
            for b in range(0, N, FLAGS.batch):
                e = min(N, b + FLAGS.batch)
                x = images[b:e]
                y, = sess.run([m.prob], feed_dict={m.X:x})
                prob[b:e] = y[:,:,:]
            axial_prob = cc.transpose_array(AXIAL, prob)
            r.append(axial_prob)
            pass
        return r
    pass

def main (_):
    assert FLAGS.model
    model = Model(FLAGS.model)
    ROOT = os.path.join('cache/LUNA', FLAGS.model)
    try:
        os.mkdir(ROOT)
    except:
        pass

    config = tf.ConfigProto()
    with tf.Session(config=config) as sess:
        tf.global_variables_initializer().run()
        model.load(sess)
        for uid in LUNA_DIR_LOOKUP.iterkeys():
            cache = os.path.join(ROOT, uid)
            if not os.path.exists(cache):
                with open(cache, 'wb') as f:
                    pass
                start_time = time.time()
                case = load_8bit_lungs(uid)
                case = case.rescale3D(SPACING)
                case.round_stride()
                load_time = time.time()
                rr = model.apply(sess, case)
                predict_time = time.time()

                lb1 = rr[0]
                np.minimum(lb1, rr[1], lb1)
                np.minimum(lb1, rr[2], lb1)

                np.savez_compressed(cache, lb1)
                save_time = time.time()
                print uid, (load_time - start_time), (predict_time - load_time), (save_time - predict_time)
            else:
                print uid, 'done'
        pass

if __name__ == '__main__':
    tf.app.run()

