#!/usr/bin/env python
import sys
import numpy as np
import tensorflow as tf
import time
import subprocess
from skimage import measure
from adsb3 import *
import cPickle as pickle
import nets

flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('model', None, '')
flags.DEFINE_integer('batch', 32, '')

def main (argv):
    argv = argv[1:]
    if len(argv) < 1:
        argv = None
    assert FLAGS.model
    model = nets.Models(FLAGS.model, FLAGS.batch)
    ROOT = os.path.join('cache', FLAGS.model)
    try:
        os.mkdir(ROOT)
    except:
        pass

    config = tf.ConfigProto()
    with tf.Session(config=config) as sess:
        tf.global_variables_initializer().run()
        model.load(sess)
        if argv:
            uids = argv
        else:
            uids = [uid for uid, _ in STAGE1.train + STAGE1.test]
        for uid in uids:
            cache = os.path.join(ROOT, uid)
            if (not argv is None) or not os.path.exists(cache):
                subprocess.check_call('rm -rf %s.*' % cache, shell=True)
                with open(cache, 'wb') as f:
                    pass
                start_time = time.time()
                case = load_8bit_lungs(uid)
                case = case.rescale3D(SPACING)
                print case.images.shape
                case.round_stride()
                load_time = time.time()
                rr = model.apply(sess, case)
                predict_time = time.time()

                lb1 = rr[0]
                np.minimum(lb1, rr[1], lb1)
                np.minimum(lb1, rr[2], lb1)

                np.savez_compressed(cache, lb1)
                save_time = time.time()
                print uid, (load_time - start_time), (predict_time - load_time), (save_time - predict_time)
            else:
                print uid, 'done'
        pass

if __name__ == '__main__':
    tf.app.run()

