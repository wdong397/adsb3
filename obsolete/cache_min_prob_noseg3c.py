#!/usr/bin/env python
import sys
import numpy as np
import tensorflow as tf
import time
import subprocess
from skimage import measure
from adsb3 import *
import cPickle as pickle
import nets

GAP=5

class Models3c:
    def __init__ (self, model):
        models = []
        #for i in range(len(VIEWS)):
        #    name = VIEW_NAMES[i]
        #    models.append(ViewModel(VIEWS[i], name, 'models/luna/' + name))
        #    pass
        # intentionally using unmatching models
        models.append(nets.ViewModel(AXIAL, 'axial', 'models/%s/axial' % model, 3))
        models.append(nets.ViewModel(SAGITTAL, 'sagittal', 'models/%s/sagittal' % model, 3))
        models.append(nets.ViewModel(CORONAL, 'coronal', 'models/%s/coronal' % model, 3))
        self.models = models
        pass

    def load (self, sess):
        for m in self.models:
            m.loader(sess)
            pass
        pass

    def apply (self, sess, case):
        r = []
        #comb = np.ones_like(case.images, dtype=np.float32)
        views = [case.transpose(AXIAL),
                 case.transpose(SAGITTAL),
                 case.transpose(CORONAL)]
        for m in self.models:
            cc = views[m.view]
            images = cc.images
            N, H, W = images.shape
            prob = np.zeros_like(images, dtype=np.float32)
            print 'shape', N, H, W

            x = np.zeros((1,H,W,3), dtype=np.float32)
            for i in range(0+GAP, N-GAP):
                x[0,:,:,0] = images[i-GAP]
                x[0,:,:,1] = images[i]
                x[0,:,:,2] = images[i+GAP]
                y, = sess.run([m.prob], feed_dict={m.X:x})
                prob[i] = y[:,:,:]
            axial_prob = cc.transpose_array(AXIAL, prob)
            r.append(axial_prob)
            pass
        return r
    pass


flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('model', None, '')

def main (argv):
    argv = argv[1:]
    if len(argv) < 1:
        argv = None
    assert FLAGS.model
    model = Models3c(FLAGS.model)
    ROOT = os.path.join('cache', FLAGS.model)
    try:
        os.mkdir(ROOT)
    except:
        pass

    config = tf.ConfigProto()
    nets.setGpuConfig()
    with tf.Session(config=config) as sess:
        tf.global_variables_initializer().run()
        model.load(sess)
        if argv:
            uids = argv
        else:
            uids = [uid for uid, _ in STAGE1.train + STAGE1.test]
        for uid in uids:
            cache = os.path.join(ROOT, uid)
            if (not argv is None) or not os.path.exists(cache):
                subprocess.check_call('rm -rf %s.*' % cache, shell=True)
                with open(cache, 'wb') as f:
                    pass
                start_time = time.time()
                case = load_8bit_lungs_noseg(uid)
                case = case.rescale3D(SPACING)
                print case.images.shape
                case.round_stride()
                load_time = time.time()
                rr = model.apply(sess, case)
                predict_time = time.time()

                lb1 = rr[0]
                np.minimum(lb1, rr[1], lb1)
                np.minimum(lb1, rr[2], lb1)

                np.savez_compressed(cache, lb1)
                save_time = time.time()
                print uid, (load_time - start_time), (predict_time - load_time), (save_time - predict_time)
            else:
                print uid, 'done'
        pass

if __name__ == '__main__':
    tf.app.run()

