#!/usr/bin/env python
import sys
import numpy as np
import time
from adsb3 import *
from scipy.ndimage.morphology import grey_dilation, binary_dilation
#from baseline2 import *
import argparse

def extend (z0, z1, Z, ext = 20):
    zx = ext
    z0 = max(0, z0 - zx)
    z1 = min(Z, z1 + zx)
    return z0, z1

def extract (images, prob, box, ext = 20):
    Z, Y, X = images.shape
    z0, y0, x0, z1, y1, x1 = box.bbox
    z0, z1 = extend(z0, z1, Z, ext)
    y0, y1 = extend(y0, y1, Y, ext)
    x0, x1 = extend(x0, x1, X, ext)
    images = images[z0:z1,y0:y1,x0:x1]
    prob = prob[z0:z1,y0:y1,x0:x1]
    print '\t', box.area
    return [images, prob]

def cache_nodule (path, images, prob, th=0.05, ext=2):

    binary = prob > th
    k = int(round(ext / SPACING))
    binary = binary_dilation(binary, iterations=5)
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    boxes = measure.regionprops(labels)

    arrays = []
    for box in boxes:
        arrays.extend(extract(images, prob, box))
        pass
    np.savez(path, *arrays)
    pass

parser = argparse.ArgumentParser(description='')
parser.add_argument('model', nargs=1)
args = parser.parse_args()

model = args.model[0]

#case = CaseBase()
#case.view = AXIAL
#ft = open('baseline2.ft', 'w')

ROOT = os.path.join('cache', model)
ROOTO = os.path.join('nodules', model)

case = CaseBase()
case.view = AXIAL
for path in glob(os.path.join(ROOT, '*.npz')):
    uid = os.path.splitext(os.path.basename(path))[0]
    output = os.path.join(ROOTO, uid + '.npz')
    if os.path.exists(output):
        continue
    open(output, 'w').close()
    start_time = time.time()
    try:
        case = load_8bit_lungs(uid)
        print uid
        case = case.rescale3D(SPACING)
        case.round_stride()
        data = np.load(path)
        lb = data['arr_0']
    except:
        print 'failed to load', uid
        continue
    load_time = time.time()
    cache_nodule(output, case.images, lb)
    pass

