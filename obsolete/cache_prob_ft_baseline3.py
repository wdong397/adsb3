#!/usr/bin/env python
import sys
import numpy as np
import time
from adsb3 import *
from baseline3 import *
import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('model', nargs=1)
args = parser.parse_args()

model = args.model[0]

#case = CaseBase()
#case.view = AXIAL
#ft = open('baseline2.ft', 'w')

ROOT = os.path.join('cache', model)
ROOTO = os.path.join('cache', model + '.b3')
try:
    os.mkdir(ROOTO)
except:
    pass

case = CaseBase()
case.view = AXIAL
for path in glob(os.path.join(ROOT, '*.npz')):
    uid = os.path.basename(path)
    output = os.path.join(ROOTO, uid + '.ft')
    if os.path.exists(output):
        continue
    open(output, 'w').close()
    start_time = time.time()
    try:
        print path
        data = np.load(path)
        lb = data['arr_0']
    except:
        print 'failed to load', uid
        continue
    load_time = time.time()
    views = [lb]
    fts = extract_ft(views)
    line = '%s %s\n' % (uid, ' '.join(['%.4f %.4f' % (x.ft[0], x.ft[1]) for x in fts]))
    with open(output, 'w') as f:
        f.write(line)
    pass

