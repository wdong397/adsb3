#!/usr/bin/env python
import sys
import numpy as np
import time
from adsb3 import *
from baseline2 import *
import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('model', nargs=1)
args = parser.parse_args()

model = args.model[0]

#ft = open('baseline2.ft', 'w')

ROOT = os.path.join('cache', model)
ROOTO = os.path.join('cache', model +'.mask')

for path in glob(os.path.join(ROOT, '*.npz')):
    uid = os.path.basename(path)
    uuid = os.path.splitext(uid)[0]
    output = os.path.join(ROOTO, uid + '.ft')
    if os.path.exists(output):
        continue
    open(output, 'w').close()
    start_time = time.time()
    case = load_lung_mask(uuid)
    case = case.rescale3D(SPACING)
    case.round_stride()
    try:
        data = np.load(path)
        lb = data['arr_0']
    except:
        print 'failed to load', uid
        continue
    lb *= case.images
    load_time = time.time()
    views = [lb]
    fts = extract_ft(views)
    line = '%s %s\n' % (uid, ' '.join(['%.4f' % x.ft[0] for x in fts]))
    with open(output, 'w') as f:
        f.write(line)
    pass

