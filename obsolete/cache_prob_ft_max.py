#!/usr/bin/env python
import sys
import numpy as np
import time
from adsb3 import *
import argparse

def extend (z0, z1, Z, ext):
    zx = int(math.ceil((z1-z0) * ext))
    if zx <= 0:
        return z0, z1
    z0 = max(0, z0 - zx)
    z1 = min(Z, z1 + zx)
    return z0, z1

class Features:
    def __init__ (self, box, prob,th):
        z0, y0, x0, z1, y1, x1 = box.bbox
        if False:
            Z, Y, X = views[0].shape
            r = 0
            rr = (z1-z0) * SPACING
            r += rr * rr
            rr = (y1-y0) * SPACING
            r += rr * rr
            rr = (x1-x0) * SPACING
            r += rr * rr
            print math.sqrt(r)
            if ext: # extend the bounding box
                z0, z1 = extend(z0, z1, Z, ext)
                y0, y1 = extend(y0, y1, Y, ext)
                x0, x1 = extend(x0, x1, X, ext)
        self.box = (z0, y0, x0, z1, y1, x1)
        #area 11
        #bbox (89L, 306L, 343L, 94L, 310L, 344L)
        #coords [[ 89 307 343],
        # ......
        # [ 93 308 343]]
        #equivalent_diameter 2.75929428187
        #extent 0.55
        #filled_area 11
        ft = []
        #ft.append((z1-z0)*(y1-y0)*(x1-x0))
        roi = prob[z0:z1,y0:y1,x0:x1]
        #roi = np.clip(roi, th, 1)
        #roi[roi < th] = 0
        unit = SPACING * SPACING * SPACING
        ft.append(np.sum(roi) * unit)
        self.ft = ft
        pass

def extract_ft (views, th=0.1, ext=5):
    lb = np.ones_like(views[0], dtype=np.float32)

    for view in views:
        np.minimum(lb, view, lb)

    binary = lb > th
    k = int(round(ext / SPACING))
    binary = binary_dilation(binary, iterations=k)
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    boxes = measure.regionprops(labels)
    boxes = sorted(measure.regionprops(labels), key=lambda x: -x.area)
    box = boxes[0]
    return Features(box,lb,th))#extract_box(box, views))

parser = argparse.ArgumentParser(description='')
parser.add_argument('model', nargs=1)
args = parser.parse_args()

model = args.model[0]

#case = CaseBase()
#case.view = AXIAL
#ft = open('baseline2.ft', 'w')

ROOT = os.path.join('cache', model)
ROOTO = os.path.join('cache', model + '.max')

case = CaseBase()
case.view = AXIAL
for path in glob(os.path.join(ROOT, '*.npz')):
    uid = os.path.basename(path)
    output = os.path.join(ROOTO, uid + '.ft')
    if os.path.exists(output):
        continue
    open(output, 'w').close()
    start_time = time.time()
    try:
        print path
        data = np.load(path)
        lb = data['arr_0']
    except:
        print 'failed to load', uid
        continue
    load_time = time.time()
    views = [lb]
    fts = extract_ft(views)
    line = '%s %s\n' % (uid, ' '.join(['%.4f' % x.ft[0] for x in fts]))
    with open(output, 'w') as f:
        f.write(line)
    pass

