#!/usr/bin/env python
import sys
import numpy as np
import time
from adsb3 import *
from baseline2 import *
import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('model', nargs=1)
args = parser.parse_args()

model = args.model[0]

#case = CaseBase()
#case.view = AXIAL
#ft = open('baseline2.ft', 'w')

ROOT = os.path.join('cache', model)
ROOTO = os.path.join('cache', model + '.shrink')

for path in glob(os.path.join(ROOT, '*.npz')):
    uid = os.path.basename(path)
    output = os.path.join(ROOTO, uid + '.ft')
    uid = os.path.splitext(uid)[0]
    if os.path.exists(output):
        continue
    open(output, 'w').close()
    start_time = time.time()
    try:
        print path
        data = np.load(path)
        lb = data['arr_0']
    except:
        print 'failed to load', uid
        continue

    case = load_case(uid)
    cache = os.path.join('cache/mask-123.85-0.01/%s.npz' % case.uid)
    binary = None
    if os.path.exists(cache) and os.path.getsize(cache) > 0:
        # load cache
        binary = load_mask(cache)
    assert not binary is None
    case.images = binary.astype(dtype=np.float32)
    case = case.rescale3D(SPACING)
    case.round_stride()
    assert case.images.shape == lb.shape

    load_time = time.time()
    views = [lb]
    fts = extract_ft(views)
    line = '%s %s\n' % (uid, ' '.join(['%.4f' % x.ft[0] for x in fts]))
    output = os.path.join(ROOTO, uid + '.ft')
    with open(output, 'w') as f:
        f.write(line)
    pass

