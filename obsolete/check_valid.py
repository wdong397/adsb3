#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys
import os
import datetime
import numpy as np
from adsb3 import *

models = sys.argv[1:]
failed = []
def load_ft (uidx, path):
    uid = None
    nodules = []
    if not os.path.exists(path):
        failed.append(path)
        return
    with open(path, 'r') as f:
        l = f.read().strip().split(' ')
        if len(l) == 0:
            failed.append(path)
            return
        uid = l[0]
        if uid[-4:] == '.npz':
            uid = uid[:-4]
        nodules = [float(x) for x in l[1:] ]
    if uid != uidx:
        failed.append(path)
    pass

def load_all_ft (uid):
    for model in models:
        cached = os.path.join('cache', model, uid + '.npz.ft')
        load_ft(uid, cached)
        pass

for uid, label in STAGE1.train + STAGE1.test:
    load_all_ft(uid)

for path in failed:
    ft = path
    npz = ft.replace('.ft', '')
    npztag = ft.replace('.npz.ft', '')
    uid = os.path.basename(npztag)
    if os.path.exists(npz):
        try:
            #print path
            data = np.load(npz)
            lb = data['arr_0']
        except:
            print('removing bad npz', npz)
            os.remove(npz)
            continue
    if not os.path.exists(npz):
        print('removing bad npz', npztag)
        try:
            os.remove(npztag)
        except:
            pass
    if os.path.exists(ft):
        print('removing bad ft', ft)
        os.remove(ft)

    #print(uid)
    pass
pass
