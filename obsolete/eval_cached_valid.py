#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys
import os
import datetime
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from adsb3 import *
from sklearn.model_selection import KFold
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss
from sklearn.preprocessing import normalize
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier
from xgboost import XGBClassifier

SPLIT=50

models = sys.argv[1:]
failed = []

def extract (x):
    if len(x) == 0:
        ft = [0.0, 0.0, 0.0]
    else:
        ft = [sum(x), len(x), max(x)]
        if not np.isfinite(ft[0]):
            ft[0] = ft[2]
    return ft #ft[2:] #[:2] #[:3]

def load_ft (uidx, path):
    uid = None
    nodules = []
    if not os.path.exists(path):
        raise Exception('xx')
    plus = False
    with open(path, 'r') as f:
        l = f.read().strip().split(' ')
        uid = l[0]
        if uid[-4:] == '.npz':
            uid = uid[:-4]
        if uid[0] == '+':
            uid=uid[1:]
            plus = True
        nodules = [float(x) for x in l[1:] ]
    if uid != uidx:
        failed.append(path)
    if plus:
        nn = nodules[0]
        if nn == 0:
            nft = [0] *8
        else:
            nft = nodules[1:9]
        if len(nft) != 8:
            print(nft)
    else:
        nft = extract(nodules)
    for x in nft:
        if not np.isfinite(x):
            print('NaN', path, nft)
            raise Exception('yy')
    return uid, nft

def load_all_ft (uid):
    ft = []
    for model in models:
        cached = os.path.join('cache', model, uid + '.npz.ft')
        a, b = load_ft(uid, cached)
        ft.extend(b)
        pass
    return ft

U = []
X = []
Y = []
Xt = []


for uid, label in STAGE1.train:
    try:
        ft = load_all_ft(uid)
        U.append(uid)
        Y.append(label)
        X.append(ft)
    except:
        pass

X = np.array(X, dtype=np.float32)
Y = np.array(Y, dtype=np.float32)
print('X', X.shape)
print('Y', Y.shape)

#for i in range(Y.shape[0]):
#    print(Y[i], X[i,0], X[i,1], U[i])

Y1 = np.sum(Y)
Y0 = len(Y) - Y1
print('neg:', Y0, 'pos:', Y1)

kf = KFold(n_splits=SPLIT, shuffle=True, random_state=88)
y_pred = Y * 0
y_pred_prob = Y * 0

#model = LogisticRegression() #class_weight={0: Y1, 1:Y0})
model = GradientBoostingClassifier(n_estimators=50, learning_rate=0.1,  max_depth=2, random_state=0)
#model = XGBClassifier(learning_rate=0.1,max_depth=1,seed=2016)

N = 0
for train, test in kf.split(X):
    X_train, X_test, y_train = X[train,:], X[test,:], Y[train]
    #model.fit(X_train, y_train)
    model.fit(X_train, y_train)

    y_pred[test] = model.predict(X_test)
    #try:
    y_pred_prob[test] = model.predict_proba(X_test)[:,1]
    N += 1
    #except:
    #    y_pred_prob[test] = y_pred[test]

try:
    print(classification_report(Y, y_pred, target_names=["0", "1"]))
except:
    pass
#print(zip(Y, y_pred_prob))
print("logloss",log_loss(Y, y_pred_prob))
    #X = X[:, 0:1]
    #print("baseline:")
if len(failed) > 0:
    print()
    print("Failed:")
    for uid in failed:
        print(uid)
        pass
    pass
