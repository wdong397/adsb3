#!/usr/bin/env python
import sys
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
from adsb3 import *
from adsb3_lab import *
import luna
import color

THUMB_SIZE=180

def save_view (path, case, slices, size):
    #print 'SCALE', np.min(case.images), np.max(case.images)
    #print 'ORIG', np.min(case.images), np.max(case.images)
    case = case.rescale(slices, None, size)
    #print 'NORM', np.min(case.images), np.max(case.images)
    #print case.view, case.spacing, case.images.shape
    case.save_gif(path)

SAMPLES=[
'be2be08151ef4d3aebd3ea4fcd5d364b',
'6ee742b62985570a1f3a142eb7e49188',
'ac68eb0a3db3de247c26909db4c10569',
'ad7e6fe9d036ed070df718f95b212a10',
'87cdf4626079509e5d6d3c3b6c8bfc2e',
'8c63c8ebd684911de92509a8a703d567',
'3f6431400c2a07a46386dba3929da45d',
'9e5c2e760b94b8919691d344cfdbac7f',
'89bfbba58ee5cd0e346cdd6ffd3fa3a3',
'ea7373271a2441b5864df2053c0f5c3e'
]

for i, uid in enumerate(SAMPLES):
    case = Case(uid)
    try:
        lb = color.detect_lb(case.images)
    except:
        print 'bad lb', uid
        continue
    case.images[case.images < lb] = lb
    case.normalizeHU()
    case = case.rescale3D(3.4)
    mask = color.segment_lung_mask(case.images, True)
    print "A"
    fig = plot_3d(case.images)
    fig.savefig('gif/%d.png' % i)
    print "B"
    fig = plot_3d(mask, 0)
    fig.savefig('gif/%d-m.png' % i)
    #mask = 1 - mask
    case.normalize()
    print np.min(case.images), np.max(case.images)
    case.images *= mask
    print case.images.shape
    case.save_gif('gif/%d.gif' % i)
    print "C"
    pass

