#!/usr/bin/env python
import sys
import random
import glob
import subprocess
from tqdm import tqdm
from adsb3 import *
import cv2
import picpac

MAX=None

#subprocess.check_call('rm -rf db/neg3c/axial.pic db/neg3c/sagittal.pic db/neg3c/coronal.pic', shell=True)

try_mkdir('/data/ssd/wdong/3c16/neg/')
axial_db = picpac.Writer('/data/ssd/wdong/3c16/neg/axial')
sagittal_db = picpac.Writer('/data/ssd/wdong/3c16/neg/sagittal')
coronal_db = picpac.Writer('/data/ssd/wdong/3c16/neg/coronal')

def dump (case, db):
    N = case.images.shape[0]
    for o in range(0, N, 2):
        image = get3c(case.images, o)
        if image is None:
            continue
        buf = cv2.imencode('.tiff', image.astype(np.uint16))[1].tostring()
        db.append(0, buf)
        pass
	pass

uids = [uid for uid, label in STAGE1.train if label == 0]
if not MAX is None:
    random.shuffle(uids)
    uids = uids[:MAX]

for uid in uids:
    print uid
    case = load_case(uid)
    case.standardize_color16()
    case = case.rescale3D(SPACING)
    dump(case, axial_db)
    dump(case.transpose(SAGITTAL), sagittal_db)
    dump(case.transpose(CORONAL), coronal_db)
    pass

