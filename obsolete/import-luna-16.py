#!/usr/bin/env python
import sys
import json
import subprocess
from multiprocessing import Pool
#from tqdm import tqdm
from adsb3 import *
#import color
import cv2
from tqdm import tqdm
import picpac

#SPACING=0.7421879768371582

#SPACING *= 2
MAX = -1 #100 #None #50
#MAX = None

def dump (db, dbneg, case):
    images = case.images
    N, H, W = images.shape
    by_frame = {}
    neg = set(range(N))
    for anno in case.picpac_anno():
        for j, x, y, rx, ry in anno:
            r = max(W * rx, H * ry) #* SPACING
            print '\t', j, x, y, rx, ry, r
            if r < 1:   # less than 
                continue
            try:
                neg.remove(j)
            except:
                pass
            one = by_frame.setdefault(j, [])
            one.append({
                    'type': 'ellipse',
                    'geometry': {
                        'x': x - rx,
                        'y': y - ry,
                        'width': rx * 2,
                        'height': ry * 2
                    }
                })
            pass
        pass
    for j, shapes in by_frame.iteritems():
        image = get3c(images, j)
        if image is None:
            continue
        anno = {"shapes": shapes}
        buf1 = cv2.imencode('.tiff', image.astype(np.uint16))[1].tostring()
        buf2 = json.dumps(anno)
        db.append(buf1, buf2)
    pass

#subprocess.check_call('rm -rf db/luna3c/*', shell=True)
try_mkdir('/data/ssd/wdong/3c16/luna')
db1 = picpac.Writer('/data/ssd/wdong/3c16/luna/axial')
#db1neg = picpac.Writer('db/luna3c/axial_neg')
db2 = picpac.Writer('/data/ssd/wdong/3c16/luna/sagittal')
#db2neg = picpac.Writer('db/luna3c/sagittal_neg')
db3 = picpac.Writer('/data/ssd/wdong/3c16/luna/coronal')
#db3neg = picpac.Writer('db/luna3c/coronal_neg')
uids = LUNA_ANNO.keys()
#if MAX and (MAX < len(uids)):
#    uids = uids[:MAX]

uids = uids[:MAX]

print 'CASES:', len(uids)
pool = Pool(None)
for uid in tqdm(uids):
    case = load_16bit_lungs_noseg(uid)
    case = case.rescale3D(SPACING)
    dump(db1, None, case)
    dump(db2, None, case.transpose(SAGITTAL))
    dump(db3, None, case.transpose(CORONAL))
    pass

