#!/bin/bash

jupyter notebook --port 8899 --ip 0.0.0.0 --no-browser --NotebookApp.disable_check_xsrf=True
