#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import numpy as np
import tensorflow as tf

class Model:
    def __init__ (self, path, name=None, prob=False):
        """applying tensorflow image model.

        path -- path to model
        name -- output tensor name
        prob -- convert output (softmax) to probability
        """
        graph = tf.Graph()
        with graph.as_default():
            saver = tf.train.import_meta_graph(path + '.meta')
        if False:
            for op in graph.get_operations():
                for v in op.values():
                    print(v.name)
        inputs = graph.get_tensor_by_name("images:0")
        outputs = graph.get_tensor_by_name(name)
        if prob:
            shape = tf.shape(outputs)    # (?, ?, ?, 2)
            # softmax
            outputs = tf.reshape(outputs, (-1, 2))
            outputs = tf.nn.softmax(outputs)
            outputs = tf.reshape(outputs, shape)
            # keep prob of 1 only
            outputs = tf.slice(outputs, [0, 0, 0, 1], [-1, -1, -1, -1])
            # remove trailing dimension of 1
            outputs = tf.squeeze(outputs, axis=[3])
            pass
        self.prob = prob
        self.path = path
        self.graph = graph
        self.inputs = inputs
        self.outputs = outputs
        self.saver = saver
        self.sess = None
        pass

    def __enter__ (self):
        assert self.sess is None
        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        self.sess = tf.Session(config=config, graph=self.graph)
        #self.sess.run(init)
        self.saver.restore(self.sess, self.path)
        return self

    def __exit__ (self, eType, eValue, eTrace):
        self.sess.close()
        self.sess = None

    def apply (self, images, batch=32):
        if self.sess is None:
            raise Exception('Model.apply must be run within context manager')
        if len(images.shape) == 3:  # grayscale
            images = images.reshape(images.shape + (1,))
            pass

        if self.prob:
            out = np.zeros(images.shape[:3])
        else:
            out = np.zeros(images.shape)
            pass

        for b in range(0, images.shape[0], batch):
            e = b + batch
            if e > images.shape[0]:
                e = images.shape[0]
            bb = self.sess.run(self.outputs, feed_dict={self.inputs: images[b:e]})
            _, H, W = bb.shape[:3]
            _, H0, W0 = out.shape[:3]
            oH = (H - H0)//2
            oW = (W - W0)//2
            out[b:e, :, :] = bb[:, oH:(oH+H0), oW:(oW+W0)]
        return out
    pass
