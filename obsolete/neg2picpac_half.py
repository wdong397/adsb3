#!/usr/bin/env python
import sys
import random
import glob
import subprocess
from tqdm import tqdm
from adsb3 import *
import cv2
import picpac

def dump (case, db):
    N = case.images.shape[0]
    for o in range(0, N, 2):
        image = case.images[o]
        buf = cv2.imencode('.png', image)[1].tostring()
        db.append(0, buf)
        pass
	pass


for i in range(2):
    subprocess.check_call('mkdir -p db/nnc_%d; rm -rf db/nnc_%d/axial_neg.pic db/nnc_%d/sagittal_neg.pic db/nnc_%d/coronal_neg.pic' % (i,i,i,i), shell=True)

    axial_db = picpac.Writer('db/nnc_%d/axial_neg.pic' % i)
    sagittal_db = picpac.Writer('db/nnc_%d/sagittal_neg.pic' % i)
    coronal_db = picpac.Writer('db/nnc_%d/coronal_neg.pic' % i)

    half = set([uid for uid, label in STAGE1.halves[i] if label == 0])
    for uid in half:
        print uid
        case = load_8bit_lungs(uid)
        case = case.rescale3D(SPACING)
        dump(case, axial_db)
        dump(case.transpose(SAGITTAL), sagittal_db)
        dump(case.transpose(CORONAL), coronal_db)
        pass

