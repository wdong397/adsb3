#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import sys
import os
import cv2
import numpy as np
from skimage import measure
from gallery import Gallery
from models import Model
from adsb3 import *
import color

SPACING=0.7421879768371582 #*2

model = sys.argv[1]
out = sys.argv[2]

uids = [uid for uid, label in STAGE1.train if label > 0]

model = Model(model, 'prob')
gal = Gallery(out)
for uid in uids:
    case = Case(uid)
    try:
        lb = color.detect_lb(case.images)
    except:
        print('bad color: %s'% uid)
        continue
    case.normalize(0, 255, min_th=lb)
    case = case.rescale(None, SPACING)
    N = images.shape[0]
    probs = model.apply(images, batch=16)
    for i in range(0, N):
        image = images[i]
        prob = probs[i]
        contours = measure.find_contours(prob, 0.05)
        if len(contours) == 0:
            continue
        for contour in contours:
            tmp = np.copy(contour[:,0])
            contour[:, 0] = contour[:, 1]
            contour[:, 1] = tmp
            contour = contour.reshape((1, -1, 2)).astype(np.int32)
            cv2.polylines(image, contour, True, 255)
            cv2.polylines(prob, contour, True, 1)
        prob *= 255
        both = np.concatenate([image, prob], axis=1)
        cv2.imwrite(gal.next(), both)
        pass
    break
gal.flush()
model.close()
pass

