#!/usr/bin/env python
from adsb3 import *
# adsb3 exposes
# - Stage1.{train, test, samples}
# - Case class to load files
# see below.

# Setup directory
# code downloaded from git already contains the meta data files
# Need to:
# - download the stage1.7z, unzip to data/stage1
# - download and unzip samples to data/samples
#

# I've verified meta data loading code to be correct
# meta data are stored in data/

count = [0, 0]
for uid, label in STAGE1.train:
    count[label] += 1
    pass
print 'negative', count[0]
print 'positive', count[1]


predict = lambda x: 0.5

submit = []
for uid, _ in STAGE1.test:
    submit.append((uid, predict(uid)))
    pass

dump_meta('submit.txt', submit)
dump_meta('samples.txt', STAGE1.samples)

# get a random UID
uid = STAGE1.samples[3][0]

# load dicoms
case = Case(uid)

# scan the dicom files
print case.uid
print case.path
print case.origin
print case.images.shape
print case.spacing

#for dcm in case.dcms:
    # dcm is of class DICOM, a wrapper of pydicom objects
    # with useful fields extracted and sanity checked
    # the original pydicom object is kept as the .dcm field
#    name = os.path.basename(dcm.dcm.filename)
#    image = dcm.image
#    print name, image.shape
#    print dcm.position
#    print dcm.ori_row, dcm.ori_col
#    print dcm.location, dcm.spacing
#    break


