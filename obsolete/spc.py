#!/usr/bin/env python
import sys
import numpy as np
import tensorflow as tf
from adsb3 import *
import color

class Model:
    def __init__ (self, path):
        #init = tf.global_variables_initializer()

        self.graph = tf.Graph()
        with self.graph.as_default():
            saver = tf.train.import_meta_graph(path + '.meta')
            self.images = self.graph.get_tensor_by_name("images:0")
            self.code = self.graph.get_tensor_by_name('code:0')

        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        self.sess = tf.Session(config=config, graph=self.graph)
        #self.sess.run(init)
        saver.restore(self.sess, path)
        pass

    def apply (self, images, batch=32):
        code = None
        images = images.reshape(images.shape + (1,))
        for b in range(0, images.shape[0], batch):
            e = b + batch
            if e > images.shape[0]:
                e = images.shape[0]
            inp = images[b:e, :, :, :]
            out = self.sess.run(self.code, feed_dict={self.images: inp})
            if code is None:
                code = np.zeros((images.shape[0],) + out.shape[1:])
            code[b:e, :, :, :] = out[:, :, :, :]
        return code

    def close (self):
        self.sess.close()


model = sys.argv[1]

SPACING=0.7421879768371582 #*2

N = 50

pos = [case for case in STAGE1.train if case[1] > 0]
neg = [case for case in STAGE1.train if case[1] == 0]
uids = pos[:N] + neg[:N]

model = Model(model)
X = []
Y = []
L = len(uids)
for i, (uid, label) in enumerate(uids):
    case = Case(uid)
    try:
        lb = color.detect_lb(case.images)
    except:
        print(sys.exc_info())
        print('bad color: %s'% uid)
        continue
    case.normalize(min_th=lb)
    case = case.rescale(100, SPACING)
    images = case.images
    images *= 255
    coding = model.apply(images, batch=16)
    feature = np.sum(coding, (0,1,2))
    S = np.sum(feature)
    print '%d/%d' % (i, L), S #, feature
    X.append(feature)
    Y.append(label)
model.close()
X = np.array(X)
np.savez('arr100', X, Y)
pass

