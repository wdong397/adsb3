#!/usr/bin/env python
import numpy as np
import xgboost as xgb
from sklearn.model_selection import KFold
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss
from sklearn.preprocessing import normalize

npz = np.load('arr100.npz')
print npz.files
X = npz['arr_0']
Y = npz['arr_1'].astype(np.float32)
print X.shape
print Y.shape

if True:
    zero = np.any(X > 0.01, 0)
    X = X[:, zero]
    print X.shape
    #normalize(X, copy=False)

print ("XGBoost")
kf = KFold(n_splits=10, shuffle=True, random_state=88)
y_pred = Y * 0
y_pred_prob = Y * 0

params = dict(
        max_depth = 5,
        learning_rate = 0.001,
        n_estimators = 20,
        objective="binary:logistic",
        gamma = 0.1
    )
for train, test in kf.split(X):
    X_train, X_test, y_train, y_test = X[train,:], X[test,:], Y[train], Y[test]
    clf = xgb.XGBClassifier(**params)
    clf.fit(X_train, y_train)
    y_pred[test] = clf.predict(X_test)
    y_pred_prob[test] = clf.predict_proba(X_test)[:,1]
print classification_report(Y, y_pred, target_names=["0", "1"])
print("logloss",log_loss(Y, y_pred_prob))
