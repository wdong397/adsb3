#!/usr/bin/env python
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from adsb3 import *
import luna
from gallery import Gallery

THUMB_SIZE=180

def plot_stat (path, case):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.hist(case.images.flatten(), bins=200)
    fig.savefig(path)
    pass

SAMPLES=[
'be2be08151ef4d3aebd3ea4fcd5d364b',
'6ee742b62985570a1f3a142eb7e49188',
'ac68eb0a3db3de247c26909db4c10569',
'ad7e6fe9d036ed070df718f95b212a10',
'87cdf4626079509e5d6d3c3b6c8bfc2e',
'8c63c8ebd684911de92509a8a703d567',
'3f6431400c2a07a46386dba3929da45d',
'9e5c2e760b94b8919691d344cfdbac7f',
'89bfbba58ee5cd0e346cdd6ffd3fa3a3',
'ea7373271a2441b5864df2053c0f5c3e'
]

gal = Gallery('/home/wdong/public_html/A_stat')
print type(gal)
for uid in SAMPLES:
    case = Case(uid)
    plot_stat(gal.next(), case)
    pass
gal.flush()

SAMPLES = [
'1.3.6.1.4.1.14519.5.2.1.6279.6001.475325201787910087416720919680',
'1.3.6.1.4.1.14519.5.2.1.6279.6001.320967206808467952819309001585',
'1.3.6.1.4.1.14519.5.2.1.6279.6001.707218743153927597786179232739',
'1.3.6.1.4.1.14519.5.2.1.6279.6001.185154482385982570363528682299',
'1.3.6.1.4.1.14519.5.2.1.6279.6001.293757615532132808762625441831',
'1.3.6.1.4.1.14519.5.2.1.6279.6001.163901773171373940247829492387',
'1.3.6.1.4.1.14519.5.2.1.6279.6001.247816269490470394602288565775',
'1.3.6.1.4.1.14519.5.2.1.6279.6001.861997885565255340442123234170',
'1.3.6.1.4.1.14519.5.2.1.6279.6001.315187221221054114974341475212',
'1.3.6.1.4.1.14519.5.2.1.6279.6001.161002239822118346732951898613'
]

gal = Gallery('/home/wdong/public_html/L_stat')
for uid in SAMPLES:
    case = luna.Case(uid)
    plot_stat(gal.next(), case)
    pass
gal.flush()
