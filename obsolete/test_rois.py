#!/usr/bin/env python
import sys
import numpy as np
import tensorflow as tf
from skimage import measure
from adsb3 import *
from gallery import Gallery
from papaya import Papaya, Annotations
import nets

def find_center (prob, bbox):
    z0, y0, x0, z1, y1, x1 = bbox
    return [(z0+z1)/2, (y0+y1)/2, (x0+x1)/2], [(z1-z0)/2, (y1-y0)/2, (x1-x0)/2]

def find_blobs (gal, case, prob):
    binary = prob > 0.5
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    pp = measure.regionprops(labels)
    boxes = []
    for roi in pp:
        if roi.area < 10:
            continue
        boxes.append(roi.bbox)
        C, R = find_center(prob, roi.bbox)
        for view in VIEWS:
            images = case.transpose(view).images
            c = index_view(C, view)
            r = index_view(R, view)
            I = cv2.cvtColor(images[c[0]], cv2.COLOR_GRAY2BGR)
            #P = cv2.cvtColor(prob[C[0]], cv2.COLOR_GRAY2BGR)
            #P *= 255
            cv2.circle(I, (c[2], c[1]), max(r[1], r[2]), (0, 255, 0))
            cv2.imwrite(gal.next(), I)
            #vis.append(I)
            pass
        #vis = np.hstack(tuple(vis))
        pass
    return boxes

BATCH = 32
def round_stride (images, stride=16):
    _, H, W = images.shape[:3]
    H = H / stride * stride
    W = W / stride * stride
    return images[:,0:H,0:W]

X = tf.placeholder(tf.float32, shape=(None, None, None), name="images")
X4 = tf.expand_dims(X, axis=3)
PROB, loader = nets.import_meta_graph('models/luna/axial', X4, 'axial', softmax=True)

config = tf.ConfigProto()
with tf.Session(config=config) as sess:
    tf.global_variables_initializer().run()
    loader(sess)

    gal = Gallery('/home/wdong/public_html/roi_cross', cols=3, header=['axial', 'sagittal', 'coronal'])    
    pap = Papaya('/home/wdong/public_html/roi_full')
    C = 0
    for uid, label in STAGE1.train:
        if label == 0:
            continue
        #uid = 'eb008af181f3791fdce2376cf4773733'
        uid = sys.argv[1]
        C += 1
        if C > 1:
            break
        case = load_8bit_lungs_noseg(uid)
        print case.images.shape
        case = case.rescale3D(SPACING)
        case.images = round_stride(case.images)
        prob = np.zeros_like(case.images, dtype=np.float32)
        for b in range(0, case.images.shape[0], BATCH):
            e = min(case.images.shape[0], b + BATCH)
            x = case.images[b:e]
            y, = sess.run([PROB], feed_dict={X:x})
            prob[b:e] = y[:,:,:]
            print '.'
        boxes = find_blobs(gal, case, prob)
        annos = Annotations()
        for box in boxes:
            annos.add(box, '')
        pap.next(case, annos)
    gal.flush()
    pap.flush()

