#!/usr/bin/env python
import sys
import numpy as np
import tensorflow as tf
from skimage import measure
from adsb3 import *
import nets

def find_blobs (prob):
    binary = prob > 0.5
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    pp = measure.regionprops(labels)
    for roi in pp:
        print roi.area, roi.bbox
    pass


BATCH = 32
def round_stride (images, stride=16):
    _, H, W = images.shape[:3]
    H = H / stride * stride
    W = W / stride * stride
    return images[:,0:H,0:W]

X = tf.placeholder(tf.float32, shape=(None, None, None), name="images")
X4 = tf.expand_dims(X, axis=3)
PROB, loader = nets.import_meta_graph('models/luna/axial', X4, 'axial', softmax=True)

config = tf.ConfigProto()
with tf.Session(config=config) as sess:
    tf.global_variables_initializer().run()
    loader(sess)

    for uid, label in STAGE1.train[:1]:
        case = load_8bit_lungs_noseg(uid)
        case = case.rescale3D(SPACING)
        case.images = round_stride(case.images)
        prob = np.zeros_like(case.images, dtype=np.float32)
        for b in range(0, case.images.shape[0], BATCH):
            e = min(case.images.shape[0], b + BATCH)
            x = case.images[b:e]
            y, = sess.run([PROB], feed_dict={X:x})
            prob[b:e] = y[:,:,:]
            print '.'
        find_blobs(prob)
    

