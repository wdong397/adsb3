#!/usr/bin/env python
import sys
import json
from tqdm import tqdm
from adsb3 import *
import color
import cv2
import picpac

SPACING=0.7421879768371582

#SPACING *= 2
#MAX = 50 #None #50
MAX = 2
STEP = 20

def dump (db, case):
    images = case.images
    for j in range(0, images.shape[0], STEP):
        image = images[j]
        image *= 255
        buf1 = cv2.imencode('.jpg', image)[1].tostring()
        db.append(0, buf1)
    pass

if len(sys.argv) < 2:
    print("Usage: %s db" % sys.argv[0])

db1 = picpac.Writer(sys.argv[1] + '.1')
db2 = picpac.Writer(sys.argv[1] + '.2')
db3 = picpac.Writer(sys.argv[1] + '.3')
uids = [uid for uid, label in STAGE1.train if label > 0]
if MAX and (MAX < len(uids)):
    uids = uids[:MAX]
for uid in tqdm(uids):
    case = Case(uid)
    try:
        lb = color.detect_lb(case.images)
    except:
        print 'bad color', uid
        continue
        pass
    case.normalize(min_th=lb)
    dump(db1, case.rescale3D(SPACING))
    dump(db2, case.transpose(SAGITTAL).rescale3D(SPACING))
    dump(db3, case.transpose(CORONAL).rescale3D(SPACING))
    pass

