#!/usr/bin/env python
from adsb3 import *
import subprocess
import random
import simplejson as json
import picpac


subprocess.call('rm patch/a? patch/b? patch/[ab]', shell=True)
dba = picpac.Writer('patch/a')
dbas = [picpac.Writer('patch/a0'),
		picpac.Writer('patch/a1'),
		picpac.Writer('patch/a2')]
dbb = picpac.Writer('patch/b')
dbbs = [picpac.Writer('patch/b0'),
	    picpac.Writer('patch/b1'),
		picpac.Writer('patch/b2')]

pos = []
neg = []
for i in range(len(STAGE1.train)):
    _, label = STAGE1.train[i]
    if label > 0:
        pos.append(i)
    else:
        neg.append(i)
random.shuffle(pos)
random.shuffle(neg)

n_pos = len(pos)
n_neg = len(neg)

pos1 = set(pos[:n_pos/2])
neg1 = set(neg[:n_neg/2])
pos2 = set(pos[n_pos/2:])
neg2 = set(neg[n_neg/2:])

train1 = pos1.union(neg1)
train2 = pos2.union(neg2)
pos = pos1.union(pos2)

db_in = picpac.Reader('patch/all')
for r in db_in:
    meta = json.loads(r.fields[1])
    pk = meta['index']
    nid = meta['nodule']
    view = meta['view']
    if pk in train1:
        db = dba
        dbs = dbas
    elif pk in train2:
        db = dbb
        dbs = dbbs
    else:
        continue    #test test
    label = 0
    if pk in pos:
        label = 1
        if nid > 0:
            continue	# small nodule
    db.append(label, r.fields[0])
    dbs[int(view)].append(label, r.fields[0])
    pass

