#!/usr/bin/env python
import math
import sys
import numpy as np
import time
from skimage import measure
from adsb3 import *
from gallery import Gallery

def extend (z0, z1, Z, ext):
    zx = int(math.ceil((z1-z0) * ext))
    if zx <= 0:
        return z0, z1
    z0 = max(0, z0 - zx)
    z1 = min(Z, z1 + zx)
    return z0, z1

class Features:
    def __init__ (self, box, views, ext=0.3):
        Z, Y, X = views[0].shape
        z0, y0, x0, z1, y1, x1 = box.bbox
        if ext: # extend the bounding box
            z0, z1 = extend(z0, z1, Z, ext)
            y0, y1 = extend(y0, y1, Y, ext)
            x0, x1 = extend(x0, x1, X, ext)
        self.box = (z0, y0, x0, z1, y1, x1)
        #area 11
        #bbox (89L, 306L, 343L, 94L, 310L, 344L)
        #coords [[ 89 307 343],
        # ......
        # [ 93 308 343]]
        #equivalent_diameter 2.75929428187
        #extent 0.55
        #filled_area 11
        ft = [box.area]
        #ft.append((z1-z0)*(y1-y0)*(x1-x0))
        for view in views:
            roi = view[z0:z1,y0:y1,x0:x1]
            ft.append(np.sum(roi))
        self.ft = ft
        pass

def extract_ft (views, th=0.1):
    lb = np.ones_like(views[0], dtype=np.float32)
    ub = np.zeros_like(views[0], dtype=np.float32)
    f = []
    for view in views:
        f.append(np.sum(view))
        np.minimum(lb, view, lb)
        np.maximum(ub, view, ub)

    binary = lb > th
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    boxes = measure.regionprops(labels)

    views = views + [lb,ub]

    fts = []
    for box in boxes:
        #if box.area < 10:
        #    continue
        fts.append(Features(box,views))#extract_box(box, views))
        pass
    return fts

