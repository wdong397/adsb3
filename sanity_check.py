#!/usr/bin/env python
import cPickle as pickle
from adsb3 import *

uids = []

uids.extend([uid for uid, _ in STAGE1.train])
uids.extend([uid for uid, _ in STAGE1.test])
#uids.extend([uid for uid, _ in STAGE1.samples])

cases = []
for uid in uids:
    case = Case(uid)
    for dcm in case.dcms:
        dcm.dcm = None
        dcm.image = None
    cases.append(case)
    pass

pickle.dump(cases, open('stage1.pkl', 'wb'))
