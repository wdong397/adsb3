#!/usr/bin/env python
import sys
import time
import numpy as np
import cv2
from skimage import measure
#from skimage import regionprops
from adsb3 import *
import scipy
import pyadsb3
import mesh

UIDS = ['b635cda3e75b4b7238c18c6a5f1858f6']

class Timer:
    def __init__ (self):
        self.last = time.time()
        self.begin = self.last
        pass
    def report (self, text):
        t = time.time()
        print '%.4f:%.4f' % (t-self.last, t-self.begin), text
        self.last = t
        pass

#for uid in sys.stdin:
if True:
    uid = sys.argv[1] #uid.strip()
    print uid
    timer = Timer()
    case = load_case(uid)
    timer.report('load')
    case.normalizeHU()
    case = case.rescale3D(SPACING)
    timer.report('rescale')
    binary, _ = mesh.segment_body(case.images) #, th=-300) #, smooth=20)
    timer.report('segment')
    case.images = mesh.pad(binary, dtype=np.float) 
    case.images = scipy.ndimage.filters.gaussian_filter(case.images, 2, mode='constant')
    print np.min(case.images), np.max(case.images)
    verts, faces = measure.marching_cubes(case.images, 0.5)
    timer.report('mesh')
    outp = 'body/mesh/%s.ply' % uid
    pyadsb3.color_mesh(verts, faces, outp)
    #pyadsb3.save_mesh(verts, faces, outp)
    timer.report('colorize')
    pass

