#!/usr/bin/env python
import sys
import time
import numpy as np
import cv2
from skimage import measure
#from skimage import regionprops
from adsb3 import *
import scipy
import pyadsb3
import mesh

class Timer:
    def __init__ (self):
        self.last = time.time()
        self.begin = self.last
        pass
    def report (self, text):
        t = time.time()
        print '%.4f:%.4f' % (t-self.last, t-self.begin), text
        self.last = t
        pass

mask = sys.argv[1] #uid.strip()
uid = os.path.splitext(os.path.basename(mask))[0]
print uid
timer = Timer()
case = load_case(uid)
binary = load_mask(mask) #mesh.segment_lung(case.images, th=-600) #, smooth=20)
timer.report('segment')
#binary = mesh.convex_hull(binary)
#timer.report('convex')
case.images = binary.astype(dtype=np.float32) #case.rescale3D(SPACING)
#case = case.rescale3D(SPACING)
#case.images = scipy.ndimage.filters.gaussian_filter(case.images, 2, mode='constant')
print np.min(case.images), np.max(case.images)
verts, faces = measure.marching_cubes(case.images.astype(np.float32), 0.5)
timer.report('mesh')
outp = 'mesh/%s.ply' % uid
pyadsb3.color_mesh(verts, faces, outp)
timer.report('colorize')
pass

