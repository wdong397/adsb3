#!/usr/bin/env python
from glob import glob
import xml.etree.ElementTree as ET
import numpy as np
import adsb3
import logging
import cv2
from skimage import measure
import tcia
import picpac
import pyadsb3
from gallery import Gallery


logging.basicConfig(level=logging.INFO)
#for path in glob('data/tcia/tcia-lidc-xml/185/069.xml'):
gal = Gallery('/home/wdong/public_html/noduledb')

picpac_config = dict(shuffle=True,
            loop=False,
            batch=1,
            anno_type=cv2.CV_8UC3,
            annotate='json',
            channels=3,
            perturb=False,
            channel_first=False
            )
db = picpac.ImageStream('/data/ssd/wdong/3c/tcia.ft/axial', **picpac_config)

C = 0
for images, labels, _ in db:
    image_orig = cv2.cvtColor(images[0,:,:,1], cv2.COLOR_GRAY2RGB)
    image = np.copy(image_orig)
    binary, _, ft = pyadsb3.decode_labels(labels)
    binary = binary[0,:,:,0]
    contours = measure.find_contours(binary.astype(dtype=np.float32), 0.5)
    for contour in contours:
        tmp = np.copy(contour[:,0])
        contour[:, 0] = contour[:, 1]
        contour[:, 1] = tmp
        contour = contour.reshape((1, -1, 2)).astype(np.int32)
        cv2.polylines(image, contour, True, (0,255,0))
        pass
    # find label
    sbt, sph, mgn, lob, spi, tex, mal = ft
    ins, cal = 1, 6
    ft = [sbt, ins, cal, sph, mgn, lob, spi, tex, mal] 
    text_y = 20
    for i in range(len(ft)):
        cv2.putText(image, '%s: %.1f' % (tcia.FEATURES[i], ft[i]),
                        (10, text_y), 
                        cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 0))
        text_y += 20
        pass
    cv2.imwrite(gal.next(), np.hstack((image, image_orig)))
    C += 1
    if C > 200:
        break
    pass
gal.flush()


