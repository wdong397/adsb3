#!/usr/bin/env python
from glob import glob
import xml.etree.ElementTree as ET
import numpy as np
import adsb3
import logging
import cv2
import tcia
from gallery import Gallery

logging.basicConfig(level=logging.INFO)
#for path in glob('data/tcia/tcia-lidc-xml/185/069.xml'):
gal = Gallery('/home/wdong/public_html/noduleft')
C = 0
for path in glob('data/tcia/tcia-lidc-xml/*/*.xml'):
    anno = tcia.Annotation(path)
    print anno.uid, C, len(anno.slices), path
    if len(anno.slices) == 0:
        continue
    try:
        case = adsb3.LunaCase(anno.uid)
    except:
        print 'failed to load', anno.uid
        continue
    case.standardize_color()
    for z, sl in anno.slices.iteritems():
        i = (z - case.origin[0]) / case.spacing[0]
        if int(i) != i:
            logging.warn('offset %f' % i)
        i = int(round(i))
        image_orig = cv2.cvtColor(case.images[i], cv2.COLOR_GRAY2BGR)
        for ft, pts in sl.nodules:
            image = np.copy(image_orig)
            pts = np.array(pts)
            pts = pts.reshape((1,) + pts.shape).astype(np.int32)
            cv2.polylines(image, pts, True, (0,0,255))
            text_y = 20 
            for i in range(len(ft)):
                cv2.putText(image, '%s: %.1f' % (tcia.FEATURES[i], ft[i]),
                                (10, text_y), 
                                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 0))
                text_y += 20
                pass
            cv2.putText(image, 'z: %.1f' % z,
                            (10, text_y), 
                            cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 0))
            cv2.imwrite(gal.next(), np.hstack((image, image_orig)))
        C += 1
        pass
    if C > 200:
        break
    pass
gal.flush()


