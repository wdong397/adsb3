#!/usr/bin/env python
from glob import glob
import xml.etree.ElementTree as ET
import numpy as np
import adsb3
import logging
import cv2
import tcia
from gallery import Gallery

logging.basicConfig(level=logging.INFO)
#for path in glob('data/tcia/tcia-lidc-xml/185/069.xml'):
gal = Gallery('/home/wdong/public_html/nonnodule')
C = 0
for path in glob('data/tcia/tcia-lidc-xml/*/*.xml'):
    anno = tcia.Annotation(path)
    print anno.uid, C, len(anno.slices), path
    if len(anno.slices) == 0:
        continue
    try:
        case = adsb3.LunaCase(anno.uid)
    except:
        print 'failed to load', anno.uid
        continue
    case.standardize_color()
    for z, sl in anno.slices.iteritems():
        i = (z - case.origin[0]) / case.spacing[0]
        if int(i) != i:
            logging.warn('offset %f' % i)
        i = int(round(i))
        image = cv2.cvtColor(case.images[i], cv2.COLOR_GRAY2BGR)
        for x, y in sl.non_nodules:
            cv2.circle(image, (int(round(x)), int(round(y))), 10, (0, 255, 0), 1)
        for x, y in sl.small_nodules:
            cv2.circle(image, (int(round(x)), int(round(y))), 5, (255, 0, 0), 1)
        for ft, pts in sl.nodules:
            pts = np.array(pts)
            pts = pts.reshape((1,) + pts.shape).astype(np.int32)
            cv2.polylines(image, pts, True, (0,0,255))
        cv2.imwrite(gal.next(), image)
        C += 1
        pass
    if C > 200:
        break
    pass
gal.flush()


