#!/usr/bin/env python
import math
import sys
import os
import cPickle as pickle
from multiprocessing import Pool
import subprocess
from glob import glob
import numpy as np
import cv2
import datetime
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage.morphology import grey_dilation, binary_dilation
from skimage import measure
from gallery import Gallery
from adsb3 import *
from sklearn.model_selection import KFold
from sklearn.metrics import classification_report
from sklearn.model_selection import cross_val_score
from sklearn.metrics import log_loss
from sklearn.preprocessing import normalize
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier
from xgboost import XGBClassifier
import one_slice

SPLIT=50

def extend (z0, z1, Z, ext):
    zx = ext
    z0 = max(0, z0 - zx)
    z1 = min(Z, z1 + zx)
    return z0, z1

def find_max_box (image, prob):
    ext = 20
    # same as cache_nodule
    binary = prob > 0.01
    binary = binary_dilation(binary, iterations=5)
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    Z, Y, X = image.shape
    boxes = measure.regionprops(labels)
    if len(boxes) == 0:
        return None
    all = []
    for box in boxes:
        z0, y0, x0, z1, y1, x1 = box.bbox
        z0, z1 = extend(z0, z1, Z, ext)
        y0, y1 = extend(y0, y1, Y, ext)
        x0, x1 = extend(x0, x1, X, ext)
        all.append((box, (z1-z0)*(y1-y0)*(x1-x0)))
        pass
    return sorted(all, key=lambda x:-x[1])[0][0]

def grad_swap (image, idx):
    swapped = np.swapaxes(image, idx, 2)
    Z, Y, X = swapped.shape
    swapped2D = np.reshape(swapped, (-1, X))
    g2d = cv2.Sobel(swapped2D, cv2.CV_32F, 1, 0)
    g3d = np.reshape(g2d, (Z, Y, X))
    return np.swapaxes(g3d, idx, 2)

def grad3d (image):
    return grad_swap(image, 2), grad_swap(image, 1), grad_swap(image, 0)

def index3d (shape):
    return (np.fromfunction(lambda i,j,k: k, shape, dtype=np.float32),
            np.fromfunction(lambda i,j,k: j, shape, dtype=np.float32),
            np.fromfunction(lambda i,j,k: i, shape, dtype=np.float32))

def weighted_avg (array, prob):
    return np.sum(array * prob) / np.sum(prob)

def sum_sqr (a, b, c):
    return a * a + b * b + c * c

def irreg (image, prob, box):
    g3x, g3y, g3z = grad3d(image)
    i3x, i3y, i3z = grad3d(gaussian_filter(image, 8))
    
    i3x -= weighted_avg(i3x, prob)
    i3y -= weighted_avg(i3y, prob)
    i3z -= weighted_avg(i3z, prob)

    n = np.sqrt(sum_sqr(i3x, i3y, i3z)) + 0.1
    i3x /= n
    i3y /= n
    i3z /= n

    g3i = g3x * i3x + g3y * i3y + g3z * i3z

    rem = np.sqrt(sum_sqr(g3x, g3y, g3z) - g3i * g3i)

    #return np.sum(rem)

    pos = (prob > 0.1).astype(dtype=np.float32)
    pos_avg = weighted_avg(image, pos)

    neg = (prob < 0.001 * 255).astype(dtype=np.float32)
    neg_avg = weighted_avg(image, neg)

    th = pos_avg * 0.8 + neg_avg * 0.2

    ctrs = np.zeros_like(prob, dtype=np.float32)

    label = measure.label(image <= th)
    vv, cc = np.unique(label, return_counts=True)
    sss = []
    for v in vv:
        pp = np.copy(prob)
        pp[label != v] = 0
        cr = np.sum(pp)
        sss.append((cr, v))
        pass
    _, v = sorted(sss, key=lambda x: -x[0])[0]
    ctrs[label == v] = 1

    g3x, g3y, g3z = grad3d(prob)
    weight = np.sqrt(sum_sqr(g3x, g3y, g3z))
    weight /= weight + 0.1
    weight = gaussian_filter(weight, 5)

    return weighted_avg(rem, weight), rem * weight

def show (gal, image, prob):
    assert image.shape == prob.shape
    box = find_max_box(image, prob)
    if box is None:
        return 0, 0
    mask = np.zeros_like(prob, dtype=np.float32)
    z0, y0, x0, z1, y1, x1 = box.bbox
    mask[z0:z1, y0:y1, x0:x1] = 1.0
    prob *= mask
    #image = image[z0:z1,y0:y1,x0:x1]
    #prob = np.copy(prob[z0:z1,y0:y1,x0:x1])

    total = np.sum(prob)
    if total < 80:
        return 0, 0
    #third, rem = irreg(image, prob, box)
    Z, Y, X = image.shape
    #print 'xxx', math.sqrt(X*Y*Z)
    xxx = np.reshape(prob, (-1, Y * X))
    assert xxx.base is prob
    cv2.normalize(xxx, xxx, 0, 255, cv2.NORM_MINMAX)

    #xxx = np.reshape(rem, (-1, Y * X))
    #assert xxx.base is rem
    #cv2.normalize(xxx, xxx, 0, 255, cv2.NORM_MINMAX)

    vis = []
    for i in range(0, image.shape[0], 2):
        vis.append(np.vstack((image[i], prob[i]))) #, rem[i])))
        pass
    hs = np.hstack(tuple(vis))
    
    color = cv2.cvtColor(hs, cv2.COLOR_GRAY2BGR)
    cv2.putText(color, '%.4f'%total, (0,40), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,255,0), 2)

    if gal:
        cv2.imwrite(gal.next(), color)

    return 1, total #, third 

def get_max (images, probs):
    cc = []
    for image, prob in zip(images, probs):
        assert image.shape == prob.shape
        box = find_max_box(image, prob)
        if box is None:
            continue
        z0, y0, x0, z1, y1, x1 = box.bbox
        total = np.sum(prob[z0:z1, y0:y1, x0:x1])
        if total < 80:
            continue
        cc.append((total, box, image, prob))
    if len(cc) == 0:
        return None, None, None
    return sorted(cc, key=lambda x:-x[0])[0]

big_uids = None
try:
    with open('data/big_nodule', 'r') as f:
        big_uids = pickle.load(f)
except:
    pass

print 'BIG', len(big_uids)

def process (path):
    uid = os.path.splitext(os.path.basename(path))[0]
    if big_uids and not uid in big_uids:
        return (uid, None)
    print uid
    sz = os.path.getsize(path)
    if sz < 100:
        return (uid, None)
    npzfiles = np.load(path)
    idx = [int(x.split('_')[1]) for x in npzfiles.files]
    arrays = [npzfiles['arr_%d' % x] for x in sorted(idx)]
    images = arrays[0::2]
    probs = arrays[1::2]
    outd = '/home/wdong/public_html/nodules/%s' % uid
    #gal = Gallery(outd)
    gal = None
    n = 0
    total = 0
    M = 0

    W, box, image, prob = get_max(images, probs)
    if W is None:
        return (uid, None)
    Z, Y, X = prob.shape
    xxx = np.reshape(prob, (-1, Y * X))
    assert xxx.base is prob
    cv2.normalize(xxx, xxx, 0, 1, cv2.NORM_MINMAX)
    W2 = np.sum(prob)

    if False:
        z0, _, _, z1, _, _ = box.bbox
        #mid = (z0 + z1)/2
        gap = (z1-z0)/3
        z0 += gap
        z1 -= gap
        fts = []
        for z in range(z0, z1):
            ft = one_slice.extract(image[z], prob[z])
            if not ft is None:
                fts.append(ft)
                pass
            pass
        if len(fts) == 0:
            ft = 0
        else:
            ft = sum(fts)/len(fts)

    if gal:
        gal.flush()
    #if n == 0:
    #    subprocess.check_call('rm -rf %s' % outd, shell=True)
    #    return (uid, None)
    return (uid, [W2]) #total, n, M])

paths = glob('nodules/luna/*.npz')

subprocess.check_call('rm -rf /home/wdong/public_html/nodules/*', shell=True)
pool = Pool(None)
uid_ft = pool.map(process, paths)

uid_ft = [(uid, ft) for (uid, ft) in uid_ft if not ft is None]

if big_uids is None:
    big_uids = set([x for x, _ in uid_ft])
    with open('data/big_nodule', 'w') as f:
        pickle.dump(big_uids, f)

DIM = len(uid_ft[0][1])

lookup = {uid:ft for (uid, ft) in uid_ft}

def load_all_ft (uid):
    if uid in lookup:
        return lookup[uid]
    return [0.0] * DIM


U = []
X = []
Y = []
Xt = []

for uid, label in STAGE1.train:
    ft = load_all_ft(uid)
    U.append(uid)
    Y.append(label)
    X.append(ft)

for uid, _ in STAGE1.test:
    Xt.append(load_all_ft(uid))

X = np.array(X, dtype=np.float32)
Y = np.array(Y, dtype=np.float32)
Xt = np.array(Xt, dtype=np.float32)
print('X', X.shape)
print('Y', Y.shape)
print('Xt', Xt.shape)

Y1 = np.sum(Y)
Y0 = len(Y) - Y1
print('neg:', Y0, 'pos:', Y1)

kf = KFold(n_splits=SPLIT, shuffle=True, random_state=88)
y_pred = Y * 0
y_pred_prob = Y * 0
Yt = np.zeros((Xt.shape[0],), dtype=np.float32)

#model = LogisticRegression() #class_weight={0: Y1, 1:Y0})
#model = GradientBoostingClassifier(n_estimators=50, learning_rate=0.1,  max_depth=2, random_state=0)
model = XGBClassifier(n_estimators=100, learning_rate=0.1,max_depth=1,seed=2016)

def pred_wrap (Xin):
    Yout = model.predict_proba(Xin)[:,1]
    #Yout[Yout < 0.01] = 0.01
    #Yout[Yout > 0.99] = 0.99
    return Yout

N = 0
for train, test in kf.split(X):
    X_train, X_test, y_train = X[train,:], X[test,:], Y[train]
    #model.fit(X_train, y_train)
    model.fit(X_train, y_train)

    y_pred[test] = model.predict(X_test)
    #try:
    y_pred_prob[test] = pred_wrap(X_test) #model.predict_proba(X_test)[:,1]
    Yt += pred_wrap(Xt) #model.predict_proba(Xt)[:,1]
    N += 1
    #except:
    #    y_pred_prob[test] = y_pred[test]

try:
    print(classification_report(Y, y_pred, target_names=["0", "1"]))
except:
    pass
#print(zip(Y, y_pred_prob))
print("logloss",log_loss(Y, y_pred_prob))
    #X = X[:, 0:1]
    #print("baseline:")

assert N == SPLIT
loss = Y * y_pred_prob + (1-Y) * (1 - y_pred_prob)
rank = []
for i in range(len(loss)):
    rank.append((loss[i], Y[i], U[i]))
    pass
rank = sorted(rank, key=lambda x:x[0])
with open('rank', 'w') as f:
    f.write('\n'.join('%s %d %.4f' % (uid, label, loss) for loss, label, uid in rank))

with open('/home/wdong/public_html/miss.html', 'w') as f:
    f.write('<html><body><table>\n')
    for i in range(len(loss)):
        if (X[i][0]==0) and Y[i] > 0:
            f.write('<tr><td><a target="_blank" href="lungs/%s.html">%s</a></td></tr>\n' % (U[i], U[i]))
    f.write('</table></body></html>\n')
sys.exit(0)

Yt /= N
test_uids = [uid for uid, _ in STAGE1.test]
test_ys = list(Yt)

test_meta = zip(test_uids, test_ys)

prefix = os.path.join('submit', datetime.datetime.now().strftime('%m%d-%H%M')[1:])
if os.path.exists(prefix + '.submit'):
    print("submission %s already exists, not overwriting." % prefix)
    sys.exit(0)
dump_meta(prefix + '.submit', test_meta)
with open(prefix + '.cmd', 'w') as f:
    f.write(' '.join(sys.argv))
    f.write('\n')
fig = plt.figure()
ax = fig.add_subplot(111)
ax.hist(Yt, bins=20)
fig.savefig(prefix + '.jpg')

COLS = [i+1 for i in range(X.shape[1])]

def libsvm_row (y, x):
    return '%g %s\n' % (y,
            ' '.join(['%d:%.6f' % (c, v) for c, v in zip(COLS, list(x))]))

with open(prefix + '.train', 'w') as f:
    for i in range(len(Y)):
        f.write(libsvm_row(Y[i], list(X[i])))
with open(prefix + '.test', 'w') as f:
    for i in range(Xt.shape[0]):
        f.write(libsvm_row(0, list(Xt[i])))

# generate bad cases
with open(prefix + '.html', 'w') as f:
    f.write('<html><body><table><tr><th>class</th><th>prob</th><th>uid</th></tr>\n')
    v = zip(Y, list(Y * y_pred_prob + (1-Y)*(1- y_pred_prob)), U, X)
    v = sorted(v, key=lambda x: x[1])
    for y, p, uid, x in v:
        ft_txt = ' '.join(['%.4f' % ft for ft in x])
        f.write('<tr><td>%d</td><td>%.4f</td><td><a target="_blank" href="http://localhost/~wdong/lungs/%s.html">%s</a></td><td>%s</td></tr>\n' % (y, p, uid, uid, ft_txt))
        pass
    f.write('</table></body></html>\n')

print('Submission generated:', prefix)
