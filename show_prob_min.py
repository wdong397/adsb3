#!/usr/bin/env python
import sys
import argparse
import numpy as np
from skimage import measure
from adsb3 import *
from gallery import Gallery
from papaya import Papaya, Annotations

parser = argparse.ArgumentParser(description='')
parser.add_argument('--model')
parser.add_argument('--cache')
parser.add_argument('--uid')
args = parser.parse_args()

uid = args.uid

def find_center (prob, bbox):
    z0, y0, x0, z1, y1, x1 = bbox
    return [(z0+z1)/2, (y0+y1)/2, (x0+x1)/2], [(z1-z0)/2, (y1-y0)/2, (x1-x0)/2]

def find_blobs (gal, case, prob):
    binary = prob > 0.1
    labels = measure.label(binary, background=0)
    #vv, cc = np.unique(labels, return_counts=True)
    #print zip(vv,cc)
    pp = measure.regionprops(labels)
    boxes = []
    image_views = [
                    case.transpose_array(VIEWS[0], case.images),
                    case.transpose_array(VIEWS[1], case.images),
                    case.transpose_array(VIEWS[2], case.images),
                  ]
    prob_views = [
                    case.transpose_array(VIEWS[0], prob),
                    case.transpose_array(VIEWS[1], prob),
                    case.transpose_array(VIEWS[2], prob),
                ]
                    
    for roi in pp:
        if roi.area < 10:
            continue
        boxes.append(roi.bbox)
        C, R = find_center(prob, roi.bbox)
        for view in VIEWS:
            images = image_views[view]
            pp = prob_views[view]
            c = index_view(C, view)
            r = index_view(R, view)
            I = cv2.cvtColor(images[c[0]], cv2.COLOR_GRAY2BGR)
            P = cv2.cvtColor(pp[c[0]], cv2.COLOR_GRAY2BGR) * 255
            cv2.circle(I, (c[2], c[1]), max(r[1], r[2]), (0, 255, 0))
            cv2.circle(P, (c[2], c[1]), max(r[1], r[2]), (0, 255, 0))
            cv2.imwrite(gal.next(), np.hstack((I, P)))
            #vis.append(I)
            pass
        #vis = np.hstack(tuple(vis))
        pass
    return boxes

def round_stride (images, stride=16):
    _, H, W = images.shape[:3]
    H = H / stride * stride
    W = W / stride * stride
    return images[:,0:H,0:W]

def compute_model (case, model):
    import tensorflow as tf
    import nets
    X = tf.placeholder(tf.float32, shape=(None, None, None), name="images")
    X4 = tf.expand_dims(X, axis=3)
    PROB, loader = nets.import_meta_graph(model, X4, 'show', softmax=True)

    config = tf.ConfigProto()

    bn = os.path.basename(model)
    if bn == 'axial':
        view = AXIAL
    elif bn == 'sagittal':
        view = SAGITTAL
    elif bn == 'coronal':
        view = CORONAL
    else:
        assert False
    case = case.transpose(view)
    with tf.Session(config=config) as sess:
        tf.global_variables_initializer().run()
        loader(sess)
        prob = np.zeros_like(case.images, dtype=np.float32)
        BATCH = 32
        for b in range(0, case.images.shape[0], BATCH):
            e = min(case.images.shape[0], b + BATCH)
            x = case.images[b:e]
            y, = sess.run([PROB], feed_dict={X:x})
            prob[b:e] = y[:,:,:]
            print '.'
        return case.transpose_array(AXIAL, prob)

def load_cache (uid, cache):
    return np.load(os.path.join('cache', cache, uid + '.npz'))['arr_0']

gal = Gallery('/home/wdong/public_html/roi_cross', cols=3, header=['axial', 'sagittal', 'coronal'])    
pap = Papaya('/home/wdong/public_html/roi_full')

case = load_8bit_lungs(uid)
print case.images.shape
case = case.rescale3D(SPACING)
case.round_stride()

if args.model:
    prob = compute_model(case, args.model)
elif args.cache:
    prob = load_cache(uid, args.cache)
else:
    assert False

boxes = find_blobs(gal, case, prob)
annos = Annotations()
for box in boxes:
    annos.add(box, '')
pap.next(case, annos)
gal.flush()
pap.flush()
