#!/usr/bin/env python
from glob import glob
import xml.etree.ElementTree as ET
import adsb3
import logging
import cv2
from gallery import Gallery

def xmlget (parent, text):
    for c in parent.getchildren():
        t = c.tag.lower()
        if '}' in t:
            t = t[(t.find('}')+1):]
        if text in t:
            return c

def xmlgetfloat (parent, text):
    return float(xmlget(parent, text).text.strip())

def xmlgetall (parent, text):
    for c in parent.getchildren():
        t = c.tag.lower()
        if '}' in t:
            t = t[(t.find('}')+1):]
        if text in t:
            yield c
        pass
    pass

logging.basicConfig(level=logging.INFO)
#for path in glob('data/tcia/tcia-lidc-xml/185/069.xml'):
gal = Gallery('/home/wdong/public_html/nonnodule')
C = 0
for path in glob('data/tcia/tcia-lidc-xml/*/*.xml'):
    print path
    root = ET.parse(path).getroot()
    header = xmlget(root, 'responseheader')
    uid = xmlget(header, 'seriesinstanceuid')
    uid = uid.text.strip()
    logging.info(uid)
    anno = {}
    for session in xmlgetall(root, 'readingsession'):
        for non in xmlgetall(session, 'nonnodule'):
            locus = xmlget(non, 'locus')
            z = xmlgetfloat(non, 'imagezposition')
            x = xmlgetfloat(locus, 'xcoord')
            y = xmlgetfloat(locus, 'ycoord')
            print z, x, y
            anno.setdefault(z, []).append((x,y))
    if len(anno) > 1:
        try:
            case = adsb3.LunaCase(uid)
        except:
            continue
        case.standardize_color()
        for z, nons in anno.iteritems():
            i = (z - case.origin[0]) / case.spacing[0]
            if int(i) != i:
                logging.warn('offset %f' % i)
            i = int(round(i))
            image = cv2.cvtColor(case.images[i], cv2.COLOR_GRAY2BGR)
            for x, y in nons:
                cv2.circle(image, (int(round(x)), int(round(y))), 10, (0, 255, 0), 1)
            cv2.imwrite(gal.next(), image)
            C += 1
            pass
        pass
    if C > 200:
        break
gal.flush()


