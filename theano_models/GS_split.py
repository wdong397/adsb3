#!/usr/bin/env python

import random
import cv2
import numpy as np

def GS_split (FILE,size):
    all_train_line=[]
    TRAIN_LIST=open(FILE,'r')

    ## count pos and ge

    pos_count=0
    neg_count=0
    
    for line in TRAIN_LIST:
        line=line.rstrip()
        all_train_line.append(line)
        table=line.split('\t')
        if (table[1]=='negative_pair.tif'):
            neg_count=neg_count+1
        else:
            pos_count=pos_count+1

    ratio=neg_count/pos_count

    random.shuffle(all_train_line)
    
## first time split into 2 fold;

    i=0
    all_train_train_line=[]
    all_train_test_line=[]
    cut1=int(len(all_train_line)/2)
    cut2=int(len(all_train_line))

    for line in all_train_line:
        table=line.split('\t')
        if (i<cut1):
            all_train_train_line.append(line)

        else:
            all_train_test_line.append(line)
        i=i+1


    random.shuffle(all_train_train_line)
    random.shuffle(all_train_test_line)

    print(len(all_train_train_line))
    print(len(all_train_test_line))

    train_input= [] 
    train_label= []

    for line in all_train_train_line:
        table=line.split('\t')
        img= cv2.imread(table[0],-1)
        img= cv2.resize(img,(size,size))
        img= img.reshape(1,size,size)
        label=cv2.imread(table[1],cv2.CV_LOAD_IMAGE_GRAYSCALE)
        label= cv2.resize(label,(size,size))
        label=label.reshape(1,size,size)
        label=label/255
        train_input.append(img)
        train_label.append(label)


    test_input= [] 
    test_label= []


    for line in all_train_test_line:
        table=line.split('\t')
        img= cv2.imread(table[0],-1)
        img= cv2.resize(img,(size,size))
        img= img.reshape(1,size,size)
        label=cv2.imread(table[1],cv2.CV_LOAD_IMAGE_GRAYSCALE)
        label= cv2.resize(label,(size,size))
        label=label.reshape(1,size,size)
        label=label/255
        test_input.append(img)
        test_label.append(label)

    test_input=np.asarray(test_input)
    test_label=np.asarray(test_label)
    train_label=np.asarray(train_label)
    train_input=np.asarray(train_input)

    return (train_input, train_label, test_input, test_label)
