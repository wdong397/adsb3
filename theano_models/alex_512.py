from theano import tensor as T
import lasagne as nn
from lasagne.layers import batch_norm as bn

#def sorenson_dice(pred, tgt, ss=10):
def sorenson_dice(pred, tgt, ss=0):
    return -2*(T.sum(pred*tgt)+ss)/(T.sum(pred) + T.sum(tgt) + ss) 

def network(input_var, label_var, shape):
    layer = nn.layers.InputLayer(shape, input_var)                          # 512
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=8, filter_size=5))  # 508
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=16, filter_size=5))  # 504
    layer = nn.layers.MaxPool2DLayer(layer, pool_size=2)                    # 252
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=32, filter_size=5))  # 248
    layer = nn.layers.MaxPool2DLayer(layer, pool_size=2)                    # 124
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=5)) # 120
    layer = nn.layers.MaxPool2DLayer(layer, pool_size=2)                    # 60
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=5)) # 56
    layer = nn.layers.MaxPool2DLayer(layer, pool_size=2)                    # 28
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=256, filter_size=5)) # 24 
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=256, filter_size=5, pad='full')) # 24 + 8 - 4 =  28
    layer = nn.layers.Upscale2DLayer(layer, scale_factor=2)                 # 56
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=5, pad='full')) # 60
    layer = nn.layers.Upscale2DLayer(layer, scale_factor=2)                 # 120
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=5, pad='full')) # 124
    layer = nn.layers.Upscale2DLayer(layer, scale_factor=2)                 # 248
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=32, filter_size=5, pad='full')) #252
    layer = nn.layers.Upscale2DLayer(layer, scale_factor=2)                 # 504
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=16, filter_size=5, pad='full'))  # 508
    layer = nn.layers.Conv2DLayer(layer, num_filters=1, filter_size=5, pad='full',
                nonlinearity=nn.nonlinearities.sigmoid)                     # 512

    #for l in nn.layers.get_all_layers(layer):
    #    print nn.layers.get_output_shape(l)

    output = nn.layers.get_output(layer)
    output_det = nn.layers.get_output(layer, deterministic=True)

    loss = sorenson_dice(output, label_var) #, ss=ss)
    te_loss = sorenson_dice(output_det, label_var) #,ss=ss)
    te_acc = nn.objectives.binary_accuracy(output_det, label_var).mean()
    return layer, loss, te_loss, te_acc
