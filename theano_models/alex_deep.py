from theano import tensor as T
import lasagne as nn
from lasagne.layers import batch_norm as bn

#def sorenson_dice(pred, tgt, ss=10):
def sorenson_dice(pred, tgt, ss=10):
    return -2*(T.sum(pred*tgt)+ss)/(T.sum(pred) + T.sum(tgt) + ss) 

def network(input_var, label_var, shape):
    layer = nn.layers.InputLayer(shape, input_var)                          # 256
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=2, filter_size=5))  # 252
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=4, filter_size=3)) # 250
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=8, filter_size=5)) # 246
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=16, filter_size=5)) # 242
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=16, filter_size=5)) # 238
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=32, filter_size=5)) # 234
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=32, filter_size=5)) # 230
    layer = nn.layers.MaxPool2DLayer(layer, pool_size=2)                    # 115
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=5)) # 111
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=5)) # 107
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=4)) # 104
    layer = nn.layers.MaxPool2DLayer(layer, pool_size=2)                    # 52
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=5)) # 48
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=5)) # 44
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=5, pad='full')) # 48
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=5, pad='full')) # 52
    layer = nn.layers.Upscale2DLayer(layer, scale_factor=2)                 # 104
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=4, pad='full')) # 107
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=5, pad='full')) # 111
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=5, pad='full')) # 115
    layer = nn.layers.Upscale2DLayer(layer, scale_factor=2)                 # 230
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=32, filter_size=5, pad='full')) #234
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=32, filter_size=5, pad='full')) #238
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=16, filter_size=5, pad='full')) #242
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=8, filter_size=5, pad='full')) #246
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=4, filter_size=5, pad='full')) #250
    layer = bn(nn.layers.Conv2DLayer(layer, num_filters=2, filter_size=3, pad='full')) #252
    layer = nn.layers.Conv2DLayer(layer, num_filters=1, filter_size=5, pad='full',
                nonlinearity=nn.nonlinearities.sigmoid)                     # 256

    #for l in nn.layers.get_all_layers(layer):
    #    print nn.layers.get_output_shape(l)

    output = nn.layers.get_output(layer)
    output_det = nn.layers.get_output(layer, deterministic=True)

    loss = sorenson_dice(output, label_var) #, ss=ss)
    te_loss = sorenson_dice(output_det, label_var) #,ss=ss)
    te_acc = nn.objectives.binary_accuracy(output_det, label_var).mean()
    return layer, loss, te_loss, te_acc
