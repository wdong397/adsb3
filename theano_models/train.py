#!/usr/bin/env python
import sys
import cv2
import numpy as np
import theano
from theano import tensor as T
from theano import shared 
import lasagne
import pkgutil
import pickle
import random
import skimage.transform as sktrans
import scipy.ndimage.interpolation
from scipy.ndimage import zoom

## import DM specific modules
import GS_split as GS_split


## argv[1]=TRAIN_LIST, argv[1]: x,y, size

size=int(sys.argv[2])


batch=16
max_rotate=20
max_shift=25

def run_epoch(input_var,label_var,fn,if_train):
    count=int(input_var.shape[0]/batch)
    i=0
    err = 0 
    while (i<count):
        tmp_input=[] 
        tmp_label=[] 
        jjj=i*batch
        while (jjj<(i*batch+batch)):
                img=input_var[jjj,:,:]
                img=img.reshape(size,size)
                ## rotate
                rrr=(random.random()-0.5)*2
                rrr_rotate=rrr*max_rotate*if_train
                img=scipy.ndimage.interpolation.rotate(img, rrr_rotate,reshape=False)

                ## shift
                rrr=(random.random()-0.5)*2
                rrr_shift_x=rrr*max_shift*if_train
                rrr=(random.random()-0.5)*2
                rrr_shift_y=rrr*max_shift*if_train
                img=scipy.ndimage.interpolation.shift(img, [rrr_shift_x,rrr_shift_y])
                img=img.reshape(1,size,size)
                tmp_input.append(img)

                label=label_var[jjj,:,:]
                label=label.reshape(size,size)
                label=scipy.ndimage.interpolation.rotate(label, rrr_rotate,reshape=False)
                label=scipy.ndimage.interpolation.shift(label, [rrr_shift_x,rrr_shift_y])
                label=label.reshape(1,size,size)
                tmp_label.append(label)
                jjj=jjj+1

        tmp_input_a=np.asarray(tmp_input)
        tmp_label_a=np.asarray(tmp_label)
        e = fn(tmp_input_a, tmp_label_a)
        #print(e)

        err+=(e/batch)
        i=i+1
    err=err/count;
    return err



## train the model
# model setup

def run_params(train_input_var, train_label_var, test_input_var, test_label_var,fold):
    record=open(fold,'w')

    input_var= T.tensor4('input')
    label_var= T.tensor4('label')
    loader = pkgutil.get_importer('./')
    #model=sys.argv[3]
    model='unet'
    model = loader.find_module(model).load_module(model)
    net, loss, te_loss, te_acc= model.network(input_var, label_var, [batch,1,size,size])
    params = lasagne.layers.get_all_params(net, trainable=True)
    #lr = theano.shared(lasagne.utils.floatX(3e-3))
    lr = theano.shared(lasagne.utils.floatX(1e-3))
    updates = lasagne.updates.adam(loss, params, learning_rate=lr)
    best = None # (score, epoch, params)
    epoch=1
    max_epoch=101
    train_fn = theano.function([input_var, label_var], loss, updates=updates,allow_input_downcast=True)
    test_fn = theano.function([input_var, label_var], te_loss, allow_input_downcast=True)
    best_te_err=100
    while (epoch<=max_epoch):
        tr_err = run_epoch(train_input_var,train_label_var, train_fn,1)
        te_err = run_epoch(test_input_var, test_label_var,test_fn,0)

        if (te_err<best_te_err):
            best =[np.copy(p) for p in (lasagne.layers.get_all_param_values(net))]
            best_te_err=te_err
            best_epoch=epoch

        print (best_epoch,tr_err, te_err)
        if (epoch%50==0):
            te_err = run_epoch(test_input_var, test_label_var,test_fn,0)
            #param_vals = lasagne.layers.get_all_param_values(best[2])
            params_file=fold+'_params_'+str(epoch)
            
            with open(params_file, 'w') as wr:
                pickle.dump(best, wr)
            record.write('%d\t%d\t%.4f\t%.4f\n' % (epoch,best_epoch,tr_err,te_err))
            print(tr_err,te_err)

        epoch=epoch+1
    record.close()


(fold_train_input, fold_train_label, fold_test_input, fold_test_label)=GS_split.GS_split(sys.argv[1],size)
run_params(fold_train_input, fold_train_label, fold_test_input, fold_test_label,'fold4')

(fold_train_input, fold_train_label, fold_test_input, fold_test_label)=GS_split.GS_split(sys.argv[1],size)
run_params(fold_train_input, fold_train_label, fold_test_input, fold_test_label,'fold3')

(fold_train_input, fold_train_label, fold_test_input, fold_test_label)=GS_split.GS_split(sys.argv[1],size)
run_params(fold_train_input, fold_train_label, fold_test_input, fold_test_label,'fold2')

(fold_train_input, fold_train_label, fold_test_input, fold_test_label)=GS_split.GS_split(sys.argv[1],size)
run_params(fold_train_input, fold_train_label, fold_test_input, fold_test_label,'fold1')


(fold_train_input, fold_train_label, fold_test_input, fold_test_label)=GS_split.GS_split(sys.argv[1],size)
run_params(fold_train_input, fold_train_label, fold_test_input, fold_test_label,'fold0')
