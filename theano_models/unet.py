from theano import tensor as T
import lasagne as nn

def sorenson_dice(pred,target):
    return -2*T.sum(pred*target)/(pred+target+20)


def network (input_var, label_var, shape):
    layer=nn.layers.InputLayer(shape,input_var) #572
    layer=nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=3) #570
    layer=nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=3) #568
    layer=nn.layers.MaxPool2DLayer(layer,pool_size=2) #284
    layer=nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=3) #282
    layer=nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=3) #280
    layer=nn.layers.MaxPool2DLayer(layer,pool_size=2) #140
    layer=nn.layers.Conv2DLayer(layer, num_filters=256, filter_size=3) #138
    layer=nn.layers.Conv2DLayer(layer, num_filters=256, filter_size=3) #136
    layer=nn.layers.MaxPool2DLayer(layer,pool_size=2) #68
    layer=nn.layers.Conv2DLayer(layer, num_filters=512, filter_size=3) #66
    layer=nn.layers.Conv2DLayer(layer, num_filters=512, filter_size=3) #64
    layer=nn.layers.MaxPool2DLayer(layer,pool_size=2) #32
    layer=nn.layers.Conv2DLayer(layer, num_filters=1024, filter_size=3) #30
    layer=nn.layers.Conv2DLayer(layer, num_filters=1024, filter_size=3,pad='full') #32=30-2+2+2
    layer=nn.layers.Upscale2DLayer(layer,scale_factor=2) #64=32*2
    layer=nn.layers.Conv2DLayer(layer, num_filters=512, filter_size=3,pad='full') #66=64-2+2+2
    layer=nn.layers.Conv2DLayer(layer, num_filters=512, filter_size=3,pad='full') #68=66-2+2+2
    layer=nn.layers.Upscale2DLayer(layer,scale_factor=2) #136
    layer=nn.layers.Conv2DLayer(layer, num_filters=256, filter_size=3,pad='full') #138
    layer=nn.layers.Conv2DLayer(layer, num_filters=256, filter_size=3,pad='full') #140
    layer=nn.layers.Upscale2DLayer(layer,scale_factor=2) #280
    layer=nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=3,pad='full') #282 
    layer=nn.layers.Conv2DLayer(layer, num_filters=128, filter_size=3,pad='full') #284

    layer=nn.layers.Upscale2DLayer(layer,scale_factor=2) #568
    layer=nn.layers.Conv2DLayer(layer, num_filters=64, filter_size=3,pad='full') #570
    layer=nn.layers.Conv2DLayer(layer, num_filters=1, filter_size=3, pad='full', nonlinearality=nn.nonlinearities.sigmoid) #572


    output=nn.layers.get_output(layer)
    output_det=nn.layers.get_output(layer,deterministic=True)

    loss=sorenson_dice(output,label_var)
    te_loss=sorenson_dice(output_det,label_var)

    te_acc=nn.objectives.binary_accuracy(output_det,label_var)
    return layer, loss, te_loss,te_acc

