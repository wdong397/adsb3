#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import time
import logging
import random
from tqdm import tqdm
from skimage import measure
import cv2
import numpy as np
import tensorflow as tf
import tensorflow.contrib.slim as slim
from adsb3 import *
import picpac
import picpac3d
import tripod_nets
from gallery import Gallery

flags = tf.app.flags
FLAGS = flags.FLAGS
flags.DEFINE_string('luna_fg', 'luna.fg', '')
flags.DEFINE_string('luna_bg', 'luna.bg', '')
flags.DEFINE_string('db', 'half0', '')
flags.DEFINE_string('net', 'tiny', '')
flags.DEFINE_string('opt', 'adam', '')
flags.DEFINE_float('learning_rate', 0.01, 'Initial learning rate.')
flags.DEFINE_bool('decay', True, '')
flags.DEFINE_float('decay_rate', 0.9, '')
flags.DEFINE_float('decay_steps', 10000, '')
flags.DEFINE_float('momentum', 0.99, 'when opt==mom')
flags.DEFINE_string('model', 'model', 'Directory to put the training data.')
flags.DEFINE_string('resume', None, '')
flags.DEFINE_integer('max_steps', 1000000, '')
flags.DEFINE_integer('epoch_steps', 100, '')
flags.DEFINE_integer('ckpt_epochs', 100, '')
flags.DEFINE_string('log', None, 'tensorboard')
flags.DEFINE_integer('channels', 3, '')
flags.DEFINE_integer('verbose', logging.INFO, '')
flags.DEFINE_integer('max_to_keep', 1000, '')
flags.DEFINE_bool('pert_hflip', True, '')
flags.DEFINE_bool('pert_vflip', True, '')
flags.DEFINE_float('pert_color1', 20, '')
flags.DEFINE_float('gpu_memory', 0.95, '')
flags.DEFINE_float('keep', 0.5, '')
flags.DEFINE_bool('pretrain', None, '')
flags.DEFINE_integer('sample', 3, '')
flags.DEFINE_integer('extend', 12, '')
flags.DEFINE_integer('topk', 4, '')
flags.DEFINE_integer('pool', 128, '')
flags.DEFINE_float('pos_weight', None, '')
flags.DEFINE_integer('seed', 2019, '')

def e2e_logits2prob (v):
    v = tf.nn.softmax(v)
    # keep prob of 1 only
    v = tf.slice(v, [0, 1], [-1, -1])
    # remove trailing dimension of 1
    v = tf.squeeze(v, axis=[1])
    return v

def fcn_loss_fun (logits, labels):
    # to HWC
    logits = tf.reshape(logits, (-1, 2))
    labels = tf.reshape(labels, (-1,))
    xe = tf.nn.sparse_softmax_cross_entropy_with_logits(logits, tf.to_int32(labels))
    if FLAGS.pos_weight:
        POS_W = tf.pow(tf.constant(FLAGS.pos_weight, dtype=tf.float32),
                       labels)
        xe = tf.multiply(xe, POS_W)
    loss = tf.reduce_mean(xe, name='fcn_xe')
    return loss

def e2e_loss_fun (logits, labels):
    logits = tf.reshape(logits, (-1, 2))
    labels = tf.reshape(labels, (-1,))
    xe = tf.nn.sparse_softmax_cross_entropy_with_logits(logits, tf.to_int32(labels))
    loss = tf.reduce_mean(xe, name='e2e_xe')
    return loss


def volume2images (images):
    sample = FLAGS.sample
    off = random.randint(0, FLAGS.sample-1)
    xx = []
    yy = []
    for i in range(GAP + off, images.shape[0]-GAP, sample):
        xx.append(get3c(images, i))
        yy.append(i)
    return np.stack(xx), yy

def volume2images_range (images, key):
    xx = []
    for i in range(key-FLAGS.extend, key+FLAGS.extend+1):
        a = get3c(images, i)
        if not a is None:
            xx.append(a)
    return np.stack(xx)

def visualize (image, label):
    image = np.copy(image[0,:,:,:])
    label = label[0,:,:,0]
    contours = measure.find_contours(label, 0.5)
    for contour in contours:
        tmp = np.copy(contour[:,0])
        contour[:, 0] = contour[:, 1]
        contour[:, 1] = tmp
        contour = contour.reshape((1, -1, 2)).astype(np.int32)
        cv2.polylines(image, contour, True, (0, 255, 0))
    return image

def main (_):
    logging.basicConfig(level=FLAGS.verbose)
    try_mkdir(FLAGS.model)
    try_mkdir('/home/wdong/public_html/tripod/luna')
    assert FLAGS.db and os.path.exists(FLAGS.db)
    assert FLAGS.luna_fg and os.path.exists(FLAGS.luna_fg)
    assert FLAGS.luna_bg and os.path.exists(FLAGS.luna_bg)

    X = tf.placeholder(tf.float32, shape=(None, None, None, FLAGS.channels), name="images")
    e2e_Y = tf.placeholder(tf.float32, shape=(None,), name="e2e_labels")
    fcn_Y = tf.placeholder(tf.float32, shape=(None, None, None, 1), name="fcn_labels")
    KEEP = tf.placeholder(tf.float32, shape=(), name='keep')

    e2e_logits, fcn_logits, stride = getattr(tripod_nets, FLAGS.net)(X, KEEP)

    fcn_loss = fcn_loss_fun(fcn_logits, fcn_Y)
    e2e_loss = e2e_loss_fun(e2e_logits, e2e_Y)
    e2e_prob = e2e_logits2prob(e2e_logits)

    rate = FLAGS.learning_rate
    if FLAGS.opt == 'adam':
        rate /= 100
    global_step = tf.Variable(0, name='global_step', trainable=False)
    reset_global_step = global_step.assign(0)
    if FLAGS.decay:
        rate = tf.train.exponential_decay(rate, global_step, FLAGS.decay_steps, FLAGS.decay_rate, staircase=True)
    if FLAGS.opt == 'adam':
        optimizer = tf.train.AdamOptimizer(rate)
    elif FLAGS.opt == 'mom':
        optimizer = tf.train.MomentumOptimizer(rate, FLAGS.momentum)
    else:
        optimizer = tf.train.GradientDescentOptimizer(rate)
        pass

    train_fcn = optimizer.minimize(fcn_loss, global_step=global_step)
    train_e2e = optimizer.minimize(e2e_loss, global_step=global_step)
    train_neg = optimizer.minimize(fcn_loss + e2e_loss, global_step=global_step)

    config_common = dict(seed=FLAGS.seed, shuffle=True, reshuffle=True, batch=1,
                pert_colorspace='SAME', pert_color1=20,
                pert_angle=20,
                split=1,
                split_fold=0,
                stratify=True,
                pert_min_scale=0.85, pert_max_scale=1.15,
                pert_hflip=True, pert_vflip=False)
    config_2d = dict(round_div=stride,
                annotate='json',
                channels=FLAGS.channels,
                mixin=FLAGS.luna_bg,
                mixin_group_delta=1,
                channel_first=False
                )
    config_2d.update(config_common)
    config_3d = dict(threads=1, decode_threads=4, preload=32, channels=1, stride=1)
    config_3d.update(config_common)

    luna_stream = picpac.ImageStream(FLAGS.luna_fg, perturb=True, loop=True, cache=True, **config_2d)
    db_stream = picpac3d.VolumeStream(FLAGS.db, perturb=True, loop=True, cache=False, **config_3d)

    init = tf.global_variables_initializer()

    saver = tf.train.Saver(max_to_keep=FLAGS.max_to_keep)
    config = tf.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = FLAGS.gpu_memory

    if 'pre' in FLAGS.resume:
        restorer = tf.train.Saver(slim.get_variables_to_restore(exclude=["tinyX"]))
    else:
        restorer = saver

    with tf.Session(config=config) as sess:
        sess.run(init)
        if FLAGS.resume:
            restorer.restore(sess, FLAGS.resume)
        sess.run(reset_global_step)
        step = 0
        epoch = 0
        global_start_time = time.time()
        e2e_labels = [np.array([0], dtype=np.float32), np.array([1], dtype=np.float32)]

        db_cache = []
        while step < FLAGS.max_steps:
            start_time = time.time()
            fcn_xe = 0
            e2e_xe = 0
            e2e_xe_pos = 0
            e2e_xe_neg = 0
            e2e_pos = 0
            for _ in tqdm(range(FLAGS.epoch_steps), leave=False):
                if True:
                    images, labels, _ = luna_stream.next()

                    feed_dict = {X: images, fcn_Y: labels, KEEP: FLAGS.keep}
                    cv2.imwrite('/home/wdong/public_html/tripod/luna/%d.png' % (step % 10), visualize(images, labels))
                    l, _ = sess.run([fcn_loss, train_fcn], feed_dict=feed_dict)
                    fcn_xe += l

                #if step < 10000:
                #    step += 1
                #    continue
                if FLAGS.pretrain:
                    #images, _, _ = luna_stream.next()
                    pass
                else:
                    if len(db_cache) < FLAGS.pool:
                        label, volume = db_stream.next()
                        label = int(label)
                        gal = Gallery('/home/wdong/public_html/tripod/%d' % label)
                        images, images_index = volume2images(volume)
                        ys0, = sess.run([e2e_prob], feed_dict={X:images, KEEP:1.0})
                        key = np.argmax(ys0)

                        images = volume2images_range(volume, images_index[key])
                        ys, = sess.run([e2e_prob], feed_dict={X:images, KEEP:1.0})


                        topk = [(i, v) for i, v in enumerate(ys)]
                        topk = sorted(topk, key = lambda x:-x[1])
                        if label > 0:
                            topk = topk[:FLAGS.topk]
                        #print()
                        #print(key, ys0[key], topk, ys0)
                        for i, v in topk:
                            vis = np.copy(images[i])
                            cv2.putText(vis, '%d: %.3f' % (label, v), (10, 20), 
                                    cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 255, 0))

                            cv2.imwrite(gal.next(), vis)
                            db_cache.append((label, images[i:(i+1)]))
                            pass
                        gal.flush()
                        #print('volume loaded %d' % label)
                        random.shuffle(db_cache)
                    label, images = db_cache.pop()

                feed_dict = {X: images, e2e_Y: e2e_labels[label], KEEP: FLAGS.keep}
                l, _ = sess.run([e2e_loss, train_e2e], feed_dict=feed_dict)
                e2e_xe += l
                if label > 0:
                    e2e_xe_pos += l
                    e2e_pos += 1
                else:
                    e2e_xe_neg += l

                step += 1
                pass
            fcn_xe /= FLAGS.epoch_steps
            e2e_xe /= FLAGS.epoch_steps
            e2e_xe_pos /= e2e_pos
            e2e_xe_neg /= (FLAGS.epoch_steps - e2e_pos)
            e2e_pos = 1.0 * e2e_pos / FLAGS.epoch_steps
            stop_time = time.time()
            cur_rate, = sess.run([rate])
            print('step %d: el=%.2f ti=%.2f ra=%.5f, fcn %.3f, e2e %%=%.2f p=%.3f n=%.3f xe=%.3f'
                    % (step, (stop_time - global_start_time), (stop_time - start_time), cur_rate,
                        fcn_xe, e2e_pos, e2e_xe_pos, e2e_xe_neg, e2e_xe))
            epoch += 1
            if epoch and (epoch % FLAGS.ckpt_epochs == 0):
                ckpt_path = '%s/%d' % (FLAGS.model, step)
                start_time = time.time()
                saver.save(sess, ckpt_path)
                stop_time = time.time()
                print('epoch %d step %d, saving to %s in %.4fs.' % (epoch, step, ckpt_path, stop_time - start_time))
            pass
        pass
    pass

if __name__ == '__main__':
    tf.app.run()

