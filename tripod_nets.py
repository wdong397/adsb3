#!/usr/bin/env python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import os
import tensorflow as tf
import tensorflow.contrib.slim as slim
from tensorflow.contrib.slim.nets import resnet_v1
from tensorflow.contrib.slim.nets import resnet_utils

def tiny (X, KEEP, num_classes=2):
    net = X
    layers = [X]
    with slim.arg_scope([slim.conv2d, slim.max_pool2d, slim.conv2d_transpose], padding='SAME'):
        net = slim.conv2d(net, 32, 3, 2)        #
        net = slim.conv2d(net, 64, 3, 1)        #
        net = slim.max_pool2d(net, 2, 2)        #
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.max_pool2d(net, 2, 2)        #
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.max_pool2d(net, 2, 2)        #
        net = slim.conv2d(net, 512, 3, 1)
        net = slim.dropout(net, keep_prob=KEEP, scope='dropout')
        net = slim.conv2d(net, 128, 3, 1)
        choke = tf.identity(net, 'choke')       # 16x16x256

        net = slim.conv2d_transpose(choke, 32, 9, 4)
        net = slim.conv2d_transpose(net, 2, 9, 4, activation_fn=None, normalizer_fn=None)
    fcn_logits = tf.identity(net, 'fcn_logits')
    with slim.arg_scope([slim.conv2d, slim.max_pool2d], padding='SAME'):
        net = slim.max_pool2d(choke, 2, 2)      # 8x8x256
        #net = slim.conv2d(net, 64, 3, 1, scope='tinyX/a1')
        net = slim.conv2d(net, 32, 3, 1, scope='tinyX/a2')
        #net = slim.max_pool2d(net, 2, 2)      # 8x8x256
        #net = slim.conv2d(net, 32, 3, 1, scope='tinyX/a3')
        net = slim.conv2d(net, 16, 3, 1, scope='tinyX/a4')
        net = tf.reduce_mean(net, [1, 2])        # x1024
        #net = slim.dropout(net, keep_prob=KEEP, scope='dropout')
        net = slim.fully_connected(net, 2, activation_fn=None, normalizer_fn=None, scope='tinyX/a6')
    #e2e_logits = tf.identity(net, 'logits')
    e2e_logits = tf.identity(net)
    return e2e_logits, fcn_logits, 16

def tinyAAA (X, KEEP, num_classes=2):
    net = X
    layers = [X]
    with slim.arg_scope([slim.conv2d, slim.max_pool2d, slim.conv2d_transpose], padding='SAME'):
        net = slim.conv2d(net, 32, 3, 2)        #
        net = slim.conv2d(net, 64, 3, 1)        #
        net = slim.max_pool2d(net, 2, 2)        #
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.max_pool2d(net, 2, 2)        #
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.max_pool2d(net, 2, 2)        #
        net = slim.conv2d(net, 512, 3, 1)
        net = slim.dropout(net, keep_prob=KEEP, scope='dropout')
        net = slim.conv2d(net, 128, 3, 1)
        choke = tf.identity(net, 'choke')       # 16x16x256

        net = slim.conv2d_transpose(choke, 32, 9, 4)
        net = slim.conv2d_transpose(net, 2, 9, 4, activation_fn=None, normalizer_fn=None)
    fcn_logits = tf.identity(net, 'fcn_logits')
    with slim.arg_scope([slim.conv2d, slim.max_pool2d], padding='SAME'):
        net = slim.max_pool2d(choke, 2, 2)      # 8x8x256
        net = slim.conv2d(net, 64, 3, 1, scope='tinyX/a1')
        net = slim.conv2d(net, 32, 3, 1, scope='tinyX/a2')
        net = slim.max_pool2d(net, 2, 2)      # 8x8x256
        net = slim.conv2d(net, 32, 3, 1, scope='tinyX/a3')
        net = slim.conv2d(net, 16, 3, 1, scope='tinyX/a4')
        net = tf.reduce_mean(net, [1, 2])        # x1024
        #net = slim.dropout(net, keep_prob=KEEP, scope='dropout')
        net = slim.fully_connected(net, 2, activation_fn=None, normalizer_fn=None, scope='tinyX/a6')
    #e2e_logits = tf.identity(net, 'logits')
    e2e_logits = tf.identity(net)
    return e2e_logits, fcn_logits, 16

def tiny_orig (X, KEEP, num_classes=2):
    net = X
    layers = [X]
    with slim.arg_scope([slim.conv2d, slim.max_pool2d, slim.conv2d_transpose], padding='SAME'):
        net = slim.conv2d(net, 32, 3, 2)        #
        net = slim.conv2d(net, 64, 3, 1)        #
        net = slim.max_pool2d(net, 2, 2)        #
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.max_pool2d(net, 2, 2)        #
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.max_pool2d(net, 2, 2)        #
        net = slim.conv2d(net, 512, 3, 1)
        net = slim.dropout(net, keep_prob=KEEP, scope='dropout')
        net = slim.conv2d(net, 128, 3, 1)
        choke = tf.identity(net, 'choke')       # 16x16x256

        net = slim.conv2d_transpose(choke, 32, 9, 4)
        net = slim.conv2d_transpose(net, 2, 9, 4, activation_fn=None, normalizer_fn=None)
    fcn_logits = tf.identity(net, 'fcn_logits')
    with slim.arg_scope([slim.conv2d, slim.max_pool2d], padding='SAME'):
        net = slim.max_pool2d(choke, 3, 2)      # 8x8x256
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.conv2d(net, 128, 3, 1) 
        net = slim.max_pool2d(net, 3, 2)        # 4x4x512
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.conv2d(net, 128, 1, 1) 
        net = tf.reduce_max(net, [1, 2])        # x1024
        #net = slim.dropout(net, keep_prob=KEEP, scope='dropout')
        net = slim.fully_connected(net, 32)
        net = slim.fully_connected(net, 2, activation_fn=None, normalizer_fn=None)
    e2e_logits = tf.identity(net)
    return e2e_logits, fcn_logits, 16

def tiny2 (X, KEEP, num_classes=2):
    net = X
    layers = [X]
    with slim.arg_scope([slim.conv2d, slim.max_pool2d, slim.conv2d_transpose], padding='SAME'):
        net = slim.conv2d(net, 32, 3, 2)        # /2
        net = slim.conv2d(net, 64, 3, 1)        
        net = slim.max_pool2d(net, 2, 2)        # /2
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.conv2d(net, 128, 3, 1)
        net = slim.max_pool2d(net, 2, 2)        # /2
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.conv2d(net, 256, 3, 1)
        net = slim.max_pool2d(net, 2, 2)        # /2
        net = slim.conv2d(net, 512, 3, 1)
        net = slim.dropout(net, keep_prob=KEEP, scope='dropout')
        net = slim.conv2d(net, 128, 3, 1)
        #net = slim.conv2d(net, 64, 1, 1)
        choke = tf.identity(net, 'choke')       # 16x16x256

        net = slim.conv2d_transpose(choke, 32, 9, 4)
        net = slim.conv2d_transpose(net, 2, 9, 4, activation_fn=None, normalizer_fn=None)
    fcn_logits = tf.identity(net, 'fcn_logits')

    blocks = [resnet_utils.Block('block1', resnet_v1.bottleneck, [(64, 32, 1)] + [(64, 32, 2)]),  # 8x8
              resnet_utils.Block('block2', resnet_v1.bottleneck, [(32, 16, 1)] + [(32, 16, 2)]),  # 4x4
             ]
    net, _ = resnet_v1.resnet_v1(
                net, blocks, num_classes=num_classes,
                global_pool=True, include_root_block=False, reuse=False)
    net = tf.squeeze(net, axis=[1,2])
    #e2e_logits = tf.identity(net, 'e2e_logits')
    e2e_logits = tf.identity(net)
    return e2e_logits, fcn_logits, 16
